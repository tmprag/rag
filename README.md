# R.A.G (Ready And Go) Project

In short :
Each application "app" (the entire project) contains a number of "stacks". Each stack contains a number of Docker services (php-fpm, php-cli, nginx, database ...).

## Benefits :

* All Dockers images are based on Alpine images and therefore perform very well.
* One command to rule them all.

## URLs

### Project URLs

* Symfony Project : http://YOUR_DOMAIN/
* Angular Project : http://ng.YOUR_DOMAIN/

* Api Platform : http://YOUR_DOMAIN/api
* Swagger Api Platform : http://YOUR_DOMAIN/swagger
* Api Platform Admin : http://<CUSTOM_LOCAL_IP>:3000/#/ (run `cmd sf yarn-api-platform-admin start` before )Symfony

Note : cutomize your domain and subdomain in .env file (in ./stack_<...>/ folders only)

### Environment URLs

* PHP Code Coverage : http://YOUR_DOMAIN/php-coverage/index.html (run `cmd sf phpunit --no-interaction` before )
* Traefik : http://localhost:8080/dashboard/#/
* MailDev : http://maildev.YOUR_DOMAIN/
* Portainer : http://portainer.YOUR_DOMAIN/
* RabbitMQ : http://rabbitmq.YOUR_DOMAIN/
* Kibana Logs : [http://kibana.YOUR_DOMAIN/app/infra](http://kibana.YOUR_DOMAIN/app/infra#/logs/stream?_g=()&logFilter=(expression:'event.dataset%20:%20%22nginx.access%22%20or%20event.dataset%20:%20%22nginx.access%22%20or%20event.dataset%20:%20%22system.syslog%22%20',kind:kuery)) (run `docker-compose exec -T filebeat filebeat setup` before)

Note : cutomize your domain and subdomain in .env file (in ./stack_<...>/ folders only)

## Configuration

### /etc/hosts config :
```text
127.0.0.1       YOUR_DOMAIN
127.0.0.1       ez.YOUR_DOMAIN

127.0.0.1       traefik.local
127.0.0.1       portainer.local
127.0.0.1       maildev.local
127.0.0.1       doc.local
127.0.0.1       kibana.local
127.0.0.1       rabbitmq.local
```

### Database in your IDE :
```text
Name : Dev
Host : localhost
Port : 5432
Database : rag (by default)
Username : rag (by default)
Password : rag (by default)
URL : jdbc:postgresql://localhost:5432/rag

Name : Test
Host : localhost
Port : 5432
Database : rag_test (by default)
Username : rag (by default)
Password : rag (by default)
URL : jdbc:postgresql://localhost:5432/rag_test
```

### Create your alias :

```
echo 'alias cmd="./docker/cmd.sh' >> ~/.bash_aliases && source ~/.bash_aliases
```

## The CLI tools (~/.bash_aliases)

One command to rule them all.

```shell script
cmd
```

### Examples : 

#### Symfony Stack (with ApiPlatform and Swagger) : 

The stack alias is "sf"

* `cmd sf php -v` (php -i, php -m, ...)
* `cmd sf composer install`
* `cmd sf console cache:clear`
* `cmd sf phpunit` (is required **first** to create the database from fixtures)
* `cmd sf phpunit --no-interaction` (fast mod)
* `cmd sf yarn install`
* `cmd sf yarn-api-platform-admin install` or `cmd sf yarn-api-platform-admin start`

#### Ez Platform Stack : 

The stack alias is "ez"

* `cmd ez php -v` (php -i, php -m, ...)
* `cmd ez composer install`
* `cmd ez console cache:clear`
* `cmd ez phpunit` (is required **first** to create the database from fixtures)
* `cmd ez phpunit --no-interaction` (fast mod)
* `cmd ez yarn install`

#### Angular Stack : 

The stack alias is "agr"

Yarn

* `cmd agr yarn -v`
* `cmd agr yarn install`
...
* `cmd agr yarn --help`

Angular CLI

* `cmd agr ng version`
* `cmd agr ng generate component xyz`
* `cmd agr ng build --output-hashing=all`
* `cmd agr ng build --output-hashing=all --watch=true`
...
* `cmd agr ng help`

Serve with Traefik (nginx)

* `cmd agr serve`

Show logs

* `cmd agr logs`

#### And more...

* `cmd all drop`
* `cmd common start` Start common containers (traefik, portainer, maildev, elk, ...)
* `cmd help`

#### Notes

##### Tests

This feature is available from the command `cmd <stack_alias> phpunit` :
* Possibility of **restoring** the database on the fixtures before each test battery.
* It is possible to **rollback** or not the database after the end of the test.
* Everything needed to reach API routes with the following pre-authenticated methods: get, post, delete, put and patch.
* Use paratest (https://github.com/paratestphp/paratest)

```php
 $json = $this->apiGet('/examples?page=1');
 $json = $this->apiPost('/example', [...]);
 $json = $this->apiPut('/example', [...]);
 $json = $this->apiPatch('/example', [...]);
 $json = $this->apiDelete('/example');
```

Or with a specific user (to test a role for example) :

```php
 $json = $this->apiGet('/examples?page=1', Test::USER_1_USERNAME, Test::USER_1_USERNAME);
 $json = $this->apiPost('/example', [...], Test::USER_1_USERNAME, Test::USER_1_USERNAME);
 $json = $this->apiPut('/example', [...], Test::USER_1_USERNAME, Test::USER_1_USERNAME);
 $json = $this->apiPatch('/example', [...], Test::USER_1_USERNAME, Test::USER_1_USERNAME);
 $json = $this->apiDelete('/example', Test::USER_1_USERNAME, Test::USER_1_USERNAME);
```

#### Configure your Portainer

Run this command : 

```shell script
cmd sf change-portainer-password "admin" "password"
```

#### Api PLatform Admin

* `cmd sf yarn-api-platform-admin start`

And open : http://<CUSTOM_IP>:3000/#/

#### Dump Database

* `cmd sf database-dump`
* `cmd ez database-dump`

#### Restore Database

Restore specific dump :

* `cmd sf restore-database "03-10-2020_18_38_31"`
* `cmd ez restore-database "03-10-2020_18_38_31"`

Restore last dump :

* `cmd sf database-reset`
* `cmd ez database-reset`

## Project configuration and versions

* All Stacks :
```text
OPCache is pre-configured
PHP-CS-Fixer which is executed in the git hook, at the time of pre-commit.

==========================================================================

docker 19.03.13
docker-compose 1.27.4
Dockerfile 3.8

PHP 7.14.11 (alpine 3.12)
Nginx 1.19.2 (alpine 3.12)
Composer v2 (https://php.watch/articles/composer-2)
xDebug 2.9.8

ELK 7.9.2
PostgreSQL 13.0 (https://www.enterprisedb.com/blog/postgresql-vs-mysql-360-degree-comparison-syntax-performance-scalability-and-features)
```

* Symfony Stack :
```text
Symfony 5.1.7
PHP Unit 9.4.0 (Prophecy is deprecated https://github.com/sebastianbergmann/phpunit/issues/4141 !)
PHP Code Coverage 9.2.0
```

* Angular Stack :
```text
NodeJs 14.13.1 (alpine 3.12)
Yarn 1.22.5
Angular CLI 10.1.6
```

### Git flow

Git flow is integrated with the cmd command which as always uses an alpine Docker image of the latest version !!

Learn more about git flow:
* [EN] https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
* [FR] https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow

Setup example :
```
cmd common start
```

Little example :
```
cmd git flow feature <name> start
cmd git add .
git commit -m "<message>" (for the moment, without "cmd" prefix because : https://gitlab.com/tmprag/rag/-/issues/1)
...
cmd git flow feature finish --squash
cmd git pull
cmd git push
```

### A venir

* Codecov Report (https://codecov.io/)
* GrumPHP https://github.com/phpro/grumphp
* PHP Mess Detector
* PHP Code Sniffer
* Noter ici tous les mots de passe par défaut du modèle !
* Intégration continue sur gitlab-ci
* Deployer avec Let's Encrypt et Kubernetes
* Rédiger les tests PHP Unit et Cypress.io
* Vulcain + Mecrure : https://www.youtube.com/watch?v=iqEAPKARx9E&ab_channel=Les-Tilleuls.coop
* Remember Me préconfiguré !!
* Switch User préconfiguré !!
* Connexion avec JWT depuis ApiPlatformAdmin : https://github.com/api-platform/demo/pull/147/files (https://github.com/api-platform/docs/issues/1062 and https://api-platform.com/docs/admin/authentication-support/)
* XDebug in Docker !
* HTTP 2 (a venir)

### A implémenter en exemple

* Echange Messenger (SensioLabs)
* Echange RabbitMQ
* DDD : https://dev.to/ludofleury/domain-driven-design-avec-php-symfony-1p2h
* Event Sourcing
* CQRS : https://medium.com/tiller-systems/pourquoi-avoir-choisi-dutiliser-l-architecture-cqrs-e04c082f8b5f
* Constellation
* Angular

## En cours

* Fix : http://<DOMAIN>/admin/quizz/add => 403 meme en admin !
* Mettre les assertions dans les entités...
* Catch API 403 error... => pretty json render
* Catch 403 error... => pretty render
* Supprimer toutes les classes PHP plus utilisées (login, forgot password...)

### Retours & bugs

If you encounter an error for nginx (permission denied), please delete the logs volume : `docker volume rm <COMPOSE_PROJECT_NAME>_logs`

##### Filebeat

* Filebeat container :
```text
2020-09-23T19:01:19.564Z	ERROR	log/input.go:501	Harvester could not be started on new file: /var/log/nginx/.../error.log, Err: registry already stopped
2020-09-23T19:01:19.585Z	ERROR	instance/beat.go:878	Exiting: Error while initializing input: Can only start an input when all related states are finished: {Id:7083877-2054 Finished:false Fileinfo:0xc0006fc340 Source:/var/log/nginx/symfony_access.log Offset:0 Timestamp:2020-09-23 19:01:19.563324766 +0000 UTC m=+0.042034147 TTL:-1ns Type:log Meta:map[] FileStateOS:7083877-2054}
Exiting: Error while initializing input: Can only start an input when all related states are finished: {Id:7083877-2054 Finished:false Fileinfo:0xc0006fc340 Source:/var/log/nginx/symfony_access.log Offset:0 Timestamp:2020-09-23 19:01:19.563324766 +0000 UTC m=+0.042034147 TTL:-1ns Type:log Meta:map[] FileStateOS:7083877-2054
```

[]: http://kibana.YOUR_DOMAIN/app/infra