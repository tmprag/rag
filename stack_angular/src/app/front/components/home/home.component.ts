import {Component} from '@angular/core';

import {XyzComponent} from '../xyz/xyz.component';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent {
  title = 'example';
}
