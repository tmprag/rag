import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';

import {HomeComponent} from './front/components/home/home.component'
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {NoCacheHeadersInterceptor} from "./network/interceptors/NoCacheHeadersInterceptor";
import {XyzComponent} from "./front/components/xyz/xyz.component";

@NgModule({
  declarations: [
    HomeComponent,
    XyzComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NoCacheHeadersInterceptor,
      multi: true
    }
  ],
  bootstrap: [HomeComponent]
})

export class AppModule {
}


