# shellcheck disable=SC2164
# shellcheck disable=SC2068
dockerComposeCommon() {
  set -x;
  dockerCreateNetwork "common"
  docker-compose \
    --file="${DIR}/docker-compose.yml" \
    --project-directory="${DIR}" \
    --project-name="common" \
    ${1}
  set +x;
}

# shellcheck disable=SC2164
# shellcheck disable=SC2068
dockerComposeCommonRun() {
  set -x;
  dockerCreateNetwork "common"
  docker-compose \
    --file="${DIR}/docker-compose.yml" \
    --project-directory="${DIR}" \
    --project-name="common" \
      run \
        --rm \
        ${@}
  set +x;
}

# shellcheck disable=SC2164
# shellcheck disable=SC2068
dockerComposeApp() {
  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
    ${@}
  set +x;
}

# shellcheck disable=SC2164
# shellcheck disable=SC2068
dockerComposeAppExec() {
  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
    exec \
      ${@}
  set +x;
}

dockerComposeAppExec() {
  # shellcheck disable=SC2016
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
    exec \
      ${@}
  set +x;
}

dockerComposeAppExecSh() {
  # shellcheck disable=SC2016
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
    exec \
      ${1} /bin/sh -c "${2}"
  set +x;
}

# shellcheck disable=SC2068
# shellcheck disable=SC2016
dockerComposeAppRun() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
      run \
        --rm \
        ${@}
  set +x;
}

# shellcheck disable=SC2068
# shellcheck disable=SC2016
dockerComposeAppRunSh() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
      run \
        --rm \
        "${1}" /bin/sh -c "${2}"
  set +x;
}

# shellcheck disable=SC2164
# shellcheck disable=SC2068
dockerComposeAppExecRoot() {
  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
    exec \
      --user=root \
      ${@}
  set +x;
}

# shellcheck disable=SC2164
# shellcheck disable=SC2068
dockerComposeAppExecRootSh() {
  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
    exec \
      --user=root \
      "${1}" /bin/sh -c "${2}"
  set +x;
}


# shellcheck disable=SC2068
# shellcheck disable=SC2016
dockerComposeAppRunRoot() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi
  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
      run \
        --rm \
        --user=root \
        ${@}
  set +x;
}

# shellcheck disable=SC2068
# shellcheck disable=SC2016
dockerComposeAppRunRootSh() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  set -x;
  dockerCreateNetwork "${COMPOSE_PROJECT_NAME}_${STACK_NAME}"
  dockerCreateVolume "${COMPOSE_PROJECT_NAME}_logs"
  docker-compose \
    --file="${DIR}/../${STACK_NAME}/docker-compose.yml" \
    --project-directory="${DIR}/../${STACK_NAME}" \
    --project-name="${COMPOSE_PROJECT_NAME}_${STACK_NAME}" \
      run \
        --rm \
        --user=root \
        ${1} /bin/sh -c "${2}"
  set +x;
}

# shellcheck disable=SC2046
dockerComposeDropService() {
  SERVICE=${1}
  title2 "Drop ${SERVICE} service"

  read -p "Stop and drop service ${SERVICE} ? [y or Y] " -n 1 -r; echo; if [[ $REPLY =~ ^[Yy]$ ]]; then
    dockerComposeApp "rm --stop --force ${SERVICE}"
  fi
}