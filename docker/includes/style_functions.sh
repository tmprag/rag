title1() {
  echo ""
  echo -e "\e[105m"
  echo " >   ${1}   "
  echo -e "\e[49m"
  echo ""
}

title2() {
  echo ""
  echo -e "\e[44m"
  echo " >>   ${1}   "
  echo -e "\e[49m"
  echo ""
}