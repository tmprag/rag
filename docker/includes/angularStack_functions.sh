# shellcheck disable=SC2145
angularContainer() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  title2 "Node CLI [www-data] (${STACK_NAME}) : ${@}"
  dockerComposeAppRun "angular_cli ${@}"
}

# shellcheck disable=SC2120
# shellcheck disable=SC2046
cypressInstall() {
  if [ ! -d "${DIR}/cypress/e2e/node_modules" ]; then
    echo "Installing des node_modules de Cypress dans ${1}/cypress/e2e/node_modules"
    docker run --rm -it \
      -e PUID=$(id -u) \
      -e PGID=$(id -g) \
      -v "${DIR}/cypress/e2e":/app \
      -w "/${STACK_NAME}" \
      node:12-alpine npm install
    cd "${DIR}/cypress/e2e" && ./node_modules/cypress/bin/cypress install
  fi
}

function angularLogs() {
  dockerComposeApp "logs"
  #dockerComposeAppExecSh "nginx" "tail /var/log/nginx/stack_angular/*"
}

function stackAngularProcess() {
    title1 "Choose your action : "
    dockerPrepareNodeBasesImagesIfNotExist "14.13.1"

    if [[ "${1}" == "yarn" ]]; then
        angularContainer "${@:1}"
    elif [[ "${1}" == "ng" ]]; then
        angularContainer "${@:1}"
    elif [[ "${1}" == "serve" ]]; then
      title2 "DockerCompose Build & Up"
      dockerComposeCommon "build"
      dockerComposeCommon "up -d"

      dockerComposeApp "build"
      dockerComposeApp "up -d"

#      dockerComposeAppExecRootSh "angular_cli" "mkdir -p /var/www/project/dist/angular-project"
#      dockerComposeAppExecRootSh "angular_cli" "chown -R 1000:1000 /var/www/project"
#      dockerComposeAppExecRootSh "angular_cli" "chmod -R 0777 /var/www/project/dist"
#      dockerComposeAppExecSh "angular_cli" "ls -lah /var/www/project"
#      dockerComposeAppExecSh "angular_cli" "ng build"

#      dockerComposeAppExecRootSh "nginx" "mkdir -p /var/www/project/dist/angular-project"
#      dockerComposeAppExecRootSh "nginx" "chown -R 1000:1000 /var/www/project"
#      dockerComposeAppExecRootSh "nginx" "chmod -R 0777 /var/www/project/dist"
#      dockerComposeAppExecSh "nginx" "ls -lah /var/www/project"
      angularLogs
    elif [[ "${1}" == "logs" ]]; then
      angularLogs
    else
      if [ ! -z "${1}" ]; then
        echo "Your choice (${1}) is not allowed !!"
      fi
      echo -e "You have to choose between
        'yarn',
        'ng',
        'serve'"
      exit 1;
    fi
}