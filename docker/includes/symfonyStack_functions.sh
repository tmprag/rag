# shellcheck disable=SC2016
checkSymfonyStackRequirements() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi
  if [ -z "${DATABASE_HOST}" ]; then echo 'var ${DATABASE_HOST} NOT FOUND !!!'; exit 1; fi
  if [ -z "${DATABASE_USER}" ]; then echo 'var ${DATABASE_USER} NOT FOUND !!!'; exit 1; fi
  if [ -z "${DATABASE_NAME}" ]; then echo 'var ${DATABASE_NAME} NOT FOUND !!!'; exit 1; fi
}

# shellcheck disable=SC2145
phpContainer() {
  checkSymfonyStackRequirements

  title2 "PHP Cli [www-data] (${STACK_NAME}) : ${@}"
  dockerComposeAppRun "php_cli ${@}"
}

# shellcheck disable=SC2145
phpRootContainer() {
  checkSymfonyStackRequirements

  title2 "PHP Cli [root] (${STACK_NAME}) : ${@}"

  dockerComposeAppRunRootSh "php_cli" "${@}"
}

# shellcheck disable=SC2016
# shellcheck disable=SC2124
postgresQuery() {
  checkSymfonyStackRequirements

  QUERY="${@}";
  title2 "Postgres (${STACK_NAME}) : ${QUERY}"
  echo "${QUERY}" | dockerComposeAppExec "-T ${DATABASE_HOST} psql --username=${DATABASE_USER} --dbname=${DATABASE_NAME}"
}

# shellcheck disable=SC2124
# shellcheck disable=SC2016
postgresRootQuery() {
  checkSymfonyStackRequirements

  QUERY="${@}";
  title2 "Postgres [root] (${STACK_NAME}) : ${QUERY}"
  echo "${QUERY}" | dockerComposeAppExecRoot "-T ${DATABASE_HOST} psql --username=${DATABASE_USER} --dbname=${DATABASE_NAME}"
}

# shellcheck disable=SC2016
postgresLoadFileContainer() {
  checkSymfonyStackRequirements

  FILE_PATH=${1};
  title2 "Postgres (${STACK_NAME}) : ${QUERY}"
  # shellcheck disable=SC2002
  cat "${FILE_PATH}" | dockerComposeAppExecRoot "-T ${DATABASE_HOST} psql --username=${DATABASE_USER} --dbname=${DATABASE_NAME}"
}

postgresCommand() {
  COMMAND="${1}";
  dockerComposeAppExec "-T ${DATABASE_HOST} psql --username=${DATABASE_USER} --dbname=${DATABASE_NAME} --command=${COMMAND}"
}

postgresRootCommand() {
  COMMAND="${1}";
  dockerComposeAppExecRoot "-T ${DATABASE_HOST} psql --username=${DATABASE_USER} --dbname=${DATABASE_NAME} --command=${COMMAND}"
}

# shellcheck disable=SC2145
symfonyConsole() {
  title1 "Console : ${@}"
  phpContainer "bin/console ${@}"
}

# shellcheck disable=SC2145
# shellcheck disable=SC2016
phpUnit() {
  checkSymfonyStackRequirements

  title1 "Console : ${@}"
  dockerComposeAppRun "-e ROLLBACK_MOD=${ROLLBACK_MOD} --rm php_cli bin/phpunit -vvv ${@/--no-interaction/}"

  # Todo : debug (paratest) this : https://github.com/paratestphp/paratest/issues/397
  #dockerComposeAppRun "-e ROLLBACK_MOD=${ROLLBACK_MOD} --rm php_cli vendor/bin/paratest -p8 -vvv ${@/--no-interaction/}"

  # Run ALL coverage in background... ;)
  rm -rf "${DIR}/../${STACK_NAME}/public/php-coverage"
  dockerComposeAppRun "-e ROLLBACK_MOD=${ROLLBACK_MOD} --rm php_cli bin/phpunit --coverage-html /var/www/project/public/php-coverage -vvv" &>/dev/null &
}

removeComposerLock() {
  # shellcheck disable=SC2016
  checkSymfonyStackRequirements

  title1 "Remove ${DIR}/../${STACK_NAME}/composer.lock :"
  rm -f "${DIR}/../${STACK_NAME}/composer.lock";
}

# shellcheck disable=SC2145
phpComposer() {
  title1 "Composer : ${@}"
  phpContainer "composer ${@}"
}

fixChown() {
  title2 "Fix Chown"

  phpRootContainer "chown -R 1000:1000 *"
  phpRootContainer "chown -R 1000:1000 ./.git"
}

clearDatabase() {
  postgresQuery "DROP SCHEMA public CASCADE;"
  postgresQuery "CREATE SCHEMA public;"
  postgresRootQuery "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${DATABASE_NAME}';"
}

clearDatabaseWithSymfony() {
  symfonyConsole "--env=${APP_ENV} doctrine:schema:drop --no-interaction --force --full-database --env=${APP_ENV}"
  symfonyConsole "doctrine:database:create --if-not-exists --no-interaction --env=${APP_ENV}";
}

loadDevFixtures() {
  symfonyConsole "--env=${APP_ENV} doctrine:fixtures:load --no-interaction --append --group=dev --env=${APP_ENV}"
}

loadTestFixtures() {
  symfonyConsole "--env=${APP_ENV} doctrine:fixtures:load --no-interaction --append --group=functional-test --group=unit-test --env=${APP_ENV}"
}

restoreLastDump() {
  clearDatabase
  # shellcheck disable=SC2046
  # shellcheck disable=SC2002
  # shellcheck disable=SC2012
  # shellcheck disable=SC2140
  postgresLoadFileContainer "${DIR}/../${STACK_NAME}"/dumps/"${DATABASE_PLATFORM}"/$(ls -t "${DIR}/../${STACK_NAME}"/dumps/"${DATABASE_PLATFORM}"* | head -1)
  postgresRootQuery "ALTER USER ${DATABASE_USER} WITH PASSWORD '${DATABASE_PASSWORD}'"
}

# shellcheck disable=SC2145
appYarn() {
  checkSymfonyStackRequirements

  title2 "Yarn (${STACK_NAME}) : ${@}"

  docker run --rm -it \
    -e PUID="$(id -u)" \
    -e PGID="$(id -g)" \
    -v "${DIR}/../${STACK_NAME}":/app \
    -w /app \
    andrewmackrodt/nodejs:12 yarn "${@}"
}

# shellcheck disable=SC2145
appYarnApi() {
  checkSymfonyStackRequirements

  title2 "Yarn (${STACK_NAME}) : ${@}"

  docker run --rm -it \
    -e PUID="$(id -u)" \
    -e PGID="$(id -g)" \
    -v "${DIR}/../${STACK_NAME}/api-platform-admin":/app \
    -w /app \
    andrewmackrodt/nodejs:12 yarn "${@}"
}

function stackEzPlatformProcess() {
    stackSymfonyProcess
}

# shellcheck disable=SC2120
function stackSymfonyProcess() {
  title1 "Choose your action : "

  if [[ "${1}" == "reset" || "${1}" == "start" ]]; then
      dockerPreparePhpBasesImagesIfNotExist "7.4.11" "cli";
      dockerPreparePhpBasesImagesIfNotExist "7.4.11" "fpm";
      docker images --filter reference=base

      dockerComposeDropService "nginx"
      dockerComposeDropService "php_fpm"
      sleep 5
      dockerDropImages

      title2 "DockerCompose Build & Up"
      dockerComposeCommon "build"
      dockerComposeCommon "up -d"

      dockerComposeApp "build"
      dockerComposeApp "up -d"

      removeComposerLock
      fixChown
      phpComposer "install --dev --optimize-autoloader"
      phpComposer "dump-autoload --classmap-authoritative"
      symfonyConsole "doctrine:migration:migrate --no-interaction"
      symfonyConsole "assets:install --symlink public"
      read -p "Erase DataBase and restore last DUMP (for app ${STACK_NAME}) [y or Y] " -n 1 -r; echo; if [[ $REPLY =~ ^[Yy]$ ]]; then
         restoreLastDump
      fi

      read -p "Install and compil front ? [y or Y] " -n 1 -r; echo; if [[ $REPLY =~ ^[Yy]$ ]]; then
        appYarn "install"
        appYarn "encore dev"
      fi

      fixChown

      docker ps -a
      sleep 5;
      docker ps -a

      dockerComposeApp "logs -f"
  elif [[ "${1}" == "database-dump" ]]; then
      dockerComposeApp "exec -T ${DATABASE_HOST} pg_dumpall -c -U ${DATABASE_NAME}" > "${DIR}/../${STACK_NAME}/dumps/${DATABASE_PLATFORM}/dump_$(date +%d-%m-%Y"_"%H_%M_%S).sql"
  elif [[ "${1}" == "database-restore" ]]; then
      ifChoiceIsEmpty "${2}" "'your_dump_filename'"
      clearDatabase
      # shellcheck disable=SC2002
      cat "${DIR}/../${STACK_NAME}/dumps/${DATABASE_PLATFORM}/dump_${2}.sql" | dockerComposeApp "exec -T ${DATABASE_HOST} psql -U ${DATABASE_NAME}"
  elif [[ "${1}" == "database-restore-last" ]]; then
      restoreLastDump
  elif [[ "${1}" == "database-list-user" ]]; then
      postgresCommand "\du+"
  elif [[ "${1}" == "fix-rights" || "${1}" == "ch" ]]; then
      fixChown
  elif [[ "${1}" == "logs" ]]; then
      dockerComposeApp "logs -f"
  elif [[ "${1}" == "cache:clear" || "${1}" == "c:c" ]]; then
      phpContainer "rm -rf var"
      phpComposer "dump-autoload"
      symfonyConsole "cache:clear"
      symfonyConsole "cache:clear --env=${APP_ENV}"
      symfonyConsole "doctrine:cache:clear-metadata"
  elif [[ "${1}" == "php-cs-fixer" || "${1}" == "php-cs" ]]; then
      phpContainer "./vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix --config=.php_cs.php"
  elif [[ "${1}" == "cypress" ]]; then
      cypressInstall
      cd "${DIR}/cypress/e2e" && CYPRESS_BASE_URL="${CYPRESS_BASE_URL}" ./node_modules/cypress/bin/cypress open &
  elif [[ "${1}" == "cypress-cli" ]]; then
      cypressInstall
      cd "${DIR}/cypress/e2e" && CYPRESS_BASE_URL="${CYPRESS_BASE_URL}" ./node_modules/cypress/bin/cypress run
      dockerComposeApp "build e2e"
      dockerComposeApp "up e2e"
      #circleci config process circle.yml | sed /^#/d"
  elif [[ "${1}" == "js-routing:dump" ]]; then
      symfonyConsole "fos:js-routing:debug"
      symfonyConsole "fos:js-routing:dump --format=json --target=assets/js/config/fosJsRoutes.json"
  elif [[ "${1}" == "composer" ]]; then
      phpComposer "${@:2}"
  elif [[ "${1}" == "console" ]]; then
      symfonyConsole "${@:2}"
  elif [[ "${1}" == "phpunit" ]]; then
      # shellcheck disable=SC1090
      loadStackEnvTestFile

      ROLLBACK_MOD=false;

      # shellcheck disable=SC2199
      if [[ "${@}" == *"--no-interaction"* ]]; then
        ROLLBACK_MOD=true;
      else
        clearDatabaseWithSymfony
        symfonyConsole "--env=${APP_ENV} doctrine:migration:migrate --no-interaction --env=${APP_ENV}"

        read -p "First reset the database from the fixtures ? [y or Y] " -n 1 -r; echo; if [[ $REPLY =~ ^[Yy]$ ]]; then
          loadTestFixtures
        fi

        read -p "Do you want to rollback the database after the test is finished ? [y or Y] " -n 1 -r; echo; if [[ $REPLY =~ ^[Yy]$ ]]; then
          ROLLBACK_MOD=true;
        fi
      fi

      phpUnit "${@:2}"
  elif [[ "${1}" == "php" ]]; then
      # shellcheck disable=SC2145
      phpContainer "php ${@:2}"
  elif [[ "${1}" == "yarn" ]]; then
      # shellcheck disable=SC2145
      appYarn "${@:2}"
  elif [[ "${1}" == "yarn-api-platform-admin" ]]; then
      # shellcheck disable=SC2145
      appYarnApi "${@:2}"
  elif [[ "${1}" == "load-dev-fixtures" ]]; then
      loadDevFixtures
  elif [[ "${1}" == "generate-jwt-certs" ]]; then
    openssl genpkey -out "${DIR}/../${STACK_NAME}/config/jwt/private.pem" -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    openssl pkey -in "${DIR}/../${STACK_NAME}/config/jwt/private.pem" -out "${DIR}/../${STACK_NAME}/config/jwt/public.pem" -pubout
  elif [[ "${1}" == "change-portainer-password" ]]; then
      # shellcheck disable=SC2034
      ENCODED_PASSWORD=$(docker run --rm httpd:2.4-alpine htpasswd -nbB "${2}" "${3}" | cut -d ":" -f 2)
      ESCAPE_ENCODED_PASSWORD="${ENCODED_PASSWORD//\$/\$\$}"
      updateEnvVar "PORTAINER_PASSWORD" "${ESCAPE_ENCODED_PASSWORD}" ".env"
  elif [[ "${1}" == "help" || "${1}" == "--help" ]]; then
      clear
      cat "${DIR}/help.txt"
  else
    if [ ! -z "${1}" ]; then
      echo "Your choice (${1}) is not allowed !!"
    fi
    echo -e "You have to choose between
      'reset|start',
      'generate-jwt-certs',
      'database-restore',
      'database-restore-last',
      'database-dump',
      'database-list-user',
      'fix-rights|ch',
      'logs',
      'cache:clear|c:c',
      'php-cs-fixer|php-cs',
      'cypress',
      'cypress-cli',
      'js-routing:dump',
      'composer',
      'console',
      'phpunit',
      'php',
      'yarn',
      'yarn-api-platform-admin',
      'load-dev-fixtures',
      'change-portainer-password'
      'help|--help'"
    exit 1;
  fi
}