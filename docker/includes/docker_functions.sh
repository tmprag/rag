# shellcheck disable=SC2046
dockerDropAll() {
  docker container stop $(docker container ls -aq);
  docker container rm $(docker container ls -aq);
  docker image prune --all --force;
  docker volume prune --force;
  docker network prune --force;
  docker system prune --all --force --volumes;

  docker container ls --all;
  docker image ls;
  docker volume ls;
  docker network ls;
}

# shellcheck disable=SC2034
dockerPreparePhpBasesImagesIfNotExist() {
  PHP_VERSION=${1}
  MOD=${2}
  IMAGE_NAME="base:php-${PHP_VERSION}-${MOD}"

  #docker rmi ---force "${IMAGE_NAME}"

  if [[ "$(docker images -q "${IMAGE_NAME}" 2> /dev/null)" == "" ]]; then
    docker build --tag "${IMAGE_NAME}" "./docker/bases/php/${PHP_VERSION}/php-${MOD}"
  fi
}

# shellcheck disable=SC2034
dockerPrepareNodeBasesImagesIfNotExist() {
  NODE_VERSION=${1}
  MOD=${2}
  IMAGE_NAME="base:node-${NODE_VERSION}"

  #docker rmi ---force "${IMAGE_NAME}"

  if [[ "$(docker images -q "${IMAGE_NAME}" 2> /dev/null)" == "" ]]; then
    docker build --tag "${IMAGE_NAME}" "./docker/bases/node/${NODE_VERSION}"
  fi
}

dockerCreateNetwork() {
  NETWORK_NAME=${1}
  docker network inspect "${NETWORK_NAME}" >/dev/null 2>&1 || \
    docker network create --driver bridge "${NETWORK_NAME}"
  #docker network create --driver bridge "${NETWORK_NAME}" || true
}

dockerCreateVolume() {
  VOLUME_NAME=${1}
  docker volume inspect "${VOLUME_NAME}" >/dev/null 2>&1 || \
    docker volume create "${VOLUME_NAME}"
  #docker volume create "${VOLUME_NAME}" || true
}

stopContainerByRegex() {
  REGEX=${1}

  # shellcheck disable=SC2236
  if [ ! -z "$(docker images --quiet --all --filter=reference="${REGEX}")" ]; then
      docker images --all --filter=reference="${REGEX}"
      # shellcheck disable=SC2046
      docker rmi -f $(docker images --quiet --all --filter=reference="${REGEX}");
  fi
}

dropContainerByRegex() {
  REGEX=${1}

  # shellcheck disable=SC2236
  if [ ! -z "$(docker ps --quiet --all --filter "name=^/.${REGEX}$")" ]; then
      docker ps --all --filter "name=^/${REGEX}$"
      # shellcheck disable=SC2046
      docker stop $(docker ps --quiet --all --filter "name=^/${REGEX}$");
  fi
}

dropImageByRegex() {
  REGEX=${1}

  read -p "Delete the image(s) ${REGEX} for the app ${COMPOSE_PROJECT_NAME} for the stack ${STACK_NAME} ? [y or Y] " -n 1 -r; echo; if [[ $REPLY =~ ^[Yy]$ ]]; then
    # shellcheck disable=SC2236
    if [ ! -z "$(docker images --quiet --all --filter=reference="${REGEX}")" ]; then
        docker images --all --filter=reference="${REGEX}"
        # shellcheck disable=SC2046
        docker rmi -f $(docker images --quiet --all --filter=reference="${REGEX}");
    fi
  fi
}

dropVolumeByRegex() {
  REGEX=${1}

  read -p "Delete the volume(s) ${REGEX} for the app ${COMPOSE_PROJECT_NAME} for the stack ${STACK_NAME} ? [y or Y] " -n 1 -r; echo; if [[ $REPLY =~ ^[Yy]$ ]]; then
    # shellcheck disable=SC2236
    if [ ! -z "$(docker volume ls --quiet --filter "name=${REGEX}")" ]; then
        docker volume ls --filter "name=${REGEX}"
        # shellcheck disable=SC2046
        docker volume rm --force $(docker volume ls  --quiet --filter "name=${REGEX}");
    fi
  fi
}

# shellcheck disable=SC2046
dockerStopContainers() {
  title2 "Stop containers (php/nginx)"

  stopContainerByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}_php_fpm_.*"
  stopContainerByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}_php_cli_.*"
  stopContainerByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}_php_nginx_.*"
}

# shellcheck disable=SC2046
dockerDropContainers() {
  title2 "Drop containers (php/nginx)"

  dropContainerByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}_php_fpm_.*"
  dropContainerByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}_php_cli_.*"
  dropContainerByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}_php_nginx_.*"
}

# shellcheck disable=SC2046
dockerDropImages() {
  title2 "Drop images (php/nginx)"

  dropImageByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}:php_cli"
  dropImageByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}:php_fpm"
  dropImageByRegex "${COMPOSE_PROJECT_NAME}_${STACK_NAME}:nginx"
}

dockerLogs() {
  SERVICE=${1}

  dockerComposeApp "logs --tail=all ${SERVICE}"
}