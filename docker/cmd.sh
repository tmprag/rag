#!/bin/bash

clear;

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
if [ -f "${DIR}/.env" ]; then
    # Load Environment Variables
    # shellcheck disable=SC2046
    # shellcheck disable=SC2002
    export $(cat "${DIR}/.env" | grep -v '#' | awk '/=/ {print $1}')
fi

# shellcheck disable=SC1090
source "${DIR}/includes/style_functions.sh"

# shellcheck disable=SC1090
source "${DIR}/includes/docker_functions.sh"

# shellcheck disable=SC1090
source "${DIR}/includes/dockerCompose_functions.sh"

# shellcheck disable=SC1090
source "${DIR}/includes/symfonyStack_functions.sh"

# shellcheck disable=SC1090
source "${DIR}/includes/angularStack_functions.sh"

updateEnvVar() {
  VAR_NAME="${1}";
  VALUE="${2}";
  ENV_FILE="${3}";
  ESCAPE_VALUE=$(echo "${VALUE}" | sed 's/\//\\\//g')
  sed -i "s/${VAR_NAME}=.*/${VAR_NAME}=${ESCAPE_VALUE}/g" "${DIR}/${ENV_FILE}"
}

copyGitHooks() {
  # shellcheck disable=SC2225
  mkdir -p "${DIR}"/../.git/hooks/
  cp "${DIR}"/../hooks/* "${DIR}"/../.git/hooks/
}

loadStackEnvDevFile() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  # shellcheck disable=SC1090
  source "${DIR}/../${STACK_NAME}/.env"
}

loadStackEnvTestFile() {
  if [ -z "${STACK_NAME}" ]; then echo 'var ${STACK_NAME} NOT FOUND !!!'; exit 1; fi

  # shellcheck disable=SC1090
  source "${DIR}/../${STACK_NAME}/.env.test"
}

ifChoiceIsEmpty() {
  # shellcheck disable=SC2236
  if [ -z "${1}" ]; then
    echo "Your choice (${1}) is not allowed !!"
    echo "You have to choose between ${2} or 'help|--help'"
    exit 1;
  fi
}

ifChoiceIsNotAllowed() {
  # shellcheck disable=SC2236
  if [ ! -z "${1}" ]; then
    echo "Your choice (${1}) is empty !!"
  fi

  echo "You have to choose between ${2} or 'help|--help'"
  exit 1;
}

# shellcheck disable=SC2120
# shellcheck disable=SC2046
# shellcheck disable=SC2002
function chooseStackProcess() {
    title1 "Choose your stack : "

    if [[ "${1}" == "sf" ]]; then
      if [ -f "${DIR}/../stack_symfony/.env" ]; then
        export $(cat "${DIR}/../stack_symfony/.env" | grep -v '#' | awk '/=/ {print $1}')
        echo "The selected Stack is : ${STACK_NAME}";
        loadStackEnvDevFile
      else
        echo "The file ${DIR}/../stack_symfony/.env does not exist"
        exit 1;
      fi
      stackSymfonyProcess "${@:2}"
    elif [[ "${1}" == "ez" ]]; then
      if [ -f "${DIR}/../stack_ez_platform/.env" ]; then
        export $(cat "${DIR}/../stack_ez_platform/.env" | grep -v '#' | awk '/=/ {print $1}')
        echo "The selected Stack is : ${STACK_NAME}";
        loadStackEnvDevFile
      else
        echo "The file ${DIR}/../stack_ez_platform/.env does not exist"
        exit 1;
      fi
      stackEzPlatformProcess "${@:2}"
    elif [[ "${1}" == "agr" ]]; then
      if [ -f "${DIR}/../stack_angular/.env" ]; then
        export $(cat "${DIR}/../stack_angular/.env" | grep -v '#' | awk '/=/ {print $1}')
        echo "The selected Stack is : ${STACK_NAME}";
        loadStackEnvDevFile
      else
        echo "The file ${DIR}/../stack_angular/.env does not exist"
        exit 1;
      fi
      stackAngularProcess "${@:2}"
    elif [[ "${1}" == "git" ]]; then
      # shellcheck disable=SC2145
      # shellcheck disable=SC2068
      dockerComposeCommonRun \
        --volume "${HOME}/.ssh/id_rsa:/home/www-data/.ssh/id_rsa" \
        --volume "${HOME}/.ssh/id_rsa.pub:/home/www-data/.ssh/id_rsa.pub" \
        --volume "${HOME}/.ssh/known_hosts:/home/www-data/.ssh/known_hosts" \
        --volume "${HOME}/.gitconfig:/home/www-data/.gitconfig" \
        --volume "${DIR}/../:/git" \
        git ${@:2};
    elif [[ "${1}" == "all" ]]; then
      if [[ "${2}" == "drop" ]]; then
        dockerDropAll
        exit 0;
      else
        # shellcheck disable=SC2236
        if [ ! -z "${2}" ]; then
          echo "Your choice (${2}) is not allowed !!"
        fi
        echo "You can only choose 'drop' for the moment !"
        exit 1;
      fi
    elif [[ "${1}" == "common" ]]; then
      if [[ "${2}" == "start" ]]; then
        title2 "DockerCompose Common Build & Up"
        dockerComposeCommon "build"
        dockerComposeCommon "up -d"
      else
        # shellcheck disable=SC2236
        if [ ! -z "${2}" ]; then
          echo "Your choice (${2}) is not allowed !!"
        fi
        echo "You can only choose 'start' for the moment !"
        exit 1;
      fi
    elif [[ "${1}" == "help" || "${1}" == "--help" ]]; then
        clear
        cat "${DIR}/help.txt"
    else
      ifChoiceIsEmpty "${2}" "'sf', 'ez', 'agr', 'all', 'common'"
      ifChoiceIsNotAllowed "${2}" "'sf', 'ez', 'agr', 'all', 'common'"
    fi
}

copyGitHooks
chooseStackProcess "${@}"