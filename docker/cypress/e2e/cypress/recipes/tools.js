export function generateUID() {
    // I generate the UID from two parts here
    // to ensure the random number provide enough bits.
    let firstPart = (Math.random() * 46656) | 0;
    let secondPart = (Math.random() * 46656) | 0;
    firstPart = ("000" + firstPart.toString(36)).slice(-3);
    secondPart = ("000" + secondPart.toString(36)).slice(-3);
    return '_' + firstPart + secondPart;
}

Cypress.Commands.add('switchToTab', (name) => {
    cy.get('.nav-link:contains("' + name + '")').click();
    cy.get('.nav-link:contains("' + name + '")').should('have.class', 'active');
});

Cypress.Commands.add('openModal', (buttonSlector) => {
    cy.get(buttonSlector).click();
    cy.wait(1000);
});