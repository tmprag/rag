Cypress.Commands.add('login', (username, password) => {
    cy.visit('http://'+domain+'/login');
    cy.get('.kt-login__container > .kt-login__signin > .kt-form > .input-group > #login_email').click();
    cy.get('.kt-login__container > .kt-login__signin > .kt-form > .input-group > #login_email').type(username);
    cy.get('.kt-login__container > .kt-login__signin > .kt-form > .input-group > #login_password').type(password);
    cy.get('.kt-login__container > .kt-login__signin > .kt-form > .kt-login__actions > #login_submit').click();
});