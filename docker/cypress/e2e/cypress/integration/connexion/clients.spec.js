let index = require('../../recipes/index');

function editUser(name) {
    // # Search User
    cy.visit('http://'+domain+'/company_staff/');
    cy.inputText('company_staff_search_search', name);
    cy.get('.kt-section__content > form > .form-group > .col-lg-2 > .btn-primary').click();

    // # Edit User
    cy.get('tbody > tr > td > a > .flaticon-edit-1').click();
}

describe('clients', function () {
    let uniqId = index.tools.generateUID();
    let name = 'Ferrat' + uniqId;

    beforeEach(() => {
        cy.viewport(1920, 950);
        cy.login('m.lenevez@elemenrh.fr', '2Nh8{X4;f');
    });

    it('create_client_with_note', function () {
        // # Create Client
        cy.visit('http://'+domain+'/company_staff/new');
        cy.inputText('company_staff_firstName', 'Jean');
        cy.inputText('company_staff_lastName', name);
        cy.select2('company_staff_company', 'The TestCompany');
        cy.inputText('company_staff_phoneNumber', '0678906543');
        cy.inputText('company_staff_mobilePhoneNumber', '0765342178');
        cy.select2('company_staff_service', 'Comptabilité');
        cy.select2('company_staff_position', 'Chef de chantier');
        cy.inputText('company_staff_email', 'jean.midas@test.com');
        cy.inputText('company_staff_linkedinUrl', 'http://jeanmidas.linkedin.com');

        // Create note with client
        cy.switchToTab('Notes');

        // Validate
        cy.get('.kt-portlet__foot > .kt-form__actions > .row > .col-lg-6 > .btn-primary').click();

        editUser(name);

        // Assertions
        cy.get('#company_staff_firstName').should('have.value', 'Jean');
        cy.get('#company_staff_lastName').should('have.value', name);
        cy.get('#company_staff_company').should('have.text', 'The TestCompany');
        cy.get('#company_staff_phoneNumber').should('have.value', '678906543');
        cy.get('#company_staff_mobilePhoneNumber').should('have.value', '765342178');
        cy.get('#company_staff_service').should('have.text', 'Comptabilité');
        cy.get('#company_staff_position').should('have.text', 'Chef de chantier');
        cy.get('#company_staff_email').should('have.value', 'jean.midas@test.com');
        cy.get('#company_staff_linkedinUrl').should('have.value', 'http://jeanmidas.linkedin.com');
    });

    it('edit_client_note', function () {
        editUser(name);

        // Add file to note
        cy.switchToTab('Notes');
        cy.openModal('tbody > tr:nth-child(1) > td > .editNote');
        cy.datePicker('note_userDate');
        cy.inputText('note_title', 'Test de note - titre - edit v1');
        cy.inputText('note_details', 'Test de note - détails - edit v1');

        cy.get('#note_modal > .modal-dialog > .modal-content > .modal-footer > #button_note_modal').click();
    });

    it('edit_client_historic', function () {
        editUser(name);

        // Show Historic
        cy.switchToTab('Historique');
    });
});
