// /**
//  *
//  * @param data
//  * @param callback
//  */
// export function addCompany(data, callback) {
//     $.ajax({
//         url: '/api/v1/company/add',
//         type: "POST",
//         dataType: "json",
//         data: data,
//         async: true,
//         success: function (apiResponse) {
//             callback(apiResponse);
//         }
//     });
// }

/**
 *
 * @param callback
 */
export function myResults(callback) {
    $.ajax({
        url: '/api/v1/stats/charts/my-results',
        type: "GET",
        dataType: "json",
        data: {},
        async: true,
        success: function (apiResponse) {
            callback(apiResponse);
        }
    });
}

/**
 *
 * @param callback
 */
export function allResults(callback) {
    $.ajax({
        url: '/api/v1/stats/charts/all-results',
        type: "GET",
        dataType: "json",
        data: {},
        async: true,
        success: function (apiResponse) {
            callback(apiResponse);
        }
    });
}
