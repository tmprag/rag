export const RELOAD_VIEW_STORAGE_FILES_EVENT = 'reloadViewStorageFilesEvent';
export const PULL_STORAGE_FILES_EVENT = 'pullStorageFilesEvent';
export const UPPY_INIT = 'uppyInit';
export const UPPY_RELOAD_VIEW_EVENT = 'uppyReloadViewEvent';

/**
 * Permet de réappliquer le confirm-delete sur des élements apparu dynamiquement
 * @type {string}
 */
export const CONFIRM_DELETE_VIEW_EVENT = 'confirmDeleteViewEvent';

/**
 * Permet de reset la preview d'un candidat (pour switcher le l'un à l'autre par exemple)
 * @type {string}
 */
export const RESET_CANDIDATE_PREVIEW_EVENT = 'resetCandidatePreviewEvent';

/**
 * Permet de reset la preview d'un company staff (pour switcher le l'un à l'autre par exemple)
 * @type {string}
 */
export const RESET_COMPANY_STAFF_PREVIEW_EVENT = 'resetCompanyStaffPreviewEvent';

export const OPEN_PANEL_PREVIEW_TASKS_EVENT = 'openPanelPreviewTasksEvent';
export const CLOSE_PANEL_PREVIEW_TASKS_EVENT = 'closePanelPreviewTasksEvent';

export const OPEN_PANEL_PREVIEW_CANDIDATE_EVENT = 'openPanelPreviewCandidateEvent';
export const CLOSE_PANEL_PREVIEW_CANDIDATE_EVENT = 'closePanelPreviewCandidateEvent';

export const OPEN_PANEL_PREVIEW_COMPANY_STAFF_EVENT = 'openPanelPreviewCompanyStaffEvent';
export const CLOSE_PANEL_PREVIEW_COMPANY_STAFF_EVENT = 'closePanelPreviewCompanyStaffEvent';

export const RELOAD_CALENDAR = 'reloadCalendar';

// Pagination
export const SUFIX_POP = '_pop';
export const SUFIX_LISTEN = '_listen';
export const PAGINATION_AUTOLOAD = 'pagination_autoload';

// Reload view event (by Symfony route name)
export const RELOAD_VIEW_CANDIDATE_GET_NOTES_EVENT = 'api_v1_candidate_get_notes_pagination';
export const RELOAD_VIEW_CANDIDATE_GET_HISTORIC_EVENT = 'api_v1_candidate_get_historic_pagination';
export const RELOAD_VIEW_CANDIDATE_GET_POSITIONS_EVENT = 'api_v1_candidate_get_positions_pagination';
export const RELOAD_VIEW_CANDIDATE_GET_TASKS_EVENT = 'api_v1_candidate_get_tasks_pagination';

export const RELOAD_VIEW_CANDIDATE_GET_CANDIDATE_TRAINING_EVENT = 'api_v1_candidate_training_get_pagination';
export const RELOAD_VIEW_CANDIDATE_GET_CANDIDATE_LANGUAGE_EVENT = 'api_v1_candidate_language_get_pagination';
export const RELOAD_VIEW_CANDIDATE_GET_CANDIDATE_POSITION_EVENT = 'api_v1_candidate_position_get_pagination';

export const RELOAD_VIEW_COMPANY_STAFF_GET_NOTES_EVENT = 'api_v1_company_staff_get_notes_pagination';
export const RELOAD_VIEW_COMPANY_STAFF_GET_HISTORIC_EVENT = 'api_v1_company_staff_get_historic_pagination';
export const RELOAD_VIEW_COMPANY_STAFF_GET_TASKS_EVENT = 'api_v1_company_staff_get_tasks_pagination';
export const RELOAD_VIEW_CUSTOMER_GET_TASKS_EVENT = 'api_v1_customer_get_tasks_pagination';


