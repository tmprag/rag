export const STATUS_SUCCESS = 'success';
export const STATUS_ERROR = 'error';

export const OFFSET_MOD_PAGINATE = 'paginate';
export const OFFSET_MOD_LAZY_LOADING = 'lazyLoading';