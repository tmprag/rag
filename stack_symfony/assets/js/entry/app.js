// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';

import '../../scss/entry/app.scss';

import '../../scss/plugins/font-awesome/css/fontawesome-all.css';
import '../../scss/plugins/themify-icons/themify-icons.css';
import '../../scss/plugins/material-design-iconic-font/css/materialdesignicons.min.css';
import '../../scss/plugins/weather-icons/css/weather-icons.min.css';

import '../plugins/chartist/dist/chartist.min.css';
import '../plugins/jquery/dist/jquery.min';
import '../plugins/popper.js/dist/umd/popper.min.js';
import '../plugins/bootstrap/dist/js/bootstrap.min.js';
import '../plugins/chartist/dist/chartist.min.js';
import '../plugins/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js';

import '../../scss/entry/theme/style.min.css';
import './theme/pages/dashboards/dashboard1';
import './theme/app-style-switcher';
import './theme/waves';
import './theme/sidebarmenu';
import './theme/custom';