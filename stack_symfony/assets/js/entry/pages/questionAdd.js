function addAnswerFormDeleteLink($answerFormLi) {
    var $removeFormButton = $('<button type="button" class="removeQuestion btn btn-danger btn-sm text-white">Supprimer cette réponse</button>');
    $answerFormLi.append($removeFormButton);

    $removeFormButton.on('click', function (e) {
        // remove the li for the answer form
        $answerFormLi.remove();
    });
}

function addAnswerForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your answers field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a answer" link li
    var $newFormLi = $('<li class="answer alert alert-primary" role="alert"></li>').append(newForm);
    $newLinkLi.before($newFormLi);

    // add a delete link to the new form
    addAnswerFormDeleteLink($newFormLi);
}

var $collectionHolder;

// setup an "add a answer" link
var $addAnswerButton = $('<button type="button" class="add_answer_link btn btn-success">Ajouter une réponse</button>&nbsp;' +
    '<button type="button" class="add_2 btn btn-success">Ajouter 2 réponses</button>&nbsp;' +
    '<button type="button" class="add_4 btn btn-success">Ajouter 4 réponses</button>&nbsp;' +
    '<button type="button" class="add_yes_no btn btn-success">Ajouter des réponses (oui/non)</button>&nbsp;' +
    '<button type="button" class="add_true_false btn btn-success">Ajouter des réponses (vrai/faux)</button>');
var $newLinkLi = $('<li></li>').append($addAnswerButton);

jQuery(document).ready(function () {
    // Get the ul that holds the collection of answers
    $collectionHolder = $('ul.answers');

    // add the "add a answer" anchor and li to the answers ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $('.add_answer_link').on('click', function (e) {
        // add a new answer form (see next code block)
        addAnswerForm($collectionHolder, $newLinkLi);
    });
});

jQuery(document).ready(function () {
    // Get the ul that holds the collection of answers
    $collectionHolder = $('ul.answers');

    // add a delete link to all of the existing answer form li elements
    $collectionHolder.find('li.answer.alert').each(function () {
        addAnswerFormDeleteLink($(this));
    });

    // ... the rest of the block from above

    $('.add_2').click(function () {
        $('.add_answer_link').click();
        $('.add_answer_link').click();
    });

    $('.add_true_false').click(function () {
        $('.add_answer_link').click();
        $('.add_answer_link').click();

        $('#question_answers_0_sentence').val('Vrai');
        $('#question_answers_1_sentence').val('Faux');
    });

    $('.add_yes_no').click(function () {
        $('.add_answer_link').click();
        $('.add_answer_link').click();

        $('#question_answers_0_sentence').val('Oui');
        $('#question_answers_1_sentence').val('Non');
    });

    $('.add_4').click(function () {
        $('.add_answer_link').click();
        $('.add_answer_link').click();
        $('.add_answer_link').click();
        $('.add_answer_link').click();
    });

    keepThematic();
});

function keepThematic() {
    let localStorageKey = 'questionAdd_thematic';

    if (localStorage.getItem(localStorageKey) === null) {
        localStorage.setItem(localStorageKey, '11');
    }

    $('#question_thematic').val(localStorage.getItem(localStorageKey));

    $('#question_thematic').change(function () {
        localStorage.setItem(localStorageKey, $(this).val());
    });
}