let Chart = require('chart.js');
let chartsApi = require('../../ajax/charts');

jQuery(document).ready(function () {

    chartsApi.allResults(function (allResultsApiResponse) {
        chartsApi.myResults(function (myResultsApiResponse) {
            console.log(allResultsApiResponse.data.allResults);

            let config = {
                type: 'line',
                data: {
                    labels: Object.values(myResultsApiResponse.data.labels),
                    datasets: [
                        {
                            label: 'Ma note chaque jour',
                            backgroundColor: '#2ca4ff',
                            borderColor: '#2ca4ff',
                            data: Object.values(myResultsApiResponse.data.myResults),
                            fill: false,
                        },
                        {
                            label: 'Ma moyenne chaque jour',
                            fill: false,
                            backgroundColor: '#ffb13c',
                            borderColor: '#ffb13c',
                            data: Object.values(myResultsApiResponse.data.myAveragesResults),
                        },
                        {
                            label: 'Nos notes (de tous les membres) chaque jour',
                            backgroundColor: '#10c77b',
                            borderColor: '#10c77b',
                            data: Object.values(allResultsApiResponse.data.allResults),
                            fill: false,
                        },
                        {
                            label: 'Nos moyennes (de tous les membres) chaque jour',
                            fill: false,
                            backgroundColor: '#b257e0',
                            borderColor: '#b257e0',
                            data: Object.values(allResultsApiResponse.data.allAveragesResults),
                        }
                    ]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Mes courbes de mes 24 derniers Quizz'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Scéances de Quizz'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Notes / Moyennes'
                            }
                        }]
                    }
                }
            };

            let ctx = $('#chartOne');
            let chartOne = new Chart(ctx, config);
        });
    });
});