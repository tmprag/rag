<?php
/**
 * © Project
 */

namespace App\Tests\Functional\Base;

use App\Entity\Interfaces\EntityInterface;
use App\Models\Constant\Env;
use App\Tests\Base\AbstractTestCase;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\Mapping\MappingException;
use Doctrine\Persistence\ObjectRepository;
use PDOException;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class AbstractFunctionalTestCase
 */
class AbstractFunctionalTestCase extends AbstractTestCase
{
    /**
     * @var KernelBrowser
     */
    protected $client;

    /* @var bool */
    protected static $shouldBootShutdownKernel = true;

    /**
     * @return string
     */
    protected function getEntryPoint(): string
    {
        return 'http://nginx:8080';
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->preload();

        $this->client = static::createClient();

        if (false === static::$booted) {
            self::bootKernel();
        }

        $this->getConnection()->connect();
        $this->getConnection()->beginTransaction();

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        if ($this->getConnection()->isTransactionActive()) {
            try {
                if (true === \filter_var(\getenv('ROLLBACK_MOD'), FILTER_VALIDATE_BOOLEAN)) {
                    $this->getConnection()->rollBack();
                } else {
                    $this->getConnection()->commit();
                }
            } catch (PDOException $exception) {
                die($exception->getMessage());
            } catch (ConnectionException $exception) {
                die($exception->getMessage());
            } catch (MappingException $exception) {
                die($exception->getMessage());
            } finally {
                $this->entityManager()->clear();
            }
        }

        $this->getConnection()->close();
        $this->entityManager()->close();

        if (self::$shouldBootShutdownKernel) {
            parent::tearDown();
        }
    }

    /**
     * @return KernelInterface
     */
    public function getKernel()
    {
        return $this->client->getKernel();
    }

    /** @return ContainerInterface */
    public function getContainer()
    {
        return self::getKernel()->getContainer();
    }

    /**
     * @param $service
     *
     * @return mixed
     */
    public function service($service)
    {
        return self::getContainer()->get($service);
    }

    /**
     * @param $service
     *
     * @return mixed
     */
    public function privateService($service)
    {
        return self::getContainer()->get('test.service_container')->get($service);
    }

    /**
     * @return EntityManagerInterface
     */
    public function entityManager(): EntityManagerInterface
    {
        return $this->service('doctrine')->getManager();
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->entityManager()->getConnection();
    }

    /**
     * @param $class
     *
     * @return ObjectRepository
     */
    public function repository($class)
    {
        return $this->entityManager()->getRepository($class);
    }

    /**
     * @param EntityInterface $entity
     *
     * @return EntityInterface
     */
    protected function save(EntityInterface $entity): EntityInterface
    {
        $this->entityManager()->persist($entity);
        $this->entityManager()->flush();
        $this->entityManager()->refresh($entity);

        return $entity;
    }

    /**
     * @param array|EntityInterface[] $entities
     *
     * @throws EntityNotFoundException
     *
     * @return array|EntityInterface[]
     */
    protected function saveMany(array $entities = []): array
    {
        foreach ($entities as $entity) {
            if (!$entity instanceof EntityInterface) {
                throw new EntityNotFoundException('One object (' . \get_class($entity) . ') in save() function has not implement ' . EntityInterface::class);
            }

            $this->entityManager()->persist($entity);
        }

        $this->entityManager()->flush();

        foreach ($entities as $entity) {
            $this->entityManager()->refresh($entity);
        }

        return $entities;
    }

    protected function writeContentInFileIfHasAnError()
    {
        if (Response::HTTP_INTERNAL_SERVER_ERROR === $this->client->getResponse()->getStatusCode()) {
            $filename = __DIR__ . '/../var/debug_' . \date('Y-m-d_H:i:s') . '.html';
            \file_put_contents($filename, $this->client->getResponse()->getContent());
            dump($filename);
            exit;
        }
    }

    /**
     * For each file test extends AbstractTestCase, check if ENV is test before !
     */
    public function testEnv(): void
    {
        $this->assertEquals(Env::TEST, $_SERVER['APP_ENV']);
        $this->assertStringContainsString('_test', $_SERVER['DATABASE_NAME']);

        $params = $this->getConnection()->getParams();

        $this->assertStringContainsString('_test', $params['url']);
        $this->assertStringContainsString('_test', $params['dbname']);
    }
}
