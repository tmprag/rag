<?php
/**
 * © Project
 */

namespace App\Tests\Functional\Base;

use App\Models\Constant\Test;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AbstractFunctionalApiTestCase
 */
class AbstractFunctionalApiTestCase extends AbstractFunctionalTestCase
{
    /**
     * @return string
     */
    protected function getEntryPoint(): string
    {
        return parent::getEntryPoint() . '/api';
    }

    /**
     * @param string $route
     * @param string $username
     * @param string $password
     *
     * @return array
     */
    public function apiGet(string $route, $username = Test::USER_1_USERNAME, $password = Test::USER_1_USERNAME): array
    {
        $token = $this->getJwtToken($username, $password);

        $client   = new GuzzleClient();
        $response = $client->get($this->getEntryPoint() . $route, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Authorization' => $token,
                'Accept' => 'application/json',
            ],
        ]);

        $content = $response->getBody()->getContents();

        return \json_decode($content, true);
    }

    /**
     * @param string $route
     * @param array  $postData
     * @param string $username
     * @param string $password
     *
     * @return array
     */
    public function apiPost(string $route, array $postData = [], $username = Test::USER_1_USERNAME, $password = Test::USER_1_USERNAME): array
    {
        $token = $this->getJwtToken($username, $password);

        $client   = new GuzzleClient();
        $response = $client->post($this->getEntryPoint() . $route, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Authorization' => $token,
                'Accept' => 'application/json',
            ],
            'body' => $postData,
        ]);

        $content = $response->getBody()->getContents();

        return \json_decode($content, true);
    }

    /**
     * @param string $route
     * @param array  $putData
     * @param string $username
     * @param string $password
     *
     * @return array
     */
    public function apiPut(string $route, array $putData = [], $username = Test::USER_1_USERNAME, $password = Test::USER_1_USERNAME): array
    {
        $token = $this->getJwtToken($username, $password);

        $client   = new GuzzleClient();
        $response = $client->put($this->getEntryPoint() . $route, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Authorization' => $token,
                'Accept' => 'application/json',
            ],
            'body' => $putData,
        ]);

        $content = $response->getBody()->getContents();

        return \json_decode($content, true);
    }

    /**
     * @param string $route
     * @param array  $patchData
     * @param string $username
     * @param string $password
     *
     * @return array
     */
    public function apiPatch(string $route, array $patchData = [], $username = Test::USER_1_USERNAME, $password = Test::USER_1_USERNAME): array
    {
        $token = $this->getJwtToken($username, $password);

        $client   = new GuzzleClient();
        $response = $client->patch($this->getEntryPoint() . $route, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Authorization' => $token,
                'Accept' => 'application/json',
            ],
            'body' => $patchData,
        ]);

        $content = $response->getBody()->getContents();

        return \json_decode($content, true);
    }

    /**
     * @param string $route
     * @param string $username
     * @param string $password
     *
     * @return array
     */
    public function apiDelete(string $route, $username = Test::USER_1_USERNAME, $password = Test::USER_1_USERNAME): array
    {
        $token = $this->getJwtToken($username, $password);

        $client   = new GuzzleClient();
        $response = $client->delete($this->getEntryPoint() . $route, [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'Authorization' => $token,
                'Accept' => 'application/json',
            ],
        ]);

        $content = $response->getBody()->getContents();

        return \json_decode($content, true);
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return string
     */
    protected function getJwtToken($username = Test::USER_1_USERNAME, $password = Test::USER_1_USERNAME): string
    {
        $this->client->request(
            Request::METHOD_POST,
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            \json_encode([
                'username' => $username,
                'password' => $password,
            ])
        );

        $data = \json_decode($this->client->getResponse()->getContent(), true);

        $this->ifUnauthorized($username, $password);
        $this->writeContentInFileIfHasAnError();

        if (!isset($data['token'])) {
            throw new NotFoundHttpException('token not found !');
        }

        return \sprintf('Bearer %s', $data['token']);
    }

    /**
     * @param string $username
     * @param string $password
     */
    protected function ifUnauthorized($username = Test::USER_1_USERNAME, $password = Test::USER_1_USERNAME)
    {
        if (Response::HTTP_UNAUTHORIZED === $this->client->getResponse()->getStatusCode()) {
            dump(Response::HTTP_UNAUTHORIZED . ' : HTTP UNAUTHORIZED with user ' . $username . ' and password ' . $password);
            exit;
        }
    }
}
