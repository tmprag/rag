<?php
/**
 * © Project
 */

namespace App\Tests\Functional\Auth;

use App\Models\Constant\Test;
use App\Tests\Functional\Base\AbstractFunctionalApiTestCase;
use App\Tests\Unit\Base\Mocks\RequestMockTrait;

/**
 * Class JwtAuthTest
 */
class JwtAuthTest extends AbstractFunctionalApiTestCase
{
    use RequestMockTrait;

    public function testGetQuestions()
    {
        $json = $this->apiGet('/thematics?page=1', Test::USER_1_USERNAME, Test::USER_1_USERNAME);

        //dump($json);

        $this->assertNotNull($json);
        $this->assertNotEmpty($json);
    }
}
