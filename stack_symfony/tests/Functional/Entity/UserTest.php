<?php
/**
 * © Project
 */

namespace App\Tests\Functional\Entity;

use App\Entity\Interfaces\EntityInterface;
use App\Tests\Functional\Base\AbstractFunctionalTestCase;
use App\Tests\Unit\Base\Factory\AbstractFactory;
use App\Utils\Classes;
use App\Utils\Interfaces;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;

/**
 * Class UserTest
 */
class UserTest extends AbstractFunctionalTestCase
{
    /**
     * @param object $object
     *
     * @return array
     */
    private function getMethodsGrouped(object $object): array
    {
        $methodsGroup['getters']  = [];
        $methodsGroup['setters']  = [];
        $methodsGroup['hasers']   = [];
        $methodsGroup['adders']   = [];
        $methodsGroup['removers'] = [];

        foreach (\get_class_methods($object) as $method) {
            if (0 === \mb_strpos($method, 'get')) {
                $methodsGroup['getters'][] = $method;
            }

            if (0 === \mb_strpos($method, 'set')) {
                $methodsGroup['setters'][] = $method;
            }

            if (0 === \mb_strpos($method, 'has')) {
                $methodsGroup['hasers'][] = $method;
            }

            if (0 === \mb_strpos($method, 'add')) {
                $methodsGroup['adders'][] = $method;
            }

            if (0 === \mb_strpos($method, 'remove')) {
                $methodsGroup['removers'][] = $method;
            }
        }

        return $methodsGroup;
    }

    /**
     * @param string $type
     *
     * @throws ReflectionException
     *
     * @return array|bool|DateTime|int|string|null
     */
    private function fakeDataByTypeHint(string $type)
    {
        if ('string' === $type) {
            return $this->getFaker()->sentence;
        }
        if ('array' === $type) {
            return [
                $this->getFaker()->sentence,
                $this->getFaker()->sentence,
                $this->getFaker()->sentence,
            ];
        }
        if ('DateTime' === $type) {
            return new DateTime('now');
        }
        if ('int' === $type || 'integer' === $type) {
            return $this->getFaker()->randomNumber();
        }
        if ('bool' === $type || 'boolean' === $type) {
            return $this->getFaker()->boolean;
        }
        if (ArrayCollection::class === $type) {
            $arrayCollection = new ArrayCollection();

            $arrayCollection->add($this->getFaker()->sentence);
            $arrayCollection->add($this->getFaker()->sentence);
            $arrayCollection->add($this->getFaker()->sentence);

            return $arrayCollection;
        }
        if (\mb_stristr($type, 'Interface')) {
            $classes = Interfaces::getClassesImplementsInterface($type);
            if (!empty($classes) && \is_array($classes)) {
                $type = $classes[0];
            }
        }

        return new $type();
    }

    /**
     * @param string $setterName
     *
     * @return string
     */
    private function convertSetterToGetter(string $setterName): string
    {
        return 'get' . \preg_replace('/^set/', '', $setterName);
    }

    /**
     * @param string $adderName
     *
     * @return string
     */
    private function convertAdderToGetter(string $adderName): string
    {
        return 'get' . \preg_replace('/^add/', '', $adderName) . 's';
    }

    /**
     * @param string $adderName
     *
     * @return string
     */
    private function convertAdderToSetter(string $adderName): string
    {
        return 'set' . \preg_replace('/^add/', '', $adderName) . 's';
    }

    /**
     * @param string $removerName
     *
     * @return string
     */
    private function convertRemoverToGetter(string $removerName): string
    {
        return 'get' . \preg_replace('/^remove/', '', $removerName) . 's';
    }

    /**
     * @param string $removerName
     *
     * @return string
     */
    private function convertRemoverToAdder(string $removerName): string
    {
        return 'add' . \preg_replace('/^remove/', '', $removerName);
    }

    /**
     * @param string $haserName
     *
     * @return string
     */
    private function convertHaserToGetter(string $haserName): string
    {
        return 'get' . \preg_replace('/^has/', '', $haserName) . 's';
    }

    /**
     * @param string $haserName
     *
     * @return string
     */
    private function convertHaserToSetter(string $haserName): string
    {
        return 'set' . \preg_replace('/^has/', '', $haserName) . 's';
    }

    /**
     * @param string $haserName
     *
     * @return string
     */
    private function convertHaserToAdder(string $haserName): string
    {
        return 'add' . \preg_replace('/^has/', '', $haserName);
    }

    /**
     * @param array           $methods
     * @param EntityInterface $object
     */
    private function dynamicallyTestEntitiesGettersAndSetters(array $methods, EntityInterface $object)
    {
        // Dynamic test getters and setters
        foreach ($methods as $setter) {
            if ('set' === $setter) {
                continue;
            }

            try {
                $class  = new ReflectionClass($object);
                $method = $class->getMethod($setter);

                $parameters = [];

                foreach ($method->getParameters() as $parameter) {
                    /** @var ReflectionNamedType $type */
                    $type = $parameter->getType();

                    if (null === $type) {
                        throw new \TypeError('The parameter ' . $parameter->getName() . ' ont class ' . $class->getName() . ' doesn\'t have TypeHint !');
                    }

                    $parameters[] = $this->fakeDataByTypeHint($type->getName());
                }

                \call_user_func_array([$object, $setter], $parameters);
                $getter = $this->convertSetterToGetter($setter);
                $result = \call_user_func_array([$object, $getter], []);

                $this->assertEquals($parameters[0], $result);
            } catch (ReflectionException $e) {
                echo $e->getMessage();
                exit;
            }
        }
    }

    /**
     * @param array           $methods
     * @param EntityInterface $object
     */
    private function dynamicallyTestEntitiesAdders(array $methods, EntityInterface $object)
    {
        foreach ($methods as $adder) {
            if ('add' === $adder) {
                continue;
            }

            try {
                $class = new ReflectionClass($object);

                $getter = $this->convertAdderToGetter($adder);
                $setter = $this->convertAdderToSetter($adder);

                $this->clearCollection($class, $setter, $object);

                $result = \call_user_func_array([$object, $getter], []);
                $this->assertCount(0, $result);

                $numberOfObjects = $this->getFaker()->numberBetween(1, 5);

                for ($i = 0; $i < $numberOfObjects; ++$i) {
                    $parameters = [];
                    foreach ($class->getMethod($adder)->getParameters() as $parameter) {
                        /** @var ReflectionNamedType $type */
                        $type = $parameter->getType();

                        if (null === $type) {
                            throw new \TypeError('The parameter ' . $parameter->getName() . ' ont class ' . $class->getName() . ' doesn\'t have TypeHint !');
                        }

                        $parameters[] = $this->fakeDataByTypeHint($type->getName());
                    }

                    \call_user_func_array([$object, $adder], $parameters);
                }

                $result = \call_user_func_array([$object, $getter], []);
                $this->assertEquals($numberOfObjects, \count($result));
            } catch (ReflectionException $e) {
                echo $e->getMessage();
                exit;
            }
        }
    }

    /**
     * @param array           $methods
     * @param EntityInterface $object
     */
    private function dynamicallyTestEntitiesRemovers(array $methods, EntityInterface $object)
    {
        foreach ($methods as $remover) {
            if ('remove' === $remover) {
                continue;
            }

            try {
                $class  = new ReflectionClass($object);
                $method = $class->getMethod($remover);

                $getter          = $this->convertRemoverToGetter($remover);
                $adder           = $this->convertRemoverToAdder($remover);
                $resultBeforeAdd = \call_user_func_array([$object, $getter], []);
                $this->assertEquals(0, $resultBeforeAdd->count());

                $numberOfObjects = $this->getFaker()->numberBetween(1, 5);

                // Add
                for ($i = 0; $i < $numberOfObjects; ++$i) {
                    $parameters = [];
                    foreach ($method->getParameters() as $parameter) {
                        /** @var ReflectionNamedType $type */
                        $type = $parameter->getType();

                        if (null === $type) {
                            throw new \TypeError('The parameter ' . $parameter->getName() . ' ont class ' . $class->getName() . ' doesn\'t have TypeHint !');
                        }

                        $className = $type->getName();
                        $entity    = new $className();

                        $parameters[] = $entity;
                    }

                    \call_user_func_array([$object, $adder], $parameters);
                }

                $resultAfterAdd = \call_user_func_array([$object, $getter], []);
                $this->assertEquals($numberOfObjects, $resultAfterAdd->count());

                // Remove
                foreach ($resultAfterAdd as $item) {
                    \call_user_func_array([$object, $remover], [$item]);
                }

                $resultAfterRemove = \call_user_func_array([$object, $getter], []);
                $this->assertEquals(0, $resultAfterRemove->count());
            } catch (ReflectionException $e) {
                echo $e->getMessage();
                exit;
            }
        }
    }

    /**
     * @param array           $methods
     * @param EntityInterface $object
     */
    private function dynamicallyTestEntitiesHasers(array $methods, EntityInterface $object)
    {
        foreach ($methods as $harser) {
            if ('has' === $harser) {
                continue;
            }

            try {
                $class = new ReflectionClass($object);

                $getter = $this->convertHaserToGetter($harser);
                $setter = $this->convertHaserToSetter($harser);
                $adder  = $this->convertHaserToAdder($harser);

                $this->clearCollection($class, $setter, $object);

                $resultBeforeAdd = \call_user_func_array([$object, $getter], []);
                $this->assertCount(0, $resultBeforeAdd);

                $numberOfObjects = $this->getFaker()->numberBetween(1, 5);

                // Add
                for ($i = 0; $i < $numberOfObjects; ++$i) {
                    $parameters = [];
                    foreach ($class->getMethod($harser)->getParameters() as $parameter) {
                        /** @var ReflectionNamedType $type */
                        $type = $parameter->getType();

                        if (null === $type) {
                            throw new \TypeError('The parameter ' . $parameter->getName() . ' ont class ' . $class->getName() . ' doesn\'t have TypeHint !');
                        }

                        $parameters[] = $this->fakeDataByTypeHint($type->getName());
                    }

                    \call_user_func_array([$object, $adder], $parameters);
                }

                $resultAfterAdd = \call_user_func_array([$object, $getter], []);
                $this->assertEquals($numberOfObjects, \count($resultAfterAdd));

                // Remove
                foreach ($resultAfterAdd as $item) {
                    $resultAfterHas = \call_user_func_array([$object, $harser], [$item]);
                    $this->assertEquals(true, $resultAfterHas);
                }
            } catch (ReflectionException $e) {
                echo $e->getMessage();
                exit;
            }
        }
    }

    /**
     * @param ReflectionClass $class
     * @param string          $methodName
     * @param EntityInterface $object
     *
     * @throws ReflectionException
     *
     * @return EntityInterface
     */
    public function clearCollection(ReflectionClass $class, string $methodName, EntityInterface $object): EntityInterface
    {
        /** @var ReflectionNamedType $type */
        $type = $class->getMethod($methodName)->getParameters()[0]->getType();

        switch ($type->getName()) {
            case 'array':
                \call_user_func_array([$object, $methodName], [[]]);
                break;
            case ArrayCollection::class:
                \call_user_func_array([$object, $methodName], [new ArrayCollection()]);
                break;
        }

        return $object;
    }

    /**
     * @param EntityInterface $object
     */
    private function dynamicallyTestEntitiesFunctions(EntityInterface $object)
    {
        $methodsGroup = $this->getMethodsGrouped($object);

        $this->dynamicallyTestEntitiesGettersAndSetters($methodsGroup['setters'], clone $object);
        $this->dynamicallyTestEntitiesRemovers($methodsGroup['removers'], clone $object);
        $this->dynamicallyTestEntitiesAdders($methodsGroup['adders'], clone $object);
        $this->dynamicallyTestEntitiesHasers($methodsGroup['hasers'], clone $object);
    }

    /**
     * @throws Exception
     */
    public function testEntities()
    {
        foreach (Classes::getEntities($this->entityManager()) as $entityName) {
            parent::tearDown();
            parent::setUp();

            /** @var AbstractFactory $entityFactory */
            $entityFactory = $this->service(Classes::getFactoryTestFromClassName($entityName));

            /** @var EntityInterface $entity */
            $entity = $entityFactory->seed();

            $flushed = $this->save($entity);
            $this->entityManager()->refresh($entity);
            $this->dynamicallyTestEntitiesFunctions($flushed);
        }
    }
}
