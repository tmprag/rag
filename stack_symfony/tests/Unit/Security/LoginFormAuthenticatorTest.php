<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Security;

use App\Entity\User;
use App\Models\Constant\Role;
use App\Models\Constant\Routes;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use App\Tests\Unit\Base\AbstractUnitTestCase;
use Doctrine\ORM\EntityManager;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionException;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

/**
 * Class LoginFormAuthenticatorTest
 */
class LoginFormAuthenticatorTest extends AbstractUnitTestCase
{
    /**
     * @var EntityManager|MockObject
     */
    public $entityManagerMock;

    /**
     * @var MockObject|UrlGenerator
     */
    public $urlGeneratorMock;

    /**
     * @var CsrfTokenManager|MockObject
     */
    public $csrfTokenManagerMock;

    /**
     * @var MockObject|UserPasswordEncoder
     */
    public $userPasswordEncoderMock;

    /**
     * @var MockObject|Router
     */
    public $routerMock;

    /**
     * @throws ReflectionException
     */
    public function setUp(): void
    {
        parent::setUp();

        // Dependency injection of __construct()
        $this->entityManagerMock       = $this->createMock(EntityManager::class);
        $this->urlGeneratorMock        = $this->createMock(UrlGenerator::class);
        $this->csrfTokenManagerMock    = $this->createMock(CsrfTokenManager::class);
        $this->userPasswordEncoderMock = $this->createMock(UserPasswordEncoder::class);
        $this->routerMock              = $this->createMock(Router::class);

        // Class to test
        $this->classToTest = new LoginFormAuthenticator(
            $this->entityManagerMock,
            $this->urlGeneratorMock,
            $this->csrfTokenManagerMock,
            $this->userPasswordEncoderMock,
            $this->routerMock
        );
    }

    /**
     * @return array[]
     */
    public function providerSupports(): array
    {
        parent::preload();

        return [
            [
                'request_route' => 'security_login',
                'request_isMethod_POST' => true,
                'expectedResult' => true,
            ],
            [
                'request_route' => 'security_not_exist',
                'request_isMethod_POST' => true,
                'expectedResult' => false,
            ],
            [
                'request_route' => 'security_login',
                'request_isMethod_POST' => false,
                'expectedResult' => false,
            ],
            [
                'request_route' => 'security_not_exist',
                'request_isMethod_POST' => false,
                'expectedResult' => false,
            ],
        ];
    }

    /**
     * @dataProvider providerSupports
     *
     * @param string $requestRoute
     * @param bool   $requestIsMethodPost
     * @param bool   $expectedResult
     *
     * @throws ReflectionException
     */
    public function testSupports(string $requestRoute, bool $requestIsMethodPost, bool $expectedResult): void
    {
        $parameterBagMock = $this->createMock(ParameterBag::class);
        $parameterBagMock->method('get')->with('_route')->willReturn($requestRoute);

        $requestMock             = $this->createMock(Request::class);
        $requestMock->attributes = $parameterBagMock;
        $requestMock->method('isMethod')->with('POST')->willReturn($requestIsMethodPost);

        $result = $this->classToTest->supports($requestMock);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array[]
     */
    public function providerGetCredentials(): array
    {
        parent::preload();

        $email     = $this->getFaker()->email;
        $password  = $this->getFaker()->password;
        $csrfToken = \base64_encode($this->getFaker()->word);

        return [
            [
                'email' => $email,
                'password' => $password,
                '_csrf_token' => $csrfToken,
                'expectedResult' => [
                    'email' => $email,
                    'password' => $password,
                    'csrf_token' => $csrfToken,
                ],
            ],
        ];
    }

    /**
     * @dataProvider providerGetCredentials
     *
     * @param string $email
     * @param string $password
     * @param string $crsfToken
     * @param array  $expectedResult
     *
     * @throws ReflectionException
     */
    public function testGetCredentials(string $email, string $password, string $crsfToken, array $expectedResult = []): void
    {
        $sessionMock = $this->createMock(Session::class);
        $sessionMock->method('set')->with(Security::LAST_USERNAME, $email);

        $requestMock          = $this->createMock(Request::class);
        $requestMock->request = $this->createMock(ParameterBag::class);
        $requestMock->method('getSession')->willReturn($sessionMock);

        // http://pietervogelaar.nl/phpunit-how-to-mock-multiple-calls-to-the-same-method
        $requestMock->request
            ->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->willReturnCallback(static function ($param) use ($email, $password, $crsfToken) {
                switch ($param) {
                    default:
                        return 'none';
                    case 'email':
                        return $email;
                    case 'password':
                        return $password;
                    case '_csrf_token':
                        return $crsfToken;
                }
            });

        $result = $this->classToTest->getCredentials($requestMock);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array[]
     */
    public function providerGetUser(): array
    {
        parent::preload();

        $csrfToken = \base64_encode($this->getFaker()->word);
        $user      = (new User())
            ->setEmail($this->getFaker()->email)
            ->setFirstName($this->getFaker()->firstName)
            ->setLastName($this->getFaker()->lastName);

        return [
            [
                'credentials' => [
                    'email' => $user->getEmail(),
                    'csrf_token' => $csrfToken,
                ],
                'user' => $user,
                'expectedResult' => $user,
            ],
        ];
    }

    /**
     * @dataProvider providerGetUser
     *
     * @param array $credentials
     * @param User  $user
     * @param $expectedResult
     *
     * @throws ReflectionException
     */
    public function testGetUser(array $credentials, User $user, $expectedResult)
    {
        $userProviderInterfaceMock = $this->createMock(UserProviderInterface::class, false);

        $this->csrfTokenManagerMock->method('isTokenValid')->with($credentials['csrf_token'])->willReturn(true);

        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->method('findOneBy')->with(['email' => $credentials['email']])->willReturn($user);
        $this->entityManagerMock->method('getRepository')->with(User::class)->willReturn($userRepositoryMock);

        $result = $this->classToTest->getUser($credentials, $userProviderInterfaceMock);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider providerGetUser
     *
     * @param $credentials
     *
     * @throws ReflectionException
     */
    public function testGetUserInvalidCsrfTokenException(array $credentials = [])
    {
        $this->expectException(InvalidCsrfTokenException::class);

        $userProviderInterfaceMock = $this->createMock(UserProviderInterface::class, false);
        $this->csrfTokenManagerMock->method('isTokenValid')->with($credentials['csrf_token'])->willReturn(false);

        $this->classToTest->getUser($credentials, $userProviderInterfaceMock);
    }

    /**
     * @dataProvider providerGetUser
     *
     * @param $credentials
     *
     * @throws ReflectionException
     */
    public function testGetUserCustomUserMessageAuthenticationException(array $credentials = [])
    {
        $this->expectException(CustomUserMessageAuthenticationException::class);

        $userProviderInterfaceMock = $this->createMock(UserProviderInterface::class, false);
        $this->csrfTokenManagerMock->method('isTokenValid')->with($credentials['csrf_token'])->willReturn(true);

        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->method('findOneBy')->with(['email' => $credentials['email']])->willReturn(false);
        $this->entityManagerMock->method('getRepository')->with(User::class)->willReturn($userRepositoryMock);

        $this->classToTest->getUser($credentials, $userProviderInterfaceMock);
    }

    /**
     * @return array[]
     */
    public function providerCheckCredentials(): array
    {
        parent::preload();

        $password = $this->getFaker()->password;
        $user     = (new User())
            ->setEmail($this->getFaker()->email)
            ->setFirstName($this->getFaker()->firstName)
            ->setLastName($this->getFaker()->lastName);

        return [
            [
                'credentials' => [
                    'password' => $password,
                ],
                'user' => $user,
                'isPasswordValid' => true,
                'expectedResult' => true,
            ],
            [
                'credentials' => [
                    'password' => $password,
                ],
                'user' => $user,
                'isPasswordValid' => false,
                'expectedResult' => false,
            ],
        ];
    }

    /**
     * @dataProvider providerCheckCredentials
     *
     * @param array $credentials
     * @param User  $user
     * @param bool  $isPasswordValid
     * @param bool  $expectedResult
     */
    public function testCheckCredentials(array $credentials, User $user, bool $isPasswordValid, bool $expectedResult)
    {
        $this->userPasswordEncoderMock->method('isPasswordValid')->with($user, $credentials['password'])->willReturn($isPasswordValid);

        $result = $this->classToTest->checkCredentials($credentials, $user);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array[]
     */
    public function providerGetPassword(): array
    {
        parent::preload();

        $password = $this->getFaker()->password;

        return [
            [
                'credentials' => [
                    'password' => $password,
                ],
                'expectedResult' => $password,
            ],
        ];
    }

    /**
     * @dataProvider providerGetPassword
     *
     * @param array $credentials
     * @param $expectedResult
     */
    public function testGetPassword(array $credentials, $expectedResult)
    {
        $result = $this->classToTest->getPassword($credentials);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array[]
     */
    public function providerOnAuthenticationSuccess(): array
    {
        parent::preload();

        $admin = (new User())
            ->setRoles([Role::ROLE_ADMIN])
            ->setEmail($this->getFaker()->email)
            ->setFirstName($this->getFaker()->firstName)
            ->setLastName($this->getFaker()->lastName);

        $user = (new User())
            ->setRoles([Role::ROLE_USER])
            ->setEmail($this->getFaker()->email)
            ->setFirstName($this->getFaker()->firstName)
            ->setLastName($this->getFaker()->lastName);

        return [
            [
                'user' => $admin,
                'providerKey' => 'app_user_provider',
                'targetPath' => null,
                'expectedResult' => '/admin/',
            ],
            [
                'user' => $user,
                'providerKey' => 'app_user_provider',
                'targetPath' => null,
                'expectedResult' => '/dashboard/pages/',
            ],
            [
                'user' => $user,
                'providerKey' => 'app_user_provider',
                'targetPath' => '/example',
                'expectedResult' => '/example',
            ],
        ];
    }

    /**
     * @dataProvider providerOnAuthenticationSuccess
     *
     * @param User        $user
     * @param string      $providerKey
     * @param string|null $targetPath
     * @param $expectedResult
     *
     * @throws ReflectionException
     * @throws Exception
     */
    public function testOnAuthenticationSuccess(User $user, string $providerKey, ?string $targetPath, $expectedResult)
    {
        $sessionMock = $this->createMock(Session::class);
        $sessionMock->method('get')->with('_security.' . $providerKey . '.target_path')->willReturn($targetPath);
        $requestMock = $this->createMock(Request::class);
        $requestMock->method('getSession')->willReturn($sessionMock);

        $tokenMock = $this->createMock(TokenInterface::class, false);
        $tokenMock->method('getUser')->willReturn($user);

        $this->routerMock
            ->expects($this->any())
            ->method('generate')
            ->with($this->anything())
            ->willReturnCallback(static function ($param) {
                switch ($param) {
                    default:
                        return 'none';
                    case Routes::ADMIN_INDEX:
                        return '/admin/';
                    case Routes::DASHBOARD_INDEX:
                        return '/dashboard/pages/';
                }
            });

        /** @var RedirectResponse $result */
        $result = $this->classToTest->onAuthenticationSuccess($requestMock, $tokenMock, $providerKey);

        $this->assertEquals($expectedResult, $result->getTargetUrl());
    }

    /**
     * @return array[]
     */
    public function providerGetLoginUrl(): array
    {
        return [
            [
                'expectedResult' => '/login',
            ],
        ];
    }

    /**
     * @dataProvider providerGetLoginUrl
     *
     * @param $expectedResult
     *
     * @throws ReflectionException
     */
    public function testGetLoginUrl($expectedResult)
    {
        $this->urlGeneratorMock->method('generate')->with(Routes::LOGIN)->willReturn('/login');

        $privateMethod = $this->getPrivateMethod(LoginFormAuthenticator::class, 'getLoginUrl');
        $result        = $privateMethod->invokeArgs($this->classToTest, []);

        $this->assertEquals($expectedResult, $result);
    }
}
