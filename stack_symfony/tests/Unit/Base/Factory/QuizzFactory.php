<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Factory;

use App\Entity\Quizz;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class QuizzFactory
 */
class QuizzFactory extends AbstractFactory
{
    /** @var UserFactory */
    protected UserFactory $userFactory;

    /** @var QuestionFactory */
    protected QuestionFactory $questionFactory;

    /**
     * QuizzFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserFactory            $userFactory
     * @param QuestionFactory        $questionFactory
     */
    public function __construct(EntityManagerInterface $entityManager, UserFactory $userFactory, QuestionFactory $questionFactory)
    {
        $this->entityManager   = $entityManager;
        $this->userFactory     = $userFactory;
        $this->questionFactory = $questionFactory;
    }

    /**
     * @throws Exception
     *
     * @return Quizz
     */
    protected function generate(): Quizz
    {
        $resource = new Quizz();

        $resource->setNote($this->getFaker()->numberBetween(1, 10));
        $resource->setCandidate($this->userFactory->seed());

        return $resource;
    }

    /**
     * @param Quizz $resource
     *
     * @throws Exception
     *
     * @return Quizz
     */
    protected function withQuestion(Quizz $resource): Quizz
    {
        for ($i = 0; $i <= $this->getFaker()->numberBetween(1, 5); ++$i) {// todo : replace for by $this->seedS()
            $resource->addQuestion($this->questionFactory->seed());
        }

        return $resource;
    }
}
