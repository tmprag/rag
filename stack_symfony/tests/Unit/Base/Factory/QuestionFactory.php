<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Factory;

use App\Entity\Question;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class QuestionFactory
 */
class QuestionFactory extends AbstractFactory
{
    /** @var UserFactory */
    protected UserFactory $userFactory;

    /** @var ThematicFactory */
    protected ThematicFactory $thematicFactory;

    /** @var AnswerFactory */
    protected AnswerFactory $answerFactory;

    /**
     * QuestionFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserFactory            $userFactory
     * @param ThematicFactory        $thematicFactory
     * @param AnswerFactory          $answerFactory
     */
    public function __construct(EntityManagerInterface $entityManager, UserFactory $userFactory, ThematicFactory $thematicFactory, AnswerFactory $answerFactory)
    {
        $this->entityManager   = $entityManager;
        $this->userFactory     = $userFactory;
        $this->thematicFactory = $thematicFactory;
        $this->answerFactory   = $answerFactory;
    }

    /**
     * @throws Exception
     *
     * @return Question
     */
    protected function generate(): Question
    {
        $resource = new Question();

        $resource->setExplanation($this->getFaker()->sentence);
        $resource->setAuthor($this->userFactory->seed());
        $resource->setThematic($this->thematicFactory->seed());

        return $resource;
    }

    /**
     * @param Question $resource
     *
     * @throws Exception
     *
     * @return Question
     */
    protected function withAnswer(Question $resource): Question
    {
        for ($i = 0; $i <= $this->getFaker()->numberBetween(1, 5); ++$i) { // todo : replace for by $this->seedS()
            $resource->addAnswer($this->answerFactory->seed());
        }

        return $resource;
    }
}
