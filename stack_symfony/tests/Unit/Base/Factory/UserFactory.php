<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Factory;

use App\Entity\User;
use App\Models\Constant\Role;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFactory
 */
class UserFactory extends AbstractFactory
{
    /** @required @var UserPasswordEncoderInterface|null */
    public ?UserPasswordEncoderInterface $userPasswordEncoder = null;

    /**
     * UserFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return UserPasswordEncoderInterface|null
     */
    public function getUserPasswordEncoder(): ?UserPasswordEncoderInterface
    {
        return $this->userPasswordEncoder;
    }

    /**
     * @throws Exception
     *
     * @return User
     */
    protected function generate(): User
    {
        $resource = new User();

        $resource->setEmail($this->getFaker()->email);
        $resource->setFirstName($this->getFaker()->firstName);
        $resource->setLastName($this->getFaker()->lastName);
        $resource->setRoles([Role::ROLE_USER]);
        $resource->setMobile($this->getFaker()->phoneNumber);
        $resource->setPhone($this->getFaker()->phoneNumber);
        $resource->setPassword($this->getUserPasswordEncoder()->encodePassword($resource, $this->getFaker()->password));

        return $resource;
    }

    /**
     * @param User $resource
     *
     * @throws Exception
     *
     * @return User
     */
    protected function withQuestion(User $resource): User
    {
        for ($i = 0; $i <= $this->getFaker()->numberBetween(1, 5); ++$i) {// todo : replace for by $this->seedS()
            //$resource->addQuestion($this->questionFactory->seed());
        }

        return $resource;
    }

    /**
     * @param User $resource
     *
     * @throws Exception
     *
     * @return User
     */
    protected function withQuizz(User $resource): User
    {
        for ($i = 0; $i <= $this->getFaker()->numberBetween(1, 5); ++$i) {// todo : replace for by $this->seedS()
            //$resource->addQuizz($this->questionFactory->seed());
        }

        return $resource;
    }
}
