<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Factory;

use App\Entity\Interfaces\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Faker\Factory as Faker;
use Faker\Generator as FakerGenerator;

/**
 * Class AbstractFactory
 */
abstract class AbstractFactory
{
    /** @var FakerGenerator */
    public FakerGenerator $faker;

    /** @required @var EntityManagerInterface */
    public EntityManagerInterface $entityManager;

    /**
     * @param null $fakerSeed
     *
     * @return FakerGenerator
     */
    public function getFaker($fakerSeed = null): FakerGenerator
    {
        if (!(new \ReflectionProperty(__CLASS__, 'faker'))->isInitialized($this)) {
            $this->faker = Faker::create('fr_FR');

            /*
             * You may still want to get the same data generated,
             * for example when using Faker for unit testing purposes.
             *
             * The generator offers a seed() method, which boots the random number generator.
             * Calling the same script twice with the same seed will produce the same results.
             */
            $this->getFaker()->seed($fakerSeed);
        }

        return $this->faker;
    }

    /**
     * @param EntityInterface $entity
     * @param bool            $flush
     *
     * @return EntityInterface
     */
    protected function save(EntityInterface $entity, bool $flush = true): EntityInterface
    {
        $this->entityManager->persist($entity);

        if (true === $flush) {
            $this->entityManager->flush();
        }

        $this->entityManager->refresh($entity);

        return $entity;
    }

    /**
     * @param array $customizations
     * @param null  $callbacks
     * @param bool  $flush
     *
     * @throws Exception
     *
     * @return mixed
     */
    public function seed(array $customizations = [], $callbacks = null, bool $flush = true)
    {
        $object = $this->generate();

        if (null !== $callbacks && \is_array($callbacks) && !empty($callbacks)) {
            foreach ($callbacks as $callback) {
                if (\method_exists($this, $callback)) {
                    $this->{$callback}($object);
                } else {
                    $klass = \get_called_class();
                    throw new Exception("Missing callback {$callback} in factory {$klass}");
                }
            }
        }

        $this->overridePropertiesFromArray($object, $customizations);
        $this->save($object, $flush);

        return $object;
    }

    /**
     * @param int                   $iteration
     * @param array                 $customization
     * @param array|callable[]|null $callbacks
     *
     * @throws Exception
     *
     * @return array
     */
    public function seeds(int $iteration = 1, array $customization = [], array $callbacks = null): array
    {
        $entities = [];

        for ($i = 0; $i < $iteration; ++$i) {
            $entities[] = $this->seed($customization, $callbacks, false);
        }

        $this->entityManager->flush();

        foreach ($entities as $entity) {
            $this->entityManager->refresh($entity);
        }

        return $entities;
    }

    /**
     * @return EntityInterface
     */
    abstract protected function generate(): EntityInterface;

    /**
     * @param $target
     * @param array $properties
     *
     * @throws Exception
     */
    protected function overridePropertiesFromArray(&$target, $properties = [])
    {
        foreach ($properties as $property => $value) {
            $method    = 'set' . \ucfirst($property);
            $addMethod = 'add' . \ucfirst($property);

            if (\method_exists($target, $method)) {
                $target->{$method}($value);
            } elseif (\method_exists($target, $addMethod)) {
                $target->{$addMethod}($value);
            } else {
                $klass = \get_called_class();
                throw new Exception("Missing method {$method} in factory {$klass}");
            }
        }
    }
}
