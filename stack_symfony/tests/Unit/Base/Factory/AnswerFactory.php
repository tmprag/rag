<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Factory;

use App\Entity\Answer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class AnswerFactory
 */
class AnswerFactory extends AbstractFactory
{
    /** @required @var UserFactory */
    public UserFactory $userFactory;

    /** @required @var ThematicFactory */
    public ThematicFactory $thematicFactory;

    /** @var QuestionFactory */
    public QuestionFactory $questionFactory;

    /**
     * AnswerFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserFactory            $userFactory
     * @param ThematicFactory        $thematicFactory
     */
    public function __construct(EntityManagerInterface $entityManager, UserFactory $userFactory, ThematicFactory $thematicFactory)
    {
        $this->entityManager   = $entityManager;
        $this->userFactory     = $userFactory;
        $this->thematicFactory = $thematicFactory;
        $this->questionFactory = new QuestionFactory($entityManager, $this->userFactory, $this->thematicFactory, $this);
    }

    /**
     * @throws Exception
     *
     * @return Answer
     */
    protected function generate(): Answer
    {
        $resource = new Answer();

        $resource->setIsCorrect($this->getFaker()->boolean);
        $resource->setSentence($this->getFaker()->sentence);

        if (null === $resource->getQuestion()) {
            $question = $this->questionFactory->seed();
            $resource->setQuestion($question);
        }

        return $resource;
    }
}
