<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Factory;

use App\Entity\Thematic;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class ThematicFactory
 */
class ThematicFactory extends AbstractFactory
{
//    /** @var UserFactory */
//    protected UserFactory $userFactory;
//
//    /** @var QuestionFactory */
//    protected QuestionFactory $questionFactory;

    /**
     * ThematicFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager/*, UserFactory $userFactory*/)
    {
        $this->entityManager = $entityManager;
//        $this->userFactory = $userFactory;

//        $answerFactory = new AnswerFactory($entityManager, $userFactory, $this);
//        $this->questionFactory = new QuestionFactory($entityManager, $userFactory, $this, $answerFactory);
    }

    /**
     * @throws Exception
     *
     * @return Thematic
     */
    protected function generate(): Thematic
    {
        $resource = new Thematic();

        $resource->setName($this->getFaker()->word);

        return $resource;
    }

//    /**
//     * @param Thematic $resource
//     *
//     * @return Thematic
//     * @throws Exception
//     *
//     */
//    protected function withQuestion(Thematic $resource): Thematic
//    {
//        $i = $this->getFaker()->numberBetween(1, 5);
//        foreach ($this->questionFactory->seeds($i) as $question) {
//            $resource->addQuestion($question);
//        }
//
//        return $resource;
//    }
}
