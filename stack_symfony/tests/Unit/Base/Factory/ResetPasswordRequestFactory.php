<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Factory;

use App\Entity\ResetPasswordRequest;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class ResetPasswordRequestFactory
 */
class ResetPasswordRequestFactory extends AbstractFactory
{
    /** @var UserFactory */
    protected UserFactory $userFactory;

    /**
     * ResetPasswordRequestFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserFactory            $userFactory
     */
    public function __construct(EntityManagerInterface $entityManager, UserFactory $userFactory)
    {
        $this->entityManager = $entityManager;
        $this->userFactory   = $userFactory;
    }

    /**
     * @throws Exception
     *
     * @return ResetPasswordRequest
     */
    protected function generate(): ResetPasswordRequest
    {
        $user = $this->userFactory->seed();
        $this->save($user);

        return new ResetPasswordRequest($user, new DateTimeImmutable('tomorrow'), $this->getFaker()->text(20), $this->getFaker()->text(100));
    }
}
