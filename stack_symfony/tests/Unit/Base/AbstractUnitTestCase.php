<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base;

use App\Tests\Base\AbstractTestCase;
use App\Tests\Unit\Base\Mocks\DoctrineMockTrait;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionException;
use ReflectionMethod;

/**
 * Class AbstractUnitTestCase
 */
class AbstractUnitTestCase extends AbstractTestCase
{
    use DoctrineMockTrait;

    /**
     * @var object
     */
    public $classToTest;

    /**
     * @param null $classOrInterface
     * @param bool $checkIfIsInjectable
     *
     * @throws ReflectionException
     *
     * @return MockObject
     */
    public function createMock($classOrInterface = null, bool $checkIfIsInjectable = true): MockObject
    {
        $mock        = parent::createMock($classOrInterface);
        $parentClass = (new \ReflectionClass($mock))->getParentClass();

        if (true === $checkIfIsInjectable && (null === $parentClass || false === $parentClass)) {
            throw new \ReflectionException("{$classOrInterface} is not injectable (dependency injection) (the parent signature type is " . \gettype($parentClass) . " BUT is not {$classOrInterface}) !!!");
        }
        if (true === $checkIfIsInjectable && ($parentClass->getName() !== $classOrInterface)) {
            throw new \ReflectionException("{$classOrInterface} is not injectable (dependency injection) (the parent signature is " . $parentClass->getName() . " BUT is not {$classOrInterface}) !!!");
        }

        return $mock;
    }

    /**
     * @param string $className
     * @param string $methodName
     *
     * @throws ReflectionException
     *
     * @return ReflectionMethod
     */
    protected static function getPrivateMethod(string $className, string $methodName): ReflectionMethod
    {
        $class  = new \ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }
}
