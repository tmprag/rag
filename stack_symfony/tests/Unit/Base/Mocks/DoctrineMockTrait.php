<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Mocks;

use App\Entity\Interfaces\EntityInterface;
use App\Tests\Base\AbstractTestCase;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use ReflectionException;

/**
 * Trait DoctrineMockTrait
 */
trait DoctrineMockTrait
{
    /**
     * https://symfony.com/doc/current/testing/database.html#mocking-a-doctrine-repository-in-unit-tests
     *
     * mockRepositoryFind([
     *      1 => $entity1,
     *      2 => $entity2,
     *      ...
     * ])
     *
     * @param array|EntityInterface[] $entitiesReturned
     *
     * @throws ReflectionException
     *
     * @return ObjectManager
     */
    protected function mockRepositoryFind(array $entitiesReturned = []): ObjectManager
    {
        /** @var AbstractTestCase $this */
        $current = $this;

        $objectRepository = $current->createMock(ObjectRepository::class);

        // http://pietervogelaar.nl/phpunit-how-to-mock-multiple-calls-to-the-same-method
        $objectRepository
            ->expects($current->any())
            ->method('find')
            ->with($current->anything())
            ->willReturnCallback(static function ($with) use ($entitiesReturned) {
                return $entitiesReturned[$with];
            });

        $objectManager = $current->createMock(ObjectManager::class);

        $objectManager->expects($current->any())
            ->method('getRepository')
            ->willReturn($objectRepository);

        return $objectManager;
    }
}
