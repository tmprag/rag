<?php
/**
 * © Project
 */

namespace App\Tests\Unit\Base\Mocks;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Trait RequestMockTrait
 */
trait RequestMockTrait
{
    /**
     * @param $response
     */
    public function guzzle($response)
    {
        $clientMock = $this->getMockBuilder(GuzzleClient::class)
            ->disableOriginalConstructor()
            ->setMethods(['request'])
            ->getMock();

        $clientMock
            ->method('request')
            ->willReturn($response);
    }
}
