<?php
/**
 * © Project
 */

namespace App\Tests\Base;

use Faker\Factory as Faker;
use Faker\Generator as FakerGenerator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AbstractTestCase
 */
abstract class AbstractTestCase extends WebTestCase
{
    /**
     * @var FakerGenerator
     */
    private $faker;

    /**
     * @var string
     */
    private $projectDir = __DIR__ . '/../../';

    /**
     * @return string
     */
    public function getProjectDir(): string
    {
        return $this->projectDir;
    }

    /**
     * @return FakerGenerator
     */
    public function getFaker(): FakerGenerator
    {
        return $this->faker;
    }

    public function preload(): void
    {
        if (null === $this->faker) {
            $this->faker = Faker::create('fr_FR');
        }
    }
}
