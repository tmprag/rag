<?php
/**
 * © Project
 */

namespace App\Twig\Functions;

use App\Service\Traits\HttpTrait;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class GetEnvExtension
 */
class GetEnvExtension extends AbstractExtension
{
    use HttpTrait;

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getEnv', [$this, 'getEnv']),
        ];
    }
}
