<?php
/**
 * © Project
 */

namespace App\Twig\Filters;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class EmailExtension
 */
class EmailExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('email', [$this, 'email'], [
                'is_safe' => ['html'], // raw
            ]),
        ];
    }

    /**
     * @param string $email
     *
     * @return string
     */
    public function email(string $email): string
    {
        return '<a href="mailto:' . $email . '">' . $email . '</a>';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'email_extension';
    }
}
