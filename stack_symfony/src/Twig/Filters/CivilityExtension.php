<?php
/**
 * © Project
 */

namespace App\Twig\Filters;

use App\Models\Constant\Civility;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class CivilityExtension
 */
class CivilityExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('civility', [$this, 'civility'], [
                'is_safe' => ['html'], // raw
            ]),
        ];
    }

    /**
     * @param bool $civility
     *
     * @return string
     */
    public function civility(bool $civility): string
    {
        switch ($civility) {
            default:
            case Civility::MAN:
                return 'M.';
            case Civility::WOMAN:
                return 'Mme.';
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'civility_extension';
    }
}
