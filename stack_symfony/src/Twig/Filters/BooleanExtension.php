<?php
/**
 * © Project
 */

namespace App\Twig\Filters;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class BooleanExtension
 */
class BooleanExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('boolean', [$this, 'boolean'], [
                'is_safe' => ['html'], // raw
            ]),
        ];
    }

    /**
     * @param bool|null   $boolean
     * @param string|null $mode
     *
     * @return string
     */
    public function boolean(?bool $boolean, string $mode = null): string
    {
        if (null === $mode) {
            $mode = 'oui|non';
        }

        $choices = \explode('|', $mode);

        switch ($boolean) {
            default:
            case true:
                $return = $choices['0'];

                if (\is_numeric($return)) {
                    $return = (int) $return;
                }

                return $return;
            case false:
                $return = $choices['1'];

                if (\is_numeric($return)) {
                    $return = (int) $return;
                }

                return $return;
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'boolean_extension';
    }
}
