<?php
/**
 * © Project
 */

namespace App\Twig\Filters;

use DateTime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class AgeExtension
 */
class AgeExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('age', [$this, 'age']),
        ];
    }

    /**
     * @param $date
     *
     * @throws \Exception
     *
     * @return string|null
     */
    public function age($date)
    {
        if (!$date instanceof DateTime) {
            // turn $date into a valid \DateTime object or let return
            return null;
        }

        $referenceDateTimeObject = new DateTime('@' . \strtotime('now'));
        $diff                    = $referenceDateTimeObject->diff($date);

        return $diff->y . ' ans';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'age_extension';
    }
}
