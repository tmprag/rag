<?php
/**
 * © Project
 */

namespace App\Twig\Filters;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class PhoneExtension
 */
class PhoneExtension extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('phone', [$this, 'phone'], [
                'is_safe' => ['html'], // raw
            ]),
        ];
    }

    /**
     * @param string $phone
     * @param bool   $isTelType
     *
     * @return string
     */
    public function phone(?string $phone, bool $isTelType = true): string
    {
        if (empty($phone)) {
            return '';
        }
        $frenchRegex = '/(\+[0-9]{2,3})([0]{0,1})([1-9]{1})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/';

        \preg_match($frenchRegex, $phone, $groups);

        $phone = '' . $groups[1];
        $phone .= ' ' . $groups[2];

        if (1 === \mb_strlen($groups[3])) {
            $phone .= '0' . $groups[3];
        } else {
            $phone .= '' . $groups[3];
        }

        $phone .= ' ' . $groups[4];
        $phone .= ' ' . $groups[5];
        $phone .= ' ' . $groups[6];
        $phone .= ' ' . $groups[7];

        if (true === $isTelType) {
            $phone = '<a href="tel:' . $phone . '">' . $phone . '</a>';
        }

        return $phone;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'phone_extension';
    }
}
