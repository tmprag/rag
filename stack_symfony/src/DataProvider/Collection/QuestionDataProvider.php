<?php
/**
 * © Project
 */

namespace App\DataProvider\Collection;

use App\DataProvider\AbstractDataProvider;
use App\Entity\Question;
use App\Repository\QuestionRepository;

/**
 * Class QuestionDataProvider
 */
class QuestionDataProvider extends AbstractDataProvider
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * @required
     *
     * @param QuestionRepository $questionRepository
     */
    public function setQuestionService(QuestionRepository $questionRepository): void
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param string      $resourceClass
     * @param string|null $operationName
     * @param array       $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Question::class === $resourceClass;
    }

    /**
     * @param string      $resourceClass
     * @param string|null $operationName
     *
     * @return array
     */
    public function getCollection(string $resourceClass, string $operationName = null): array
    {
        // todo : paginate...

        return $this->questionRepository->findAll();
    }
}
