<?php
/**
 * © Project
 */

namespace App\DataProvider\Item;

use App\DataProvider\AbstractDataProvider;
use App\Entity\Question;
use App\Repository\QuestionRepository;

/**
 * Class QuestionDataProvider
 */
class QuestionDataProvider extends AbstractDataProvider
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * @required
     *
     * @param QuestionRepository $questionRepository
     */
    public function setQuestionService(QuestionRepository $questionRepository): void
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param string      $resourceClass
     * @param string|null $operationName
     * @param array       $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Question::class === $resourceClass;
    }

    /**
     * @param string           $resourceClass
     * @param array|int|string $id
     * @param string|null      $operationName
     * @param array            $context
     *
     * @return Question|null
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Question
    {
        /* @var Question $quesion */
        return $this->questionRepository->find($id);
    }
}
