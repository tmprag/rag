<?php
/**
 * © Project
 */

namespace App\Controller\Api\v1;

use App\Annotation\ThisIsAnExampleOfUseYouCanDeleteThisCode;
use App\Entity\Quizz;
use App\Entity\User;
use App\Models\Response\ApiResponse;
use App\Repository\QuizzRepository;
use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\SecurityServiceTrait;
use App\Service\Traits\SerializerTrait;
use App\Utils\ArrayOperations;
use Exception;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatsController
 *
 * @Route("/stats", name="stats_")
 * @ThisIsAnExampleOfUseYouCanDeleteThisCode()
 */
class StatsController extends AbstractApiController
{
    use SecurityServiceTrait;
    use AppServicesTrait;
    use SerializerTrait;

    /**
     * @Route("/charts/my-results", name="my_results", methods={"GET"})
     * @ThisIsAnExampleOfUseYouCanDeleteThisCode
     *
     * @throws Exception
     *
     * @return ApiResponse
     */
    public function getMyResults(): ApiResponse
    {
        /** @var User $user */
        $user = $this->security->getUser();

        /** @var QuizzRepository $quizzRepository */
        $quizzRepository = $this->getDoctrine()->getRepository(Quizz::class);

        /** @var Quizz[] $questions */
        $quizzs = $quizzRepository->getValidateQuizzs($user);

        $results = [
            'labels' => [],
            'myResults' => [],
            'myAveragesResults' => [],
        ];

        /** @var Quizz $quizz */
        foreach (\array_reverse($quizzs) as $quizz) {
            $results['labels'][]            = $quizz->getCreationDate()->format('Y-m-d H:i:s');
            $results['myResults'][]         = $quizz->getNote();
            $results['myAveragesResults'][] = ArrayOperations::arrayAverage($results['myResults']);
        }

        $results['labels']            = \array_slice($results['labels'], -24, 24, true);
        $results['myResults']         = \array_slice($results['myResults'], -24, 24, true);
        $results['myAveragesResults'] = \array_slice($results['myAveragesResults'], -24, 24, true);

        return new ApiResponse($results, ApiResponse::STATUS_SUCCESS, null, null);
    }

    /**
     * @Route("/charts/all-results", name="all_results", methods={"GET"})
     * @ThisIsAnExampleOfUseYouCanDeleteThisCode
     *
     * @throws Exception
     *
     * @return ApiResponse
     */
    public function getAllResults(): ApiResponse
    {
        /** @var QuizzRepository $quizzRepository */
        $quizzRepository = $this->getDoctrine()->getRepository(Quizz::class);

        /** @var Quizz[] $questions */
        $quizzs = $quizzRepository->getAllValidateQuizzs();

        $results = [
            'labels' => [],
            'allResults' => [],
            'allAveragesResults' => [],
        ];

        /** @var Quizz $quizz */
        foreach (\array_reverse($quizzs) as $quizz) {
            $results['labels'][]             = $quizz->getCreationDate()->format('Y-m-d H:i:s');
            $results['allResults'][]         = $quizz->getNote();
            $results['allAveragesResults'][] = ArrayOperations::arrayAverage($results['allResults']);
        }

        $results['labels']             = \array_slice($results['labels'], -24, 24, true);
        $results['allResults']         = \array_slice($results['allResults'], -24, 24, true);
        $results['allAveragesResults'] = \array_slice($results['allAveragesResults'], -24, 24, true);

        return new ApiResponse($results, ApiResponse::STATUS_SUCCESS, null, null);
    }
}
