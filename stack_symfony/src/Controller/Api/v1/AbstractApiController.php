<?php
/**
 * © Project
 */

namespace App\Controller\Api\v1;

use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\ConfigTrait;
use App\Service\Traits\LoggerServiceTrait;
use App\Service\Traits\TwigTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractApiController
 */
abstract class AbstractApiController extends AbstractController
{
    use AppServicesTrait;
    use LoggerServiceTrait;
    use ConfigTrait;
    use TwigTrait;

    /**
     * @param string $defaultTemplateNameSpace
     * @param array  $datas
     * @param string $defaultTemplateName
     *
     * @return string
     */
    protected function getLineTemplatePath(string $defaultTemplateNameSpace, array $datas = [], string $defaultTemplateName = '_line.html.twig'): string
    {
        if (isset($datas[0]['template'])) {
            $template = $datas[0]['template'];
        } else {
            $template = 'line/' . $defaultTemplateNameSpace . '/' . $defaultTemplateName;
        }

        return \str_replace('_html_twig', '.html.twig', $template); // todo : comportement bizarre à comprendre
    }

    /**
     * @param Request $request
     * @param string  $name
     * @param $defaultValue
     *
     * @return mixed
     */
    protected function getQuery(Request $request, string $name, $defaultValue = null)
    {
        if ($request->query->has($name)) {
            return $request->query->get($name);
        }

        return $defaultValue;
    }

    /**
     * @param Request $request
     * @param string  $name
     * @param $defaultValue
     * @param int|null $filter
     *
     * @return mixed
     */
    protected function getRequest(Request $request, string $name, $defaultValue = null, int $filter = null)
    {
        if ($request->request->has($name)) {
            $value = $request->request->get($name);

            if (null !== $filter) {
                $value = \filter_var($value, $filter);
            }

            return $value;
        }

        return $defaultValue;
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    protected function handleErrorMessages(FormInterface $form): array
    {
        $messages = [];

        $messages[] = 'Erreur(s) dans le formulaire : ' . \count($form->getErrors());
        foreach ($form->getErrors() as $error) {
            $messages[] = $error->getMessage();
            $messages[] = $error->getMessageParameters();
        }

        foreach ($form->all() as $child) {
            $transformationFailure = $child->getTransformationFailure();
            if (null !== $child->getTransformationFailure()) {
                $messages[] = 'Erreur(s) de transformation :';
                $messages[] = $transformationFailure->getMessage();
            }
        }

        return $messages;
    }
}
