<?php
/**
 * © Project
 */

namespace App\Controller\Dashboard;

use App\Annotation\ThisIsAnExampleOfUseYouCanDeleteThisCode;
use App\Entity\Question;
use App\Entity\User;
use App\Form\Admin\QuestionType;
use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\SecurityServiceTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/stats", name="stats_")
 * @ThisIsAnExampleOfUseYouCanDeleteThisCode()
 */
class StatsController extends AbstractDashboardController
{
    use AppServicesTrait;
    use SecurityServiceTrait;

    /**
     * @Route("/results", name="results")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $question = new Question();
        $question->setAuthor($user);

        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getQuestionService()->add($question);

            return $this->redirectToRoute($this->templateBaseRouteName . 'quizz_add');
        }

        return $this->render('stats.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
