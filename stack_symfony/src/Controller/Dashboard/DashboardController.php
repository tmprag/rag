<?php
/**
 * © Project
 */

namespace App\Controller\Dashboard;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pages", name="pages_")
 */
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function run(Request $request): Response
    {
        return $this->render('index.html.twig');
    }
}
