<?php
/**
 * © Project
 */

namespace App\Controller\Dashboard;

use App\Annotation\ThisIsAnExampleOfUseYouCanDeleteThisCode;
use App\Entity\Quizz;
use App\Entity\User;
use App\Form\Dashboard\QuizzType;
use App\Repository\QuizzRepository;
use App\Service\Traits\SecurityServiceTrait;
use App\Utils\TransformValues;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/quizz", name="quizz_")
 * @ThisIsAnExampleOfUseYouCanDeleteThisCode
 */
class QuizzController extends AbstractDashboardController
{
    use SecurityServiceTrait;

    /**
     * @Route("/run", name="run", methods={"GET","POST"})
     * @ThisIsAnExampleOfUseYouCanDeleteThisCode
     *
     * @param Request $request
     *
     * @return Response
     */
    public function run(Request $request): Response
    {
        /** @var User $user */
        $user = $this->security->getUser();

        // Vaut null si le formulaire n'est pas posté
        if ($request->request->has('quizz')) {
            $quizzId = $request->request->get('quizz')['quizzId'];
        } else {
            $quizzId = null;
        }

        $quizzForm = $this->createForm(QuizzType::class, [
            'user' => $user,
            'doctrine' => $this->getDoctrine(),
            'quizzId' => $quizzId,
        ]);
        $quizzForm->handleRequest($request);

        if ($quizzForm->isSubmitted() && $quizzForm->isValid()) {
            $data = $quizzForm->getData();

            /** @var QuizzRepository $quizzRepository */
            $quizzRepository = $this->getDoctrine()->getRepository(Quizz::class);
            $quizz           = $quizzRepository->find((int) $data['quizzId']);

            $questionPoints = [];
            $logs           = [];

            $questionNumber = 1;
            foreach ($quizz->getQuestions() as $question) {
                $answerNumber                    = 1;
                $questionPoints[$questionNumber] = 1;
                foreach ($question->getAnswers() as $answer) {
                    $inputName = 'question_' . $questionNumber . '_answer_' . $answerNumber;

                    if (isset($data[$inputName])) {
                        if (false === $data[$inputName] && true === $answer->getIsCorrect()) {
                            $questionPoints[$questionNumber] = 0;

                            $logs[] = $this->render('quizz/log.html.twig', [
                                'questionNumber' => $questionNumber,
                                'action' => 'oublié de cocher',
                                'answerSentence' => $answer->getSentence(),
                                'questionSentence' => $question->getSentence(),
                                'question' => $question,
                            ])->getContent();
                        }

                        if (true === $data[$inputName] && false === $answer->getIsCorrect()) {
                            $questionPoints[$questionNumber] = 0;

                            $logs[] = $this->render('quizz/log.html.twig', [
                                'questionNumber' => $questionNumber,
                                'action' => 'coché',
                                'answerSentence' => $answer->getSentence(),
                                'questionSentence' => $question->getSentence(),
                                'question' => $question,
                            ])->getContent();
                        }
                    }

                    ++$answerNumber;
                }

                // Si erreur a la question alors on explique
                if (0 === $questionPoints[$questionNumber]) {
                    $logs[] = $this->render('quizz/explanation.html.twig', [
                        'question' => $question,
                    ])->getContent();
                }

                ++$questionNumber;
            }
            $quizzPoints = \floor((\array_sum($questionPoints) / 11) * 10);

            $quizz->setNote($quizzPoints);
            $this->getDoctrine()->getManager()->persist($quizz);
            $this->getDoctrine()->getManager()->flush();

            return $this->render('quizz/finish.html.twig', [
                'form' => $quizzForm->createView(),
                'quizz' => $quizz,
                'logs' => $logs,
                'quizzPoints' => $quizzPoints,
                'comment' => TransformValues::commentByQuizzPoints($quizzPoints),
            ]);
        }

        return $this->render('quizz/run.html.twig', [
            'form' => $quizzForm->createView(),
            'quizz' => $quizzForm->getData()['quizz'],
        ]);
    }
}
