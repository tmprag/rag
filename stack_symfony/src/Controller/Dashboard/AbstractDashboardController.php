<?php
/**
 * © Project
 */

namespace App\Controller\Dashboard;

use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\ConfigTrait;
use App\Service\Traits\LoggerServiceTrait;
use App\Service\Traits\TwigTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractDashboardController
 */
abstract class AbstractDashboardController extends AbstractController
{
    use AppServicesTrait;
    use LoggerServiceTrait;
    use ConfigTrait;
    use TwigTrait;

    /**
     * @var string
     */
    protected $templateBaseRouteName = 'dashboard_';

    /**
     * @var string
     */
    private $templateBasePath = 'dashboard';

    /**
     * AbstractDashboardController constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        if ($_SERVER['DOMAIN'] !== $request->getHost()) {
            $request->headers->add(['X-Requested-With' => 'XMLHttpRequest']);
        }
    }

    /**
     * @param string        $path
     * @param array         $parameters
     * @param Response|null $response
     *
     * @return Response
     */
    public function render(string $path, array $parameters = [], Response $response = null): Response
    {
        return parent::render($this->templateBasePath . '/' . $path, $parameters, $response);
    }
}
