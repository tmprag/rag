<?php
/**
 * © Project
 */

namespace App\Controller\App;

use App\Service\MailerService;
use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\ConfigTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment as Twig;

/**
 * Class SecurityController
 *
 * @Route("/", name="security_")
 */
class SecurityController extends AbstractController
{
    use AppServicesTrait;
    use ConfigTrait;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var MailerService
     */
    private $mailer;
    /**
     * @var Twig
     */
    private $twig;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * SecurityController constructor.
     *
     * @param RequestStack                 $requestStack
     * @param MailerService                $mailer
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Twig                         $twig
     * @param EventDispatcherInterface     $eventDispatcher
     */
    public function __construct(RequestStack $requestStack, MailerService $mailer, UserPasswordEncoderInterface $passwordEncoder, Twig $twig, EventDispatcherInterface $eventDispatcher)
    {
        $this->mailer          = $mailer;
        $this->passwordEncoder = $passwordEncoder;
        $this->twig            = $twig;
        $this->eventDispatcher = $eventDispatcher;

        $request = $requestStack->getCurrentRequest();

        if ($_SERVER['DOMAIN'] !== $request->getHost()) {
            $request->headers->add(['X-Requested-With' => 'XMLHttpRequest']);
        }
    }

    /**
     * @Route("/login", name="login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('dashboard_pages_index');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/download/{path}", name="download_public_file", requirements={"path"=".+"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function downloadFile(Request $request): Response
    {
        $path = '/' . \ltrim(\urldecode($request->getPathInfo()), '/download');

        return $this->file($this->projectDir . '/public' . $path);
    }

    /**
     * @Route("/view/{path}", name="view_public_file", requirements={"path"=".+"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function viewFile(Request $request): Response
    {
        $path = '/' . \ltrim(\urldecode($request->getPathInfo()), '/view');

        return new BinaryFileResponse($this->projectDir . '/public' . $path);
    }
}
