<?php
/**
 * © Project
 */

namespace App\Controller\App;

use App\Annotation\ThisIsAnExampleOfUseYouCanDeleteThisCode;
use App\Service\MailerService;
use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\HttpTrait;
use App\Service\Traits\LoggerServiceTrait;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 *
 * @Route("/check", name="check_")
 */
class CheckController extends AbstractAppController
{
    use AppServicesTrait;
    use LoggerServiceTrait;
    use HttpTrait;

    /**
     * @Route("/email")
     * @ThisIsAnExampleOfUseYouCanDeleteThisCode()
     *
     * @param MailerService $mailerService
     *
     * @throws TransportExceptionInterface
     */
    public function sendEmail(MailerService $mailerService)
    {
        $mailerService->sendMessage(
            'Check if Mailer is OK...',
            'check/email.html.twig',
            [
                'status' => 'Ok',
            ]
        );

        echo '<a target="_blank" href="http://maildev.' . $this->getEnv('DOMAIN') . '/#/">Voir les mails de dev...</a>';

        exit;
    }
}
