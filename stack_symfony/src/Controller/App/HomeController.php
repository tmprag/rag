<?php
/**
 * © Project
 */

namespace App\Controller\App;

use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\LoggerServiceTrait;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 *
 * @Route("/", name="home_")
 */
class HomeController extends AbstractAppController
{
    use AppServicesTrait;
    use LoggerServiceTrait;

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('primary/index.html.twig');
    }
}
