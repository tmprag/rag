<?php
/**
 * © Project
 */

namespace App\Controller\Admin;

use App\Service\Traits\AppServicesTrait;
use App\Service\Traits\LoggerServiceTrait;
use App\Service\Traits\SecurityServiceTrait;
use App\Service\Traits\TwigTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

/**
 * Class AbstractAdminController
 */
abstract class AbstractAdminController extends AbstractController
{
    use AppServicesTrait;
    use LoggerServiceTrait;
    use SecurityServiceTrait;
    use TwigTrait;

    /**
     * @var string
     */
    protected $templateBaseRouteName = 'admin_';

    /**
     * @var string
     */
    private $templateBasePath = 'admin';

    /**
     * @var Security
     */
    private $security;

    /**
     * AbstractAdminController constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        if ($_SERVER['DOMAIN'] !== $request->getHost()) {
            $request->headers->add(['X-Requested-With' => 'XMLHttpRequest']);
        }
    }

    /**
     * @param string        $path
     * @param array         $parameters
     * @param Response|null $response
     *
     * @return Response
     */
    public function render(string $path, array $parameters = [], Response $response = null): Response
    {
        return parent::render($this->templateBasePath . '/' . $path, $parameters, $response);
    }
}
