<?php
/**
 * © Project
 */

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 */
class AdminController extends AbstractAdminController
{
    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function index(Request $request)
    {
        return $this->render('index.html.twig');
    }
}
