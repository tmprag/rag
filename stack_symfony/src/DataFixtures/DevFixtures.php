<?php
/**
 * © Project
 */

namespace App\DataFixtures;

use App\Annotation\ThisIsAnExampleOfUseYouCanDeleteThisCode;
use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Thematic;
use App\Entity\User;
use App\Models\Constant\Role;
use App\Service\Traits\AppServicesTrait;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Faker\Factory as Faker;
use Faker\Generator;
use Romans\Filter\IntToRoman;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class DevFixtures
 */
class DevFixtures extends Fixture
{
    use AppServicesTrait;

    /** @var UserPasswordEncoderInterface */
    private UserPasswordEncoderInterface $passwordEncoder;

    /** @var Generator */
    private Generator $faker;

    /** @var Security */
    private Security $security;

    /** @var TokenStorageInterface */

    private TokenStorageInterface $tokenStorage;
    /**
     * @var AuthenticationManagerInterface
     */
    private AuthenticationManagerInterface $authenticationManager;

    /**
     * DevFixtures constructor.
     *
     * @param UserPasswordEncoderInterface   $passwordEncoder
     * @param TokenStorageInterface          $tokenStorage
     * @param Security                       $security
     * @param AuthenticationManagerInterface $authenticationManager
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenStorageInterface $tokenStorage, Security $security, AuthenticationManagerInterface $authenticationManager)
    {
        $this->security              = $security;
        $this->tokenStorage          = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->passwordEncoder       = $passwordEncoder;
        $this->faker                 = Faker::create('fr_FR');
    }

    /**
     * @return array|string[]
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @ThisIsAnExampleOfUseYouCanDeleteThisCode()
         */
        $bot     = $this->createUser($manager, 'bot@rag.com', 'rag@A1', 'Bot', 'Bot', [Role::ROLE_ADMIN]);
        $zakaria = $this->createUser($manager, 'zakaria.naoura@gmail.com', 'rag@A1', 'Zakaria', 'Naoura', [Role::ROLE_ADMIN]);
        $laura   = $this->createUser($manager, 'laura.pouyadou@gmail.com', 'rag@A1', 'Laura', 'Pouyadou', [Role::ROLE_ADMIN]);

        $galenique                           = $this->createThematic($manager, 'Galénique');
        $chimie                              = $this->createThematic($manager, 'Chimie');
        $biochimie                           = $this->createThematic($manager, 'Biochimie');
        $microbiologie                       = $this->createThematic($manager, 'Microbiologie');
        $imunologie                          = $this->createThematic($manager, 'Imunologie');
        $pathologie                          = $this->createThematic($manager, 'Pathologie');
        $pharmacologie                       = $this->createThematic($manager, 'Pharmacologie');
        $botanique                           = $this->createThematic($manager, 'Botanique');
        $physiologie                         = $this->createThematic($manager, 'Physiologie');
        $gestionEtLegislationProfessionnelle = $this->createThematic($manager, 'Gestion et legislation professionnelle');

        $this->createQuestion($manager, $bot, $chimie, 'Question $chimie...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $biochimie, 'Question $biochimie...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $microbiologie, 'Question $microbiologie...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $imunologie, 'Question $imunologie...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $pathologie, 'Question $pathologie...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $pharmacologie, 'Question $pharmacologie...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $botanique, 'Question $botanique...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $physiologie, 'Question $physiologie...', [['a', true], ['b'], ['c'], ['d']]);
        $this->createQuestion($manager, $bot, $gestionEtLegislationProfessionnelle, 'Question $gestionEtLegislationProfessionnelle...', [['a', true], ['b'], ['c'], ['d']]);

        //# Chiffres Arabe en chiffre Romain

        for ($i = 0; 100 >= $i; ++$i) {
            $filterIntToRoman = new IntToRoman();
            $number           = $this->faker->numberBetween(1, 10000);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . ' en chiffre romain ?', [
                [$filterIntToRoman->filter(\abs($number)), true],
                [$filterIntToRoman->filter(\abs($number + $this->faker->numberBetween(1, 1000)))],
                [$filterIntToRoman->filter(\abs($number - $this->faker->numberBetween(1, 1000)))],
                [$filterIntToRoman->filter(\abs($number + $this->faker->numberBetween(1, 1000)))],
            ]);
        }

        //# Chiffres Romains en chiffre Arabe

        for ($i = 0; 100 >= $i; ++$i) {
            $filterIntToRoman = new IntToRoman();

            $number      = $this->faker->numberBetween(1, 10000);
            $romanNumber = $filterIntToRoman->filter($number);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $romanNumber . ' en chiffre arabe ?', [
                [\abs($number), true],
                [\abs($number + $this->faker->numberBetween(1, 1000))],
                [\abs($number - $this->faker->numberBetween(1, 1000))],
                [\abs($number + $this->faker->numberBetween(1, 1000))],
            ]);
        }

        //# Conversions d'unités (litres, grammes, distance, vitesse, m2, m3, durée ... https://www.kartable.fr/ressources/mathematiques/cours/les-unites-de-mesure-longueur-masse-et-temps-3/40854)

        //# Masses

        // µg en mg

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'µg en mg ?', [
                [$number / 10],
                [$number / 100],
                [$number / 1000, true],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // mg en cg

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'mg en cg ?', [
                [$number / 10, true],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // mg en dg

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'mg en dg ?', [
                [$number / 10],
                [$number / 100, true],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // mg en g

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'mg en g ?', [
                [$number / 10],
                [$number / 100],
                [$number / 1000, true],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // mg en µg

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'mg en µg ?', [
                [$number / 10],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000, true],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // cg en mg

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'cg en mg ?', [
                [$number / 10],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number * 10, true],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // dg en mg

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'dg en mg ?', [
                [$number / 10],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100, true],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // mg en g

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'g en mg ?', [
                [$number / 10],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000, true],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        // hg en kg

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'hg en kg ?', [
                [$number / 10, true],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/masse/">Voir le convertisseur de masse</a>');
        }

        //# Volumes

        // m3 en cl

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'm3 en cl ?', [
                [$number * 100000, true],
                [$number * 10000],
                [$number * 1000],
                [$number * 100],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number / 100000],
            ], '<a href="https://converticious.com/fr/volume/">Voir le convertisseur de volume</a>');
        }

        // ml en cl

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'ml en cl ?', [
                [$number / 10, true],
                [$number / 100],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/volume/">Voir le convertisseur de volume</a>');
        }

        // ml en dl

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'ml en dl ?', [
                [$number / 10],
                [$number / 100, true],
                [$number / 1000],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/volume/">Voir le convertisseur de volume</a>');
        }

        // ml en l

        for ($i = 0; 100 >= $i; ++$i) {
            $number = $this->faker->randomFloat(2, 2, 100);

            $this->createQuestion($manager, $bot, $galenique, 'Que donne ' . $number . 'ml en l ?', [
                [$number / 10],
                [$number / 100],
                [$number / 1000, true],
                [$number / 10000],
                [$number * 10],
                [$number * 100],
                [$number * 1000],
                [$number * 10000],
            ], '<a href="https://converticious.com/fr/volume/">Voir le convertisseur de volume</a>');
        }
    }

    /**
     * @ThisIsAnExampleOfUseYouCanDeleteThisCode()
     *
     * @param ObjectManager $manager
     * @param string        $name
     *
     * @return Thematic
     */
    public function createThematic(ObjectManager $manager, string $name): Thematic
    {
        $thematic = new Thematic();
        $thematic->setName($name);
        $manager->persist($thematic);
        $manager->flush();

        return $thematic;
    }

    /**
     * @ThisIsAnExampleOfUseYouCanDeleteThisCode()
     *
     * @param ObjectManager $manager
     * @param string        $email
     * @param string        $password
     * @param string        $firstName
     * @param string        $lastName
     * @param array         $roles
     *
     * @return User
     */
    public function createUser(ObjectManager $manager, string $email, string $password, string $firstName, string $lastName, array $roles): User
    {
        $user    = new User();
        $encoded = $this->passwordEncoder->encodePassword($user, $password);
        $user->setPassword($encoded);
        $user->setEmail($email);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setRoles($roles);
        $manager->persist($user);
        $manager->flush();

        return $user;
    }

    /**
     * @ThisIsAnExampleOfUseYouCanDeleteThisCode()
     *
     * @param ObjectManager $manager
     * @param User          $author
     * @param Thematic      $thematic
     * @param string        $sentence
     * @param array         $answers
     * @param string|null   $diagram
     * @param string|null   $explanation
     */
    public function createQuestion(ObjectManager $manager, User $author, Thematic $thematic, string $sentence, array $answers = [], string $explanation = null, string $diagram = null)
    {
        $question = (new Question())
            ->setAuthor($author)
            ->setThematic($thematic)
            ->setSentence($sentence)
            ->setExplanation($explanation);

        $manager->persist($question);

        foreach ($answers as $answer) {
            $answerObject = (new Answer())
                ->setQuestion($question)
                ->setSentence($answer[0]);

            if (isset($answer[1]) && true === $answer[1]) {
                $answerObject->setIsCorrect(true);
//                $answerObject->setSentence('*' . $answerObject->getSentence());
                $answerObject->setSentence($answerObject->getSentence());
            } else {
                $answerObject->setIsCorrect(false);
            }

            $manager->persist($answerObject);
        }

        $manager->flush();
    }
}
