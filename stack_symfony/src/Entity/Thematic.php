<?php
/**
 * © Project
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Interfaces\EntityHistoryInterface;
use App\Entity\Interfaces\EntityInterface;
use App\Entity\Traits\CollectionTrait;
use App\Entity\Traits\EntityHistoryTrait;
use App\Repository\ThematicRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"thematic:read"}},
 *     denormalizationContext={"groups"={"thematic:write"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 * )
 * @ORM\Entity(repositoryClass=ThematicRepository::class)
 */
class Thematic implements EntityHistoryInterface, EntityInterface
{
    use EntityHistoryTrait;
    use CollectionTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"thematic:read"})
     */
    private ?int $id = null; // todo : use uuid

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"thematic:read", "thematic:write"})
     */
    private ?string $name;

    /**
     * @ORM\OneToMany(targetEntity=Question::class, mappedBy="thematic")
     * @Groups({"thematic:read"})
     */
    private Collection $questions;

    /**
     * Thematic constructor.
     */
    public function __construct()
    {
        $this->initDates();
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    /**
     * @param ArrayCollection $questions
     */
    public function setQuestions(ArrayCollection $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @param Question $question
     *
     * @return $this
     */
    public function addQuestion(Question $question): self
    {
        $this->add($question, $this->questions, 'setThematic');

        return $this;
    }

    /**
     * @param Question $question
     *
     * @return $this
     */
    public function removeQuestion(Question $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getThematic() === $this) {
                $question->setThematic(null);
            }
        }

        return $this;
    }
}
