<?php
/**
 * © Project
 */

namespace App\Entity\Traits;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait EntityHistoryTrait
 */
trait EntityHistoryTrait
{
    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $updatedDate;

    /**
     * @return DateTime|null
     */
    public function getCreationDate(): ?DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate(DateTime $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param DateTime $updatedDate
     *
     * @return $this
     */
    public function setUpdatedDate(DateTime $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    public function initDates()
    {
        $this->setCreationDate(new \DateTime('now'));
        $this->setUpdatedDate(new \DateTime('now'));
    }
}
