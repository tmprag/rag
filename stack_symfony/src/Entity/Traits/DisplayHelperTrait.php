<?php
/**
 * © Project
 */

namespace App\Entity\Traits;

/**
 * Trait DisplayHelperTrait
 */
trait DisplayHelperTrait
{
    /**
     * @param \DateTimeInterface|null $starDate
     * @param \DateTimeInterface|null $endDate
     *
     * @return string
     */
    protected function getPeriodDateTime(?\DateTimeInterface $starDate, ?\DateTimeInterface $endDate): string
    {
        $nowDate             = new \DateTime('now');
        $displayedDateFormat = 'd/m';
        $displayedTimeFormat = 'H:i';

        if (null === $endDate && null !== $starDate) {
            $period = 'depuis le ' . $starDate->format($displayedDateFormat . ' à ' . $displayedTimeFormat);
        } elseif (null !== $endDate && null === $starDate) {
            $period = 'jusqu\'au ' . $endDate->format($displayedDateFormat . ' à ' . $displayedTimeFormat);
        } elseif (null === $endDate && null === $starDate) {
            $period = 'inconnue';
        } else {
            if ($starDate->format('Y') !== $nowDate->format('Y')) {
                $displayedDateFormat = 'd/m/Y';
            }

            if ($endDate->format('Y') !== $nowDate->format('Y')) {
                $displayedDateFormat = 'd/m/Y';
            }

            if ('00:00' === $starDate->format('H:i') && $starDate->format('H:i') === $endDate->format('H:i')) {
                $displayedTimeFormat = '';
            }

            if ($starDate->format('Y/m/d') === $endDate->format('Y/m/d')) {
                if ($starDate->format('H:i') !== $endDate->format('H:i')) {
                    $period = 'le ' . $starDate->format($displayedDateFormat);
                    $period .= ' de ' . $starDate->format($displayedTimeFormat);
                    $period .= ' à ' . $endDate->format($displayedTimeFormat);
                } else {
                    $period = 'le ' . $starDate->format($displayedDateFormat . ' à ' . $displayedTimeFormat);
                }
            } else {
                if (empty($displayedTimeFormat)) {
                    $period = 'du ' . $starDate->format($displayedDateFormat);
                    $period .= ' au ' . $endDate->format($displayedDateFormat);
                } else {
                    $period = 'du ' . $starDate->format($displayedDateFormat . ' à ' . $displayedTimeFormat);
                    $period .= ' au ' . $endDate->format($displayedDateFormat . ' à ' . $displayedTimeFormat);
                }
            }
        }

        return $period;
    }

    /**
     * @param int|null $starYear
     * @param int|null $endYear
     *
     * @return string
     */
    protected function getPeriodYear(?int $starYear, ?int $endYear): string
    {
        $nowYear = (new \DateTime('now'))->format('Y');

        if (null === $endYear && null !== $starYear) {
            if ($starYear >= $nowYear) {
                $period = 'à compter de ';
            } else {
                $period = 'depuis ';
            }

            $period .= $starYear;
        } elseif (null !== $endYear && null === $starYear) {
            $period = 'jusqu\'en ' . $endYear;
        } elseif (null === $endYear && null === $starYear) {
            $period = 'inconnue';
        } else {
            if ($starYear === $endYear) {
                $period = 'en ' . $starYear;
            } else {
                if ($starYear >= $nowYear) {
                    $period = 'à compter de ';
                } else {
                    $period = 'de ';
                }

                $period .= $starYear . ' à ' . $endYear;
            }
        }

        return $period;
    }
}
