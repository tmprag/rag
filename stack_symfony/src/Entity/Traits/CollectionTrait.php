<?php
/**
 * © Project
 */

namespace App\Entity\Traits;

use Doctrine\Common\Collections\Collection;

/**
 * Trait CollectionTrait
 */
trait CollectionTrait
{
    /**
     * @param $container
     * @param $subject
     *
     * @return bool
     */
    public function has($subject, $container): bool
    {
        if ($container instanceof Collection && $container->contains($subject)) {
            return true;
        }
        if (\is_array($container) && \in_array($subject, $container)) {
            return true;
        }

        return false;
    }

    /**
     * @param $subject
     * @param $container
     * @param string|null $mappedByMethodName
     *
     * @return CollectionTrait
     */
    public function add($subject, $container, string $mappedByMethodName = null): self
    {
        if ($container instanceof Collection) {
            if (!$container->contains($subject)) {
                $container->add($subject);

                if (null !== $mappedByMethodName) {
                    \call_user_func_array([$subject, $mappedByMethodName], [$this]);
                }
            }

            return $this;
        }

        if (\is_array($container)) {
            if (!\in_array($subject, $container)) {
                $container[] = $subject;

                if (null !== $mappedByMethodName) {
                    \call_user_func_array([$subject, $mappedByMethodName], [$this]);
                }
            }

            return $this;
        }

        return $this;
    }

    // public function remove() ...
}
