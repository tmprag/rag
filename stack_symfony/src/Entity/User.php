<?php
/**
 * © Project
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Interfaces\EntityHistoryInterface;
use App\Entity\Interfaces\EntityInterface;
use App\Entity\Traits\CollectionTrait;
use App\Entity\Traits\EntityHistoryTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="public.user")
 * @ApiResource(
 *     normalizationContext={"groups"={"user:read"}},
 *     denormalizationContext={"groups"={"user:write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, EntityHistoryInterface, EntityInterface
{
    use EntityHistoryTrait;
    use CollectionTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(name="id", type="guid")
     * @Groups({"user:read", "thematic:read"})
     */
    private ?string $id = null; // todo : use uuid

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:read", "user:write", "thematic:read"})
     */
    private ?string $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"user:read", "user:write"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"user:read", "user:write"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "user:write"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "user:write"})
     */
    private $mobile;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resetPasswordToken;

    /**
     * @var DateTime | null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $resetPasswordTokenExpireAt;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\OneToMany(targetEntity=Question::class, mappedBy="author")
     */
    private Collection $questions;

    /**
     * @ORM\OneToMany(targetEntity=Quizz::class, mappedBy="candidate")
     */
    private Collection $quizzs;

    /**
     * @var |null
     */
    private $plainPassword;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->initDates();
        $this->questions = new ArrayCollection();
        $this->quizzs    = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this|null
     */
    public function setEmail(string $email): ?self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): ?string
    {
        return (string) $this->email;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return User
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     *
     * @return User
     */
    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): ?array
    {
        $roles = $this->roles;

        return \array_unique($roles);
    }

    /**
     * @param array $roles
     *
     * @return $this|null
     */
    public function setRoles(array $roles): ?self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @param string $role
     *
     * @return $this|null
     */
    public function addRole(string $role): ?self
    {
        //$this->add($role, $this->roles); // fail test !!! todo : fix this !!!
        $this->roles[] = $role;

        return $this;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        return $this->has($role, $this->getRoles());
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this|null
     */
    public function setPassword(string $password): ?self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * @return mixed
     */
    public function getResetPasswordToken()
    {
        return $this->resetPasswordToken;
    }

    /**
     * @param string $resetPasswordToken
     *
     * @return User
     */
    public function setResetPasswordToken(string $resetPasswordToken): self
    {
        $this->resetPasswordToken = $resetPasswordToken;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getResetPasswordTokenExpireAt(): ?DateTime
    {
        return $this->resetPasswordTokenExpireAt;
    }

    /**
     * @param DateTime|null $resetPasswordTokenExpireAt
     *
     * @return $this
     */
    public function setResetPasswordTokenExpireAt(?DateTime $resetPasswordTokenExpireAt): self
    {
        $this->resetPasswordTokenExpireAt = $resetPasswordTokenExpireAt;

        return $this;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    /**
     * @param ArrayCollection $questions
     */
    public function setQuestions(ArrayCollection $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @param Question $question
     *
     * @return $this
     */
    public function addQuestion(Question $question): self
    {
        $this->add($question, $this->questions, 'setAuthor');

        return $this;
    }

    /**
     * @param Question $question
     *
     * @return bool
     */
    public function hasQuestion(Question $question): bool
    {
        return $this->has($question, $this->getQuestions());
    }

    /**
     * @param Question $question
     *
     * @return $this
     */
    public function removeQuestion(Question $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getAuthor() === $this) {
                $question->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quizz[]
     */
    public function getQuizzs(): Collection
    {
        return $this->quizzs;
    }

    /**
     * @param ArrayCollection $quizzs
     */
    public function setQuizzs(ArrayCollection $quizzs): void
    {
        $this->quizzs = $quizzs;
    }

    /**
     * @param Quizz $quizz
     *
     * @return $this
     */
    public function addQuizz(Quizz $quizz): self
    {
        $this->add($quizz, $this->quizzs, 'setCandidate');

        return $this;
    }

    /**
     * @param Quizz $quizz
     *
     * @return $this
     */
    public function removeQuizz(Quizz $quizz): self
    {
        if ($this->quizzs->contains($quizz)) {
            $this->quizzs->removeElement($quizz);
            // set the owning side to null (unless already changed)
            if ($quizz->getCandidate() === $this) {
                $quizz->setCandidate(null);
            }
        }

        return $this;
    }
}
