<?php
/**
 * © Project
 */

namespace App\Entity\Interfaces;

use DateTime;

/**
 * Interface EntityHistoryInterface
 */
interface EntityHistoryInterface
{
    /**
     * @return DateTime|null
     */
    public function getCreationDate(): ?DateTime;

    /**
     * @param DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate(DateTime $creationDate);

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): ?DateTime;

    /**
     * @param DateTime $updatedDate
     *
     * @return $this
     */
    public function setUpdatedDate(DateTime $updatedDate);

    /**
     * @return mixed
     */
    public function initDates();
}
