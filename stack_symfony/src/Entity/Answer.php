<?php
/**
 * © Project
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Interfaces\EntityHistoryInterface;
use App\Entity\Interfaces\EntityInterface;
use App\Entity\Traits\EntityHistoryTrait;
use App\Repository\AnswerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AnswerRepository::class)
 */
class Answer implements EntityHistoryInterface, EntityInterface
{
    use EntityHistoryTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null; // todo : use uuid

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource()
     */
    private ?Question $question = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $sentence;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isCorrect;

    /**
     * Answer constructor.
     */
    public function __construct()
    {
        $this->initDates();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Question|null
     */
    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    /**
     * @param Question|null $question
     *
     * @return $this
     */
    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSentence(): ?string
    {
        return $this->sentence;
    }

    /**
     * @param string $sentence
     *
     * @return $this
     */
    public function setSentence(string $sentence): self
    {
        $this->sentence = $sentence;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsCorrect(): ?bool
    {
        return $this->isCorrect;
    }

    /**
     * @param bool $isCorrect
     *
     * @return $this
     */
    public function setIsCorrect(bool $isCorrect): self
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }
}
