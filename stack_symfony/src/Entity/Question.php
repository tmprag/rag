<?php
/**
 * © Project
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Interfaces\EntityHistoryInterface;
use App\Entity\Interfaces\EntityInterface;
use App\Entity\Traits\CollectionTrait;
use App\Entity\Traits\EntityHistoryTrait;
use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"question:read"}},
 *     denormalizationContext={"groups"={"question:write"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('question_edit', object)"},
 *         "delete"={"security"="is_granted('question_delete', object)"}
 *     }
 * )
 */
class Question implements EntityHistoryInterface, EntityInterface
{
    use EntityHistoryTrait;
    use CollectionTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"question:read", "user:read"})
     */
    private ?int $id = null; // todo : use uuid

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"question:read", "question:write", "user:read"})
     */
    private ?string $sentence;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="questions")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"question:read", "question:write", "user:read"})
     */
    private ?User $author;

    /**
     * @ORM\ManyToOne(targetEntity=Thematic::class, inversedBy="questions")
     * @Groups({"question:read", "question:write", "user:read"})
     */
    private ?Thematic $thematic;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"question:read", "question:write"})
     */
    private ?string $explanation;

    /**
     * @ORM\OneToMany(targetEntity=Answer::class, mappedBy="question", cascade={"persist"})
     * @ApiSubresource()
     */
    private Collection $answers;

    /**
     * @ORM\ManyToMany(targetEntity=Quizz::class, mappedBy="questions")
     */
    private Collection $quizzs;

    /**
     * Question constructor.
     */
    public function __construct()
    {
        $this->initDates();
        $this->answers = new ArrayCollection();
        $this->quizzs  = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSentence(): ?string
    {
        return $this->sentence;
    }

    /**
     * @param string|null $sentence
     *
     * @return $this
     */
    public function setSentence(?string $sentence): self
    {
        $this->sentence = $sentence;

        return $this;
    }

    /**
     * @return Answer[]|Collection
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    /**
     * @param ArrayCollection $answers
     *
     * @return Collection
     */
    public function setAnswers(ArrayCollection $answers): Collection
    {
        return $this->answers = $answers;
    }

    /**
     * @param Answer $answer
     *
     * @return $this
     */
    public function addAnswer(Answer $answer): self
    {
        $this->add($answer, $this->answers, 'setQuestion');

        return $this;
    }

    /**
     * @param Answer $answer
     *
     * @return $this
     */
    public function removeAnswer(Answer $answer): self
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
            // set the owning side to null (unless already changed)
            if ($answer->getQuestion() === $this) {
                $answer->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return User|null
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User|null $author
     *
     * @return $this
     */
    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Quizz[]
     */
    public function getQuizzs(): Collection
    {
        return $this->quizzs;
    }

    /**
     * @param ArrayCollection $quizzs
     *
     * @return Collection
     */
    public function setQuizzs(ArrayCollection $quizzs): Collection
    {
        return $this->quizzs = $quizzs;
    }

    /**
     * @param Quizz $quizz
     *
     * @return $this
     */
    public function addQuizz(Quizz $quizz): self
    {
        $this->add($quizz, $this->quizzs, 'addQuestion');

        return $this;
    }

    /**
     * @param Quizz $quizz
     *
     * @return $this
     */
    public function removeQuizz(Quizz $quizz): self
    {
        if ($this->quizzs->contains($quizz)) {
            $this->quizzs->removeElement($quizz);
            $quizz->removeQuestion($this);
        }

        return $this;
    }

    /**
     * @return Thematic|null
     */
    public function getThematic(): ?Thematic
    {
        return $this->thematic;
    }

    /**
     * @param Thematic|null $thematic
     *
     * @return $this
     */
    public function setThematic(?Thematic $thematic): self
    {
        $this->thematic = $thematic;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExplanation(): ?string
    {
        return $this->explanation;
    }

    /**
     * @param string|null $explanation
     *
     * @return $this
     */
    public function setExplanation(?string $explanation): self
    {
        $this->explanation = $explanation;

        return $this;
    }
}
