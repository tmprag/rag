<?php
/**
 * © Project
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Interfaces\EntityHistoryInterface;
use App\Entity\Interfaces\EntityInterface;
use App\Entity\Traits\CollectionTrait;
use App\Entity\Traits\EntityHistoryTrait;
use App\Repository\QuizzRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=QuizzRepository::class)
 */
class Quizz implements EntityHistoryInterface, EntityInterface
{
    use EntityHistoryTrait;
    use CollectionTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null; // todo : use uuid

    /**
     * @ORM\ManyToMany(targetEntity=Question::class, inversedBy="quizzs")
     */
    private Collection $questions;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $note;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="quizzs")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?UserInterface $candidate;

    /**
     * Quizz constructor.
     */
    public function __construct()
    {
        $this->initDates();
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    /**
     * @param ArrayCollection $questions
     */
    public function setQuestions(ArrayCollection $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @param Question $question
     *
     * @return $this
     */
    public function addQuestion(Question $question): self
    {
        $this->add($question, $this->questions);

        return $this;
    }

    /**
     * @param Question $question
     *
     * @return $this
     */
    public function removeQuestion(Question $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNote(): ?int
    {
        return $this->note;
    }

    /**
     * @param int|null $note
     *
     * @return $this
     */
    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getCandidate(): ?UserInterface
    {
        return $this->candidate;
    }

    /**
     * @param UserInterface|null $candidate
     *
     * @return $this
     */
    public function setCandidate(?UserInterface $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }
}
