<?php
/**
 * © Project
 */

namespace App\Form\DataTransformer;

use App\Service\Traits\LoggerServiceTrait;
use DateTime;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class DateTimeTransformer
 */
class DateTimeTransformer implements DataTransformerInterface
{
    use LoggerServiceTrait;

    /**
     * @param mixed $date
     *
     * @return mixed|null
     */
    public function transform($date)
    {
        if ($date) {
            $dateTime = DateTime::createFromFormat('d/m/Y H:i', $date->format('d/m/Y H:i'), new \DateTimeZone('UTC'));

            if ($dateTime) {
                $dateTime->setTimeZone(new \DateTimeZone('Europe/Paris'));

                return $dateTime->format('d/m/Y H:i');
            }
        }

        return null;
    }

    /**
     * @param mixed $string
     *
     * @throws \Exception
     *
     * @return DateTime|mixed|null
     */
    public function reverseTransform($string)
    {
        $dateTime = DateTime::createFromFormat('d/m/Y H:i:s', $string, new \DateTimeZone('Europe/Paris'));

        if (!$dateTime) {
            $dateTime = DateTime::createFromFormat('d/m/Y H:i', $string, new \DateTimeZone('Europe/Paris'));
        }

        if ($dateTime) {
            $dateTime->setTimeZone(new \DateTimeZone('UTC'));

            return $dateTime;
        }

        return null;
    }
}
