<?php
/**
 * © Project
 */

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class YearTransformer
 */
class YearTransformer implements DataTransformerInterface
{
    /**
     * @param int $year
     *
     * @return mixed|null
     */
    public function transform($year)
    {
        if ($year) {
            return $year;
        }

        return null;
    }

    /**
     * @param mixed $string
     *
     * @return int|null
     */
    public function reverseTransform($string): ?int
    {
        if ($string) {
            return (int) $string;
        }

        return null;
    }
}
