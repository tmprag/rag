<?php
/**
 * © Project
 */

namespace App\Form\DataTransformer;

/**
 * Doc : https://symfony.com/doc/4.4/service_container/autowiring.html#autowiring-calls.
 *
 * Performance Consequences :
 * Thanks to Symfony's compiled container, there is >>>NO PERFORMANCE PENALTY<<< for using autowiring.
 * However, there is a small performance penalty in the >>DEV<< environment, as the container may be rebuilt more often as you modify classes.
 * If rebuilding your container is slow (possible on very large projects), you may not be able to use autowiring.
 */

/**
 * Trait TransformersTrait
 */
trait TransformersTrait
{
    /** @var DateTransformer */
    private $dateTransformer;

    /** @var DateTimeTransformer */
    private $dateTimeTransformer;

    /** @var YearTransformer */
    private $yearTransformer;

    /** @var MonthYearTransformer */
    private $monthYearTransformer;

    /**
     * @return DateTransformer
     */
    public function getDateTransformer(): DateTransformer
    {
        return $this->dateTransformer;
    }

    /**
     * @required
     *
     * @param DateTransformer $dateTransformer
     */
    public function setDateTransformer(DateTransformer $dateTransformer): void
    {
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @return DateTimeTransformer
     */
    public function getDateTimeTransformer(): DateTimeTransformer
    {
        return $this->dateTimeTransformer;
    }

    /**
     * @required
     *
     * @param DateTimeTransformer $dateTimeTransformer
     */
    public function setDateTimeTransformer(DateTimeTransformer $dateTimeTransformer): void
    {
        $this->dateTimeTransformer = $dateTimeTransformer;
    }

    /**
     * @return YearTransformer
     */
    public function getYearTransformer(): YearTransformer
    {
        return $this->yearTransformer;
    }

    /**
     * @required
     *
     * @param YearTransformer $yearTransformer
     */
    public function setYearTransformer(YearTransformer $yearTransformer): void
    {
        $this->yearTransformer = $yearTransformer;
    }

    /**
     * @return MonthYearTransformer
     */
    public function getMonthYearTransformer(): MonthYearTransformer
    {
        return $this->monthYearTransformer;
    }

    /**
     * @required
     *
     * @param MonthYearTransformer $monthYearTransformer
     */
    public function setMonthYearTransformer(MonthYearTransformer $monthYearTransformer): void
    {
        $this->monthYearTransformer = $monthYearTransformer;
    }
}
