<?php
/**
 * © Project
 */

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class MonthYearTransformer
 */
class MonthYearTransformer implements DataTransformerInterface
{
    /**
     * @param string $monthYear
     *
     * @return mixed|null
     */
    public function transform($monthYear)
    {
        if ($monthYear && $monthYear instanceof \DateTime) {
            return $monthYear->format('m/Y');
        }

        return null;
    }

    /**
     * @param mixed $string
     *
     * @return \DateTime|null
     */
    public function reverseTransform($string): ?\DateTime
    {
        $dateTime = \DateTime::createFromFormat('m/Y', $string, new \DateTimeZone('Europe/Paris'));

        if ($dateTime) {
            $dateTime->setTimeZone(new \DateTimeZone('UTC'));

            return $dateTime;
        }

        return null;
    }
}
