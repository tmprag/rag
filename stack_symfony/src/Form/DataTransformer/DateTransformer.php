<?php
/**
 * © Project
 */

namespace App\Form\DataTransformer;

use DateTime;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class DateTransformer
 */
class DateTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $date
     *
     * @return mixed|null
     */
    public function transform($date)
    {
        if ($date) {
            return $date->format('d/m/Y');
        }

        return null;
    }

    /**
     * @param mixed $string
     *
     * @return DateTime|null
     */
    public function reverseTransform($string): ?DateTime
    {
        $dateTime = DateTime::createFromFormat('d/m/Y', $string, new \DateTimeZone('Europe/Paris'));

        if ($dateTime) {
            $dateTime->setTimeZone(new \DateTimeZone('UTC'));

            return $dateTime;
        }

        return null;
    }
}
