<?php
/**
 * © Project
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;

/**
 * Class AbstractFormType
 */
abstract class AbstractFormType extends AbstractType
{
    // custom ...
}
