<?php
/**
 * © Project
 */

namespace App\Form\Elements;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DateTimePickerType
 */
class DateTimePickerType extends AbstractType
{
    /**
     * @return string|null
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'date_time_picker_type';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'readonly' => true,
                'placeholder' => 'Sélectionnez une date',
                'class' => 'form-control datetimepicker',
            ],
        ]);
    }
}
