<?php
/**
 * © Project
 */

namespace App\Form\Elements;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class IntlTelType
 */
class IntlTelType extends AbstractType
{
    /**
     * @return string|null
     */
    public function getParent()
    {
        return TelType::class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'intl_tel_type';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'intlTelInput',
            ],
        ]);
    }
}
