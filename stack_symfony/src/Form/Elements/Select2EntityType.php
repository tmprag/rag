<?php
/**
 * © Project
 */

namespace App\Form\Elements;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class Select2EntityType
 */
class Select2EntityType extends AbstractType
{
    /**
     * @return string|null
     */
    public function getParent()
    {
        return EntityType::class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'select2_entity_type';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'placeholder' => 'Sélectionnez une option',
            'choice_label' => 'name',
            'required' => false,
            'multiple' => false,
            'attr' => [
                'class' => 'autoSelect2EntityType',
                'select2_multiple' => false,
                'select2_minimumInputLength' => 1,
                'select2_minimumResultsForSearch' => 20,
                'select2_placeholder' => 'Sélectionnez une option',
            ],
        ]);
    }
}
