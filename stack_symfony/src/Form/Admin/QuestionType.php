<?php
/**
 * © Project
 */

namespace App\Form\Admin;

use App\Entity\Question;
use App\Entity\Thematic;
use App\Form\AbstractFormType;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuizzType
 */
class QuestionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sentence', TextType::class, [
                'label' => 'Question à poser',
                'attr' => [
                    'placeholder' => 'Entrez une question...',
                ],
            ]);

        $builder->add('thematic', EntityType::class, [
            'label' => 'Thématique',
            'class' => Thematic::class,
            'query_builder' => static function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->orderBy('c.name', 'ASC');
            },
            'choice_label' => 'name',
            'multiple' => false,
            'required' => true,
            'attr' => [
                'placeholder' => 'Choisissez une matière',
            ],
        ]);

        $builder->add('explanation', CKEditorType::class, [
            'config' => [
                'uiColor' => '#ffffff',
                //...
            ],
            'label' => 'Explication(s) (facultatif)',
            'required' => false,
            'attr' => [
                'placeholder' => 'Fournissez une explication',
            ],
        ]);

        $builder->add('answers', CollectionType::class, [
            'label' => false,
            'entry_type' => AnswerType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'by_reference' => false,
            'allow_delete' => true,
        ]);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Ajouter la question',
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}
