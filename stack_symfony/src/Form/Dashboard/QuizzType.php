<?php
/**
 * © Project
 */

namespace App\Form\Dashboard;

use App\Entity\Question;
use App\Entity\Quizz;
use App\Form\AbstractFormType;
use App\Models\Search\Search;
use App\Repository\QuestionRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class QuizzType
 */
class QuizzType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('submit', SubmitType::class, [
            'label' => 'Valider le Quizz',
        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            $data = $event->getData();
            $form = $event->getForm();

            /** @var ManagerRegistry $doctrine */
            $doctrine = $options['data']['doctrine'];
            $doctrineManager = $doctrine->getManager();

            if (null === $data['quizzId']) {
                /** @var QuestionRepository $questionRepository */
                $questionRepository = $doctrine->getRepository(Question::class);

                $questions = [];

                for ($i = 11; 23 >= $i; ++$i) {
                    $questions = \array_merge($questions, $questionRepository
                        ->getRandom((new Search())->setMaxResults(1), $i)
                        ->getResult());
                }

                /** @var Question[] $questions */
                $questions = $this->sortQuestions($questions);

                $quizz = new Quizz();
                $quizz->setCandidate($data['user']);
                $quizz->setNote(null);

                foreach ($questions as $question) {
                    $quizz->addQuestion($question);
                }

                $doctrineManager->persist($quizz);
                $doctrineManager->flush();

                $data['quizzId'] = $quizz->getId();
            }

            $quizzRepository = $doctrineManager->getRepository(Quizz::class);
            /** @var Quizz $quizz */
            $quizz = $quizzRepository->find($data['quizzId']);

            // Update Data
            $data['quizz'] = $quizz;
            $event->setData($data);

            $form->add('quizzId', HiddenType::class, [
                'data' => $quizz->getId(),
            ]);

            $questionNumber = 1;
            foreach ($quizz->getQuestions() as $question) {
                $answerNumber = 1;
                foreach ($question->getAnswers() as $answer) {
                    $inputName = 'question_' . $questionNumber . '_answer_' . $answerNumber;
                    $form->add($inputName, CheckboxType::class, [
                        'required' => false,
                        'label' => $answer->getSentence(),
                    ]);
                    ++$answerNumber;
                }

                ++$questionNumber;
            }
        });
    }

    /**
     * @param $questions
     *
     * @return array
     */
    private function sortQuestions($questions): array
    {
        /*
         * Le fait de faire ->orderBy('rand') dans getRandom() de QuestionRepository mélange tout l'order des questions
         * Il faut retrier le tableau par ID ASC afin que l'ordre des questions soient les mêmes entre
         * la création du form et sa post-validation
         */

        $array = [];

        foreach ($questions as $question) {
            if ($question instanceof Question) {
                $array[$question->getId()] = $question;
            }
        }

        \ksort($array);

        return $array;
    }
}
