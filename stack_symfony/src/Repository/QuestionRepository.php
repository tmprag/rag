<?php
/**
 * © Project
 */

namespace App\Repository;

use App\Entity\Question;
use App\Models\Search\Search;
use App\Service\Traits\SecurityServiceTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    use SecurityServiceTrait;

    /**
     * QuestionRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class);
    }

    /**
     * @param Search $search
     *
     * @return QueryBuilder
     */
    public function getAllQueryBuilder(Search $search): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('q');

        $queryBuilder->select('q');
        $queryBuilder->addOrderBy('q.id', 'ASC');

        $queryBuilder
            ->setFirstResult($search->getFirstResult())
            ->setMaxResults($search->getMaxResults());

        return $queryBuilder;
    }

    /**
     * @param Search $search
     * @param int    $thematicId
     *
     * @return Query
     */
    public function getRandom(Search $search, int $thematicId): Query
    {
        $queryBuilder = $this->getAllQueryBuilder($search);

        $queryBuilder->andWhere('q.thematic = :thematicId')
            ->setParameter('thematicId', $thematicId);

        $queryBuilder->orderBy('RANDOM()');

        return $queryBuilder->getQuery();
    }
}
