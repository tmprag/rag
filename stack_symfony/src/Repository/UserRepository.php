<?php
/**
 * © Project
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class UserRepository
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * UserRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $email
     * @param string $resetPasswordToken
     *
     * @throws NonUniqueResultException
     *
     * @return User|null
     */
    public function findByResetPasswordToken(string $email, string $resetPasswordToken): ?User
    {
        $queryBuilder = $this->createQueryBuilder('u');

        $queryBuilder->andWhere('u.email = :email')
            ->setParameter('email', $email);

        $queryBuilder->andWhere('u.resetPasswordToken = :resetPasswordToken')
            ->setParameter('resetPasswordToken', $resetPasswordToken);

        $queryBuilder->andWhere('u.resetPasswordTokenExpireAt > CURRENT_TIMESTAMP()');

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
