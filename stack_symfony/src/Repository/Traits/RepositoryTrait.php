<?php
/**
 * © Project
 */

namespace App\Repository\Traits;

use App\Models\Search\SearchInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;

/**
 * Trait RepositoryTrait
 */
trait RepositoryTrait
{
    /**
     * @param Collection $collection
     *
     * @return string
     */
    public function getIds(Collection $collection): string
    {
        $ids = [];

        foreach ($collection as $entity) {
            $ids[] = $entity->getId();
        }

        return \implode(',', $ids);
    }

    /**
     * @param QueryBuilder    $queryBuilder
     * @param SearchInterface $search
     * @param callable        $callbackDefaultOrder
     */
    public function order(QueryBuilder $queryBuilder, SearchInterface $search, callable $callbackDefaultOrder)
    {
        if (\method_exists($search, 'getOrders')) {
            if (null !== $search->getOrders()) {
                foreach ($search->getOrders() as $input => $order) {
                    $queryBuilder->orderBy($input, $order);
                }
            } else {
                $callbackDefaultOrder($queryBuilder);
            }
        } else {
            $callbackDefaultOrder($queryBuilder);
        }
    }
}
