<?php
/**
 * © Project
 */

namespace App\Repository;

use App\Entity\Quizz;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Quizz|null find($id, $lockMode = null, $lockVersion = null)
 * @method Quizz|null findOneBy(array $criteria, array $orderBy = null)
 * @method Quizz[]    findAll()
 * @method Quizz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizzRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quizz::class);
    }

    /**
     * @param User $candidate
     *
     * @return int|mixed|string
     */
    public function getValidateQuizzs(User $candidate)
    {
        $queryBuilder = $this->createQueryBuilder('q');
        $queryBuilder->where('q.note is not null')
            ->andWhere('q.candidate = :candidateId')
            ->setParameter('candidateId', $candidate->getId());

        $queryBuilder->orderBy('q.id', Criteria::DESC);

        return $queryBuilder->getQuery()
            ->getResult();
    }

    /**
     * @return int|mixed|string
     */
    public function getAllValidateQuizzs()
    {
        $queryBuilder = $this->createQueryBuilder('q');
        $queryBuilder->where('q.note is not null');

        $queryBuilder->orderBy('q.id', Criteria::DESC);

        return $queryBuilder->getQuery()
            ->getResult();
    }
}
