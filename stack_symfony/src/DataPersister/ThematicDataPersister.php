<?php
/**
 * © Project
 */

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Interfaces\EntityHistoryInterface;
use App\Entity\Thematic;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

/**
 * Class ThematicDataPersister
 */
class ThematicDataPersister implements ContextAwareDataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param Request
     */
    private $request;

    /**
     * @param Security
     */
    private $security;

    /**
     * ThematicDataPersister constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RequestStack           $request
     * @param Security               $security
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $request, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->request       = $request->getCurrentRequest();
        $this->security      = $security;
    }

    /**
     * @param $data
     * @param array $context
     *
     * @return bool
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Thematic;
    }

    /**
     * @param Thematic $data
     * @param array    $context
     *
     * @return object|void
     */
    public function persist($data, array $context = [])
    {
        if ($data instanceof EntityHistoryInterface) {
            // Post
            if (Request::METHOD_POST === $this->request->getMethod()) {
                $data->setCreationDate(new \DateTime('now'));
                $data->setUpdatedDate(new \DateTime('now'));
            }

            // Put, Patch
            if (Request::METHOD_POST !== $this->request->getMethod()) {
                $data->setUpdatedDate(new \DateTime('now'));
            }
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    /**
     * @param $data
     * @param array $context
     */
    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
