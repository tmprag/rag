<?php
/**
 * © Project
 */

namespace App\Security\Voter;

use App\Entity\Question;
use App\Entity\User;
use App\Models\Constant\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class QuestionVoter
 */
class QuestionVoter extends Voter
{
    public const EDIT   = 'question_edit';
    public const DELETE = 'question_delete';

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports(string $attribute, $subject)
    {
        if (!\in_array($attribute, [self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Question) {
            return false;
        }

        return true;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /**
         * @var Question
         */
        $question = $subject;

        return $user->hasRole(Role::ROLE_ADMIN) || $user === $question->getAuthor();
    }
}
