<?php
/**
 * © Project
 */

namespace App\Models\Constant;

interface Routes
{
    public const LOGIN           = 'security_login';
    public const ADMIN_INDEX     = 'admin_index';
    public const DASHBOARD_INDEX = 'dashboard_pages_index';
}
