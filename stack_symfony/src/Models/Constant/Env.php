<?php
/**
 * © Project
 */

namespace App\Models\Constant;

interface Env
{
    public const DEV     = 'dev';
    public const TEST    = 'test';
    public const STAGING = 'staging';
    public const PREPROD = 'preprod';
    public const PROD    = 'prod';
}
