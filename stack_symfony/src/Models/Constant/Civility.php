<?php
/**
 * © Project
 */

namespace App\Models\Constant;

interface Civility
{
    public const MAN   = 0;
    public const WOMAN = 1;
}
