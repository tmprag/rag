<?php
/**
 * © Project
 */

namespace App\Models\Constant;

interface Role
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_USER  = 'ROLE_USER';
}
