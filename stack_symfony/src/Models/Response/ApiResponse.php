<?php
/**
 * © Project
 */

namespace App\Models\Response;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ApiResponse
 */
class ApiResponse extends JsonResponse
{
    /**
     * @var string
     */
    public const STATUS_SUCCESS = 'success';

    /**
     * @var string
     */
    public const STATUS_ERROR = 'error';

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $context;

    /**
     * ApiResponse constructor.
     *
     * @param null   $data
     * @param string $status
     * @param $messages
     * @param string|null $context
     *
     * @throws Exception
     */
    public function __construct($data = null, string $status = self::STATUS_SUCCESS, $messages = [], string $context = null)
    {
        switch ($status) {
            case self::STATUS_SUCCESS:
            case self::STATUS_ERROR:
                parent::setStatusCode(self::HTTP_OK);
                $this->status = $status;
                break;
            default:
                throw new Exception('Status ' . $status . ' not allowed !');
                break;
        }

        $this->context = $context;

        if (\is_array($messages)) {
            $this->message = $messages;
        } else {
            $this->message = [$messages];
        }

        parent::__construct($this->formatData($data));
    }

    /**
     * @param string|null $context
     *
     * @return $this
     */
    public function setContext($context): self
    {
        $context       = \mb_strtolower($context);
        $context       = \str_replace(' ', '_', $context);
        $this->context = $context;

        return $this;
    }

    /**
     * @param null $data
     *
     * @return array
     */
    private function formatData($data = null): array
    {
        $response = [
            'status' => $this->status,
            'message' => $this->message,
            'data' => $data,
        ];

        if (null !== $this->context) {
            $response['context'] = $this->context;
        }

        return $response;
    }
}
