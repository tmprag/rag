<?php
/**
 * © Project
 */

namespace App\Models\Search\Traits;

/**
 * Trait OrderTrait
 */
trait OrderTrait
{
    /**
     * @var array
     */
    private $orders;

    /**
     * @return array|null
     */
    public function getOrders(): ?array
    {
        return $this->orders;
    }

    /**
     * @param array|null $orders
     *
     * @return $this
     */
    public function setOrders(?array $orders): self
    {
        $this->orders = $orders;

        return $this;
    }
}
