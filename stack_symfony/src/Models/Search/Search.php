<?php
/**
 * © Project
 */

namespace App\Models\Search;

use App\Models\Search\Traits\LimitTrait;
use App\Models\Search\Traits\OrderTrait;

/**
 * Class Search
 */
class Search implements SearchInterface
{
    use OrderTrait;
    use LimitTrait;
}
