<?php
/**
 * © Project
 */

namespace App\Models;

/**
 * Class HistoryEvent
 */
class HistoryEvent
{
    /**
     * @var
     */
    private $color;

    /**
     * @var
     */
    private $label;

    /**
     * @var
     */
    private $label2;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     *
     * @return HistoryEvent
     */
    public function setColor($color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     *
     * @return HistoryEvent
     */
    public function setLabel($label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel2()
    {
        return $this->label2;
    }

    /**
     * @param mixed $label2
     *
     * @return HistoryEvent
     */
    public function setLabel2($label2): self
    {
        $this->label2 = $label2;

        return $this;
    }

    /**
     * @return array
     */
    public function format()
    {
        return [
            'color' => $this->color,
            'label' => $this->label,
            'label2' => $this->label2,
        ];
    }

    /**
     * @return $this
     */
    public function addMod(): self
    {
        $this->setColor('green')
            ->setLabel('Ajout');

        return $this;
    }

    /**
     * @return $this
     */
    public function editMod(): self
    {
        $this->setColor('orange')
            ->setLabel('Modification');

        return $this;
    }

    /**
     * @return $this
     */
    public function deleteMod(): self
    {
        $this->setColor('red')
            ->setLabel('Suppression');

        return $this;
    }
}
