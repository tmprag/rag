<?php
/**
 * © Project
 */

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserService
 */
class UserService extends AbstractEntityService
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserService constructor.
     *
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface       $entityManagerInterface
     */
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManagerInterface)
    {
        parent::__construct($entityManagerInterface, User::class);
        $this->encoder = $encoder;
    }
}
