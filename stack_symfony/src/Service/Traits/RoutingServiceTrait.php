<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use Symfony\Component\Routing\RouterInterface;

/**
 * Trait RoutingServiceTrait
 */
trait RoutingServiceTrait
{
    /** @var RouterInterface */
    private $router;

    /**
     * @return RouterInterface
     */
    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    /**
     * @required
     *
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }
}
