<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use Symfony\Component\Security\Core\Security;

/**
 * Trait SecurityServiceTrait.
 */
trait SecurityServiceTrait
{
    /** @var Security */
    private $security;

    /**
     * @return Security
     */
    public function getSecurity(): Security
    {
        return $this->security;
    }

    /**
     * @required
     *
     * @param Security $security
     */
    public function setSecurity(Security $security): void
    {
        $this->security = $security;
    }
}
