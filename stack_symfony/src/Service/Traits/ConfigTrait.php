<?php
/**
 * © Project
 */

namespace App\Service\Traits;

/**
 * Trait ConfigTrait
 */
trait ConfigTrait
{
    /**
     * @var string
     */
    private $projectDir;

    /**
     * @return string
     */
    public function getProjectDir(): string
    {
        return $this->projectDir;
    }

    /**
     * @required
     *
     * @param string $projectDir
     */
    public function setProjectDir(string $projectDir): void
    {
        $this->projectDir = $projectDir;
    }
}
