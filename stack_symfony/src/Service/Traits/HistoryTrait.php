<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use App\Service\CandidateHistoryService;
use App\Service\CompanyStaffHistoryService;
use App\Service\RemunerationHistoryService;

/**
 * Trait HistoryTrait.
 */
trait HistoryTrait
{
    /** @var CandidateHistoryService */
    private $candidateHistoryService;

    /** @var CompanyStaffHistoryService */
    private $companyStaffHistoryService;

    /** @var RemunerationHistoryService */
    private $remunerationHistoryService;

    /**
     * @return CandidateHistoryService
     */
    public function getCandidateHistoryService(): CandidateHistoryService
    {
        return $this->candidateHistoryService;
    }

    /**
     * @required
     *
     * @param CandidateHistoryService $candidateHistoryService
     */
    public function setCandidateHistoryService(CandidateHistoryService $candidateHistoryService): void
    {
        $this->candidateHistoryService = $candidateHistoryService;
    }

    /**
     * @return CompanyStaffHistoryService
     */
    public function getCompanyStaffHistoryService(): CompanyStaffHistoryService
    {
        return $this->companyStaffHistoryService;
    }

    /**
     * @required
     *
     * @param CompanyStaffHistoryService $companyStaffHistoryService
     */
    public function setCompanyStaffHistoryService(CompanyStaffHistoryService $companyStaffHistoryService): void
    {
        $this->companyStaffHistoryService = $companyStaffHistoryService;
    }

    /**
     * @return RemunerationHistoryService
     */
    public function getRemunerationHistoryService(): RemunerationHistoryService
    {
        return $this->remunerationHistoryService;
    }

    /**
     * @required
     *
     * @param RemunerationHistoryService $remunerationHistoryService
     */
    public function setRemunerationHistoryService(RemunerationHistoryService $remunerationHistoryService): void
    {
        $this->remunerationHistoryService = $remunerationHistoryService;
    }
}
