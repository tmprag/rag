<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use App\Service\QuestionService;
use App\Service\UserService;

/**
 * Doc : https://symfony.com/doc/4.4/service_container/autowiring.html#autowiring-calls.
 *
 * Performance Consequences :
 * Thanks to Symfony's compiled container, there is >>>NO PERFORMANCE PENALTY<<< for using autowiring.
 * However, there is a small performance penalty in the >>DEV<< environment, as the container may be rebuilt more often as you modify classes.
 * If rebuilding your container is slow (possible on very large projects), you may not be able to use autowiring.
 */

/**
 * Trait AppServicesTrait.
 */
trait AppServicesTrait
{
    /** @var UserService */
    private UserService $userService;

    /** @var QuestionService */
    private QuestionService $questionService;

    /**
     * @return UserService
     */
    public function getUserService(): UserService
    {
        return $this->userService;
    }

    /**
     * @required
     *
     * @param UserService $userService
     */
    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * @return QuestionService
     */
    public function getQuestionService(): QuestionService
    {
        return $this->questionService;
    }

    /**
     * @required
     *
     * @param QuestionService $questionService
     */
    public function setQuestionService(QuestionService $questionService): void
    {
        $this->questionService = $questionService;
    }
}
