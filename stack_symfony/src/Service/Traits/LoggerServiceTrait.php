<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use Psr\Log\LoggerInterface;

/**
 * Trait LoggerServiceTrait
 */
trait LoggerServiceTrait
{
    /** @var LoggerInterface */
    private $logger;

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @required
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
