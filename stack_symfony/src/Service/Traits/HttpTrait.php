<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Trait HttpTrait
 */
trait HttpTrait
{
    /**
     * @var RequestStack|null
     */
    private $requestStack;

    /**
     * @return RequestStack|null
     */
    public function getRequestStack(): ?RequestStack
    {
        return $this->requestStack;
    }

    /**
     * @required
     *
     * @param RequestStack|null $requestStack
     */
    public function setRequestStack(?RequestStack $requestStack): void
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $varname
     *
     * @return string
     */
    public function getEnv(string $varname): string
    {
        return $this->requestStack->getCurrentRequest()->server->get($varname);
    }
}
