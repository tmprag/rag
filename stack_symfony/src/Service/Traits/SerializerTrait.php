<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use Symfony\Component\Serializer\SerializerInterface;

/**
 * Trait SerializeTrait
 */
trait SerializerTrait
{
    /** @var SerializerInterface */
    private $serializer;

    /**
     * @return SerializerInterface
     */
    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    /**
     * @required
     *
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }
}
