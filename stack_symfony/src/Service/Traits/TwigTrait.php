<?php
/**
 * © Project
 */

namespace App\Service\Traits;

use Twig\Environment as Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Trait TwigTrait
 */
trait TwigTrait
{
    /**
     * @var Twig
     */
    private Twig $twig;

    /**
     * @return Twig
     */
    public function getTwig(): Twig
    {
        return $this->twig;
    }

    /**
     * @required
     *
     * @param Twig $twig
     */
    public function setTwig(Twig $twig): void
    {
        $this->twig = $twig;
    }

    /**
     * @param string $name
     * @param array  $context
     *
     * @return string
     */
    public function renderView(string $name, array $context = []): string
    {
        try {
            return $this->getTwig()->render($name, $context);
        } catch (LoaderError $e) {
            return 'Error: ' . $e->getMessage();
        } catch (RuntimeError $e) {
            return 'Error: ' . $e->getMessage();
        } catch (SyntaxError $e) {
            return 'Error: ' . $e->getMessage();
        }
    }
}
