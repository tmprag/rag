<?php
/**
 * © Project
 */

namespace App\Service;

use App\Models\Constant\Env;
use App\Service\Traits\HttpTrait;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

/**
 * Class MailerService.
 */
class MailerService
{
    use HttpTrait;

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * MailerService constructor.
     *
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string      $subject
     * @param string      $template
     * @param array       $context
     * @param string|null $to
     * @param null        $fromEmail
     * @param null        $fromName
     *
     * @throws TransportExceptionInterface
     */
    public function sendMessage(string $subject, string $template, array $context = [], string $to = null, $fromEmail = null, $fromName = null)
    {
        if (Env::DEV === $this->getEnv('APP_ENV')) {
            $to = $this->getEnv('MAILER_DEV_TO');
        }

        if (null === $fromEmail) {
            $fromEmail = $this->getEnv('MAILER_DEFAULT_FROM');
        }

        if (null === $fromName) {
            $fromName = $this->getEnv('PROJECT_TITLE');
        }

        $emailTemplate = (new TemplatedEmail())
            ->from(Address::fromString($fromName . ' <' . $fromEmail . '>'))
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($context);

        $this->mailer->send($emailTemplate);
    }
}
