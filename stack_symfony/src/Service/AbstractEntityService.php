<?php
/**
 * © Project
 */

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Persistence\ObjectRepository;

/**
 * Class AbstractEntityService
 */
abstract class AbstractEntityService
{
    /**
     * @var ObjectRepository
     */
    protected $entityRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManagerInterface;

    /**
     * AbstractEntityService constructor.
     *
     * @param EntityManagerInterface $entityManagerInterface
     * @param string                 $entityClassName
     */
    public function __construct(EntityManagerInterface $entityManagerInterface, string $entityClassName)
    {
        $this->entityManagerInterface = $entityManagerInterface;
        $this->entityRepository       = $entityManagerInterface->getRepository($entityClassName);
    }

    /**
     * @param array $param
     * @param array $sort
     *
     * @return array
     */
    public function findAll($param = [], $sort = []): array
    {
        return $this->entityRepository->findBy($param, $sort);
    }

    /**
     * @param int $id
     *
     * @return object|null
     */
    public function findById(int $id)
    {
        return $this->entityRepository->find($id);
    }

    /**
     * @param array $criteria
     *
     * @return object|null
     */
    public function findOneBy(array $criteria)
    {
        return $this->entityRepository->findOneBy($criteria);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return object[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->entityRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param $entity
     * @param bool $flush
     *
     * @return mixed
     */
    public function add($entity, $flush = true)
    {
        $this->entityManagerInterface->persist($entity);
        if ($flush) {
            $this->entityManagerInterface->flush();
        }

        return $entity;
    }

    /**
     * @param $entity
     * @param bool $flush
     */
    public function update($entity, $flush = true): void
    {
        $this->entityManagerInterface->persist($entity);
        if ($flush) {
            $this->entityManagerInterface->flush();
        }
    }

    /**
     * @param $entity
     */
    public function delete($entity): void
    {
        $this->entityManagerInterface->remove($entity);
        $this->entityManagerInterface->flush();
    }

    public function flush(): void
    {
        $this->entityManagerInterface->flush();
    }

    public function clear(): void
    {
        $this->entityManagerInterface->clear();
    }

    /**
     * @return UnitOfWork
     */
    public function getUnitOfWork(): UnitOfWork
    {
        return $this->entityManagerInterface->getUnitOfWork();
    }

    /**
     * @return ObjectRepository
     */
    public function getEntityRepository(): ObjectRepository
    {
        return $this->entityRepository;
    }

    /**
     * @param ObjectRepository $entityRepository
     */
    public function setEntityRepository(ObjectRepository $entityRepository): void
    {
        $this->entityRepository = $entityRepository;
    }
}
