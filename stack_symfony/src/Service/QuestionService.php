<?php
/**
 * © Project
 */

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class QuestionService
 */
class QuestionService extends AbstractEntityService
{
    /**
     * QuestionService constructor.
     *
     * @param EntityManagerInterface $entityManagerInterface
     */
    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        parent::__construct($entityManagerInterface, User::class);
    }
}
