<?php
/**
 * © Project
 */

namespace App\Utils;

use ReflectionException;

/**
 * Class Interfaces
 */
class Interfaces
{
    /**
     * @param string $interfaceName
     *
     * @throws ReflectionException
     *
     * @return array
     */
    public static function getClassesImplementsInterface(string $interfaceName): array
    {
        $classes           = \get_declared_classes();
        $implementsIModule = [];
        foreach ($classes as $klass) {
            $reflect = new \ReflectionClass($klass);
            if ($reflect->implementsInterface($interfaceName)) {
                $implementsIModule[] = $klass;
            }
        }

        return $implementsIModule;
    }
}
