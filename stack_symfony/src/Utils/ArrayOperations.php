<?php
/**
 * © Project
 */

namespace App\Utils;

/**
 * Class ArrayOperations
 */
class ArrayOperations
{
    /**
     * @param array $in
     *
     * @return float|int|null
     */
    public static function arrayAverage(array $in): ?int
    {
        $in = \array_filter($in);
        if (\count($in)) {
            return \array_sum($in) / \count($in);
        }

        return null;
    }
}
