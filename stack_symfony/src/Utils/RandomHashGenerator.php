<?php
/**
 * © Project
 */

namespace App\Utils;

/**
 * Class RandomHashGenerator
 */
class RandomHashGenerator
{
    /**
     * @throws \Exception
     *
     * @return string
     */
    public static function generate(): string
    {
        return \md5(\random_bytes(30));
    }
}
