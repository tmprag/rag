<?php
/**
 * © Project
 */

namespace App\Utils;

/**
 * Class ArrayOperations
 */
class TransformValues
{
    /**
     * @param int $quizzPoints
     *
     * @return string
     */
    public static function commentByQuizzPoints(int $quizzPoints): string
    {
        if (0 <= $quizzPoints && 3 >= $quizzPoints) {
            $comment = 'Vous êtes vraiement pas fait(e)s pour ce métier !';
        } elseif (3 <= $quizzPoints && 5 >= $quizzPoints) {
            $comment = "Je vois autant de potentiel qu'un escargot aux jeux olympiques !";
        } elseif (5 <= $quizzPoints && 7 >= $quizzPoints) {
            $comment = 'Ca commence à être bien... mais bossez plus que ça !!';
        } elseif (7 <= $quizzPoints && 9 >= $quizzPoints) {
            $comment = 'Oh peut etre que vous ferriez un bon... second !! A dieu à la première place !';
        } else {
            $comment = "Enfin quelqu'un digne du BP Préparateur en Pharmacie !";
        }

        return $comment;
    }
}
