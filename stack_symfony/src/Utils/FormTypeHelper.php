<?php
/**
 * © Project
 */

declare(strict_types = 1);
namespace App\Utils;

use App\Service\AbstractEntityService;
use Doctrine\Common\Collections\Collection;
use Exception;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class FormTypeHelper
 */
abstract class FormTypeHelper
{
    /** @var FormBuilderInterface */
    private $builder;

    /** @var AbstractEntityService */
    private $service;

    /** @var mixed */
    private $choices;

    /** @var bool */
    private $byReference = false;

    /**
     * @var DataTransformerInterface|null
     */
    private $transformer;

    /**
     * FormTypeHelper constructor.
     *
     * @param FormBuilderInterface          $builder
     * @param AbstractEntityService|null    $service
     * @param DataTransformerInterface|null $transformer
     */
    public function __construct(FormBuilderInterface $builder, AbstractEntityService $service = null, DataTransformerInterface $transformer = null)
    {
        $this->builder     = $builder;
        $this->service     = $service;
        $this->transformer = $transformer;
    }

    /**
     * @return mixed
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * @param $choices
     */
    public function setChoices($choices)
    {
        if (!$choices instanceof Collection && !\is_array($choices)) {
            $this->choices = [$choices];
        } else {
            $this->choices = $choices;
        }
    }

    /**
     * @return FormBuilderInterface
     */
    public function getBuilder(): FormBuilderInterface
    {
        return $this->builder;
    }

    /**
     * @param FormBuilderInterface $builder
     */
    public function setBuilder(FormBuilderInterface $builder): void
    {
        $this->builder = $builder;
    }

    /**
     * @return AbstractEntityService|null
     */
    public function getService(): ?AbstractEntityService
    {
        return $this->service;
    }

    /**
     * @param AbstractEntityService $service
     *
     * @return $this
     */
    public function setService(AbstractEntityService $service): self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return DataTransformerInterface|null
     */
    public function getTransformer(): ?DataTransformerInterface
    {
        return $this->transformer;
    }

    /**
     * @param DataTransformerInterface|null $transformer
     */
    public function setTransformer(?DataTransformerInterface $transformer): void
    {
        $this->transformer = $transformer;
    }

    /**
     * @return bool
     */
    public function isByReference(): bool
    {
        return $this->byReference;
    }

    /**
     * @param bool $byReference
     *
     * @return $this
     */
    public function setByReference(bool $byReference): self
    {
        $this->byReference = $byReference;

        return $this;
    }

    /**
     * @param $object
     * @param string $methodName
     *
     * @throws Exception
     *
     * @return mixed
     */
    private function callMethod($object, string $methodName)
    {
        if (!\method_exists($object, $methodName)) {
            throw new Exception('The method ' . $methodName . ' does not exist in entity ' . \get_class($object));
        }

        return \call_user_func_array([$object, $methodName], []);
    }
}
