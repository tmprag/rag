<?php
/**
 * © Project
 */

namespace App\Utils;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Classes
 */
class Classes
{
    /**
     * @param string $className
     *
     * @return string
     */
    public static function getClassNameFromFullyQualifiedName(string $className): string
    {
        return \mb_substr(\mb_strrchr($className, '\\'), 1);
    }

    /**
     * @param string $className
     *
     * @return string
     */
    public static function getFactoryTestFromClassName(string $className): string
    {
        return 'App\Tests\Unit\Base\Factory\\' . Classes::getClassNameFromFullyQualifiedName($className . 'Factory');
    }

    /**
     * @param EntityManagerInterface $entityManager
     *
     * @return array
     */
    public static function getEntities(EntityManagerInterface $entityManager)
    {
        $entities = [];
        $meta     = $entityManager->getMetadataFactory()->getAllMetadata();
        foreach ($meta as $m) {
            $entities[] = $m->getName();
        }

        return $entities;
    }
}
