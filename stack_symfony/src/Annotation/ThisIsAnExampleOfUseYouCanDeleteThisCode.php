<?php
/**
 * © Project
 */

namespace App\Annotation;

/**
 * @Annotation
 * @Target({"PROPERTY","CLASS"})
 */
final class ThisIsAnExampleOfUseYouCanDeleteThisCode
{
}
