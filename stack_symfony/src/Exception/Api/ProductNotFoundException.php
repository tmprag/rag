<?php
/**
 * © Project
 */

namespace App\Exception\Api;

/**
 * Class ProductNotFoundException
 */
class ProductNotFoundException extends \Exception
{
}
