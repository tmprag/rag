<?php
/**
 * © Project
 */

namespace App\Exception\Api;

/**
 * Class UnauthorizedException
 */
class UnauthorizedException extends \Exception
{
}
