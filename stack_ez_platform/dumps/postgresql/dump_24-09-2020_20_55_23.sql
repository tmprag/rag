--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE rag;




--
-- Drop roles
--

DROP ROLE rag;


--
-- Roles
--

CREATE ROLE rag;
ALTER ROLE rag WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md51f6ab6a0219934826bc16b81dde5069d';






--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: rag
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO rag;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: rag
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: rag
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: rag
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "rag" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: rag; Type: DATABASE; Schema: -; Owner: rag
--

CREATE DATABASE rag WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE rag OWNER TO rag;

\connect rag

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO rag;

--
-- Name: ezbinaryfile; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezbinaryfile (
    contentobject_attribute_id integer DEFAULT 0 NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    download_count integer DEFAULT 0 NOT NULL,
    filename character varying(255) DEFAULT ''::character varying NOT NULL,
    mime_type character varying(255) DEFAULT ''::character varying NOT NULL,
    original_filename character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.ezbinaryfile OWNER TO rag;

--
-- Name: ezcobj_state; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcobj_state (
    id integer NOT NULL,
    default_language_id bigint DEFAULT 0 NOT NULL,
    group_id integer DEFAULT 0 NOT NULL,
    identifier character varying(45) DEFAULT ''::character varying NOT NULL,
    language_mask bigint DEFAULT 0 NOT NULL,
    priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezcobj_state OWNER TO rag;

--
-- Name: ezcobj_state_group; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcobj_state_group (
    id integer NOT NULL,
    default_language_id bigint DEFAULT 0 NOT NULL,
    identifier character varying(45) DEFAULT ''::character varying NOT NULL,
    language_mask bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezcobj_state_group OWNER TO rag;

--
-- Name: ezcobj_state_group_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcobj_state_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcobj_state_group_id_seq OWNER TO rag;

--
-- Name: ezcobj_state_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcobj_state_group_id_seq OWNED BY public.ezcobj_state_group.id;


--
-- Name: ezcobj_state_group_language; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcobj_state_group_language (
    contentobject_state_group_id integer DEFAULT 0 NOT NULL,
    real_language_id bigint DEFAULT 0 NOT NULL,
    description text NOT NULL,
    language_id bigint DEFAULT 0 NOT NULL,
    name character varying(45) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.ezcobj_state_group_language OWNER TO rag;

--
-- Name: ezcobj_state_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcobj_state_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcobj_state_id_seq OWNER TO rag;

--
-- Name: ezcobj_state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcobj_state_id_seq OWNED BY public.ezcobj_state.id;


--
-- Name: ezcobj_state_language; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcobj_state_language (
    contentobject_state_id integer DEFAULT 0 NOT NULL,
    language_id bigint DEFAULT 0 NOT NULL,
    description text NOT NULL,
    name character varying(45) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.ezcobj_state_language OWNER TO rag;

--
-- Name: ezcobj_state_link; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcobj_state_link (
    contentobject_id integer DEFAULT 0 NOT NULL,
    contentobject_state_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezcobj_state_link OWNER TO rag;

--
-- Name: ezcontent_language; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontent_language (
    id bigint DEFAULT 0 NOT NULL,
    disabled integer DEFAULT 0 NOT NULL,
    locale character varying(20) DEFAULT ''::character varying NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.ezcontent_language OWNER TO rag;

--
-- Name: ezcontentbrowsebookmark; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentbrowsebookmark (
    id integer NOT NULL,
    node_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.ezcontentbrowsebookmark OWNER TO rag;

--
-- Name: ezcontentbrowsebookmark_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentbrowsebookmark_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentbrowsebookmark_id_seq OWNER TO rag;

--
-- Name: ezcontentbrowsebookmark_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentbrowsebookmark_id_seq OWNED BY public.ezcontentbrowsebookmark.id;


--
-- Name: ezcontentclass; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentclass (
    id integer NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    always_available integer DEFAULT 0 NOT NULL,
    contentobject_name character varying(255) DEFAULT NULL::character varying,
    created integer DEFAULT 0 NOT NULL,
    creator_id integer DEFAULT 0 NOT NULL,
    identifier character varying(50) DEFAULT ''::character varying NOT NULL,
    initial_language_id bigint DEFAULT 0 NOT NULL,
    is_container integer DEFAULT 0 NOT NULL,
    language_mask bigint DEFAULT 0 NOT NULL,
    modified integer DEFAULT 0 NOT NULL,
    modifier_id integer DEFAULT 0 NOT NULL,
    remote_id character varying(100) DEFAULT ''::character varying NOT NULL,
    serialized_description_list text,
    serialized_name_list text,
    sort_field integer DEFAULT 1 NOT NULL,
    sort_order integer DEFAULT 1 NOT NULL,
    url_alias_name character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.ezcontentclass OWNER TO rag;

--
-- Name: ezcontentclass_attribute; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentclass_attribute (
    id integer NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    can_translate integer DEFAULT 1,
    category character varying(25) DEFAULT ''::character varying NOT NULL,
    contentclass_id integer DEFAULT 0 NOT NULL,
    data_float1 double precision,
    data_float2 double precision,
    data_float3 double precision,
    data_float4 double precision,
    data_int1 integer,
    data_int2 integer,
    data_int3 integer,
    data_int4 integer,
    data_text1 character varying(255) DEFAULT NULL::character varying,
    data_text2 character varying(50) DEFAULT NULL::character varying,
    data_text3 character varying(50) DEFAULT NULL::character varying,
    data_text4 character varying(255) DEFAULT NULL::character varying,
    data_text5 text,
    data_type_string character varying(50) DEFAULT ''::character varying NOT NULL,
    identifier character varying(50) DEFAULT ''::character varying NOT NULL,
    is_information_collector integer DEFAULT 0 NOT NULL,
    is_required integer DEFAULT 0 NOT NULL,
    is_searchable integer DEFAULT 0 NOT NULL,
    is_thumbnail boolean DEFAULT false NOT NULL,
    placement integer DEFAULT 0 NOT NULL,
    serialized_data_text text,
    serialized_description_list text,
    serialized_name_list text NOT NULL
);


ALTER TABLE public.ezcontentclass_attribute OWNER TO rag;

--
-- Name: ezcontentclass_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentclass_attribute_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentclass_attribute_id_seq OWNER TO rag;

--
-- Name: ezcontentclass_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentclass_attribute_id_seq OWNED BY public.ezcontentclass_attribute.id;


--
-- Name: ezcontentclass_attribute_ml; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentclass_attribute_ml (
    contentclass_attribute_id integer NOT NULL,
    version integer NOT NULL,
    language_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    data_text text,
    data_json text
);


ALTER TABLE public.ezcontentclass_attribute_ml OWNER TO rag;

--
-- Name: ezcontentclass_classgroup; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentclass_classgroup (
    contentclass_id integer DEFAULT 0 NOT NULL,
    contentclass_version integer DEFAULT 0 NOT NULL,
    group_id integer DEFAULT 0 NOT NULL,
    group_name character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.ezcontentclass_classgroup OWNER TO rag;

--
-- Name: ezcontentclass_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentclass_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentclass_id_seq OWNER TO rag;

--
-- Name: ezcontentclass_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentclass_id_seq OWNED BY public.ezcontentclass.id;


--
-- Name: ezcontentclass_name; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentclass_name (
    contentclass_id integer DEFAULT 0 NOT NULL,
    contentclass_version integer DEFAULT 0 NOT NULL,
    language_id bigint DEFAULT 0 NOT NULL,
    language_locale character varying(20) DEFAULT ''::character varying NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.ezcontentclass_name OWNER TO rag;

--
-- Name: ezcontentclassgroup; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentclassgroup (
    id integer NOT NULL,
    created integer DEFAULT 0 NOT NULL,
    creator_id integer DEFAULT 0 NOT NULL,
    modified integer DEFAULT 0 NOT NULL,
    modifier_id integer DEFAULT 0 NOT NULL,
    name character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.ezcontentclassgroup OWNER TO rag;

--
-- Name: ezcontentclassgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentclassgroup_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentclassgroup_id_seq OWNER TO rag;

--
-- Name: ezcontentclassgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentclassgroup_id_seq OWNED BY public.ezcontentclassgroup.id;


--
-- Name: ezcontentobject; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentobject (
    id integer NOT NULL,
    contentclass_id integer DEFAULT 0 NOT NULL,
    current_version integer,
    initial_language_id bigint DEFAULT 0 NOT NULL,
    language_mask bigint DEFAULT 0 NOT NULL,
    modified integer DEFAULT 0 NOT NULL,
    name character varying(255) DEFAULT NULL::character varying,
    owner_id integer DEFAULT 0 NOT NULL,
    published integer DEFAULT 0 NOT NULL,
    remote_id character varying(100) DEFAULT NULL::character varying,
    section_id integer DEFAULT 0 NOT NULL,
    status integer DEFAULT 0,
    is_hidden boolean DEFAULT false NOT NULL
);


ALTER TABLE public.ezcontentobject OWNER TO rag;

--
-- Name: ezcontentobject_attribute; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentobject_attribute (
    id integer NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    attribute_original_id integer DEFAULT 0,
    contentclassattribute_id integer DEFAULT 0 NOT NULL,
    contentobject_id integer DEFAULT 0 NOT NULL,
    data_float double precision,
    data_int integer,
    data_text text,
    data_type_string character varying(50) DEFAULT ''::character varying,
    language_code character varying(20) DEFAULT ''::character varying NOT NULL,
    language_id bigint DEFAULT 0 NOT NULL,
    sort_key_int integer DEFAULT 0 NOT NULL,
    sort_key_string character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.ezcontentobject_attribute OWNER TO rag;

--
-- Name: ezcontentobject_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentobject_attribute_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentobject_attribute_id_seq OWNER TO rag;

--
-- Name: ezcontentobject_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentobject_attribute_id_seq OWNED BY public.ezcontentobject_attribute.id;


--
-- Name: ezcontentobject_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentobject_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentobject_id_seq OWNER TO rag;

--
-- Name: ezcontentobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentobject_id_seq OWNED BY public.ezcontentobject.id;


--
-- Name: ezcontentobject_link; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentobject_link (
    id integer NOT NULL,
    contentclassattribute_id integer DEFAULT 0 NOT NULL,
    from_contentobject_id integer DEFAULT 0 NOT NULL,
    from_contentobject_version integer DEFAULT 0 NOT NULL,
    relation_type integer DEFAULT 1 NOT NULL,
    to_contentobject_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezcontentobject_link OWNER TO rag;

--
-- Name: ezcontentobject_link_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentobject_link_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentobject_link_id_seq OWNER TO rag;

--
-- Name: ezcontentobject_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentobject_link_id_seq OWNED BY public.ezcontentobject_link.id;


--
-- Name: ezcontentobject_name; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentobject_name (
    contentobject_id integer DEFAULT 0 NOT NULL,
    content_version integer DEFAULT 0 NOT NULL,
    content_translation character varying(20) DEFAULT ''::character varying NOT NULL,
    language_id bigint DEFAULT 0 NOT NULL,
    name character varying(255) DEFAULT NULL::character varying,
    real_translation character varying(20) DEFAULT NULL::character varying
);


ALTER TABLE public.ezcontentobject_name OWNER TO rag;

--
-- Name: ezcontentobject_trash; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentobject_trash (
    node_id integer DEFAULT 0 NOT NULL,
    contentobject_id integer,
    contentobject_version integer,
    depth integer DEFAULT 0 NOT NULL,
    is_hidden integer DEFAULT 0 NOT NULL,
    is_invisible integer DEFAULT 0 NOT NULL,
    main_node_id integer,
    modified_subnode integer DEFAULT 0,
    parent_node_id integer DEFAULT 0 NOT NULL,
    path_identification_string text,
    path_string character varying(255) DEFAULT ''::character varying NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    remote_id character varying(100) DEFAULT ''::character varying NOT NULL,
    sort_field integer DEFAULT 1,
    sort_order integer DEFAULT 1,
    trashed integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezcontentobject_trash OWNER TO rag;

--
-- Name: ezcontentobject_tree; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentobject_tree (
    node_id integer NOT NULL,
    contentobject_id integer,
    contentobject_is_published integer,
    contentobject_version integer,
    depth integer DEFAULT 0 NOT NULL,
    is_hidden integer DEFAULT 0 NOT NULL,
    is_invisible integer DEFAULT 0 NOT NULL,
    main_node_id integer,
    modified_subnode integer DEFAULT 0,
    parent_node_id integer DEFAULT 0 NOT NULL,
    path_identification_string text,
    path_string character varying(255) DEFAULT ''::character varying NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    remote_id character varying(100) DEFAULT ''::character varying NOT NULL,
    sort_field integer DEFAULT 1,
    sort_order integer DEFAULT 1
);


ALTER TABLE public.ezcontentobject_tree OWNER TO rag;

--
-- Name: ezcontentobject_tree_node_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentobject_tree_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentobject_tree_node_id_seq OWNER TO rag;

--
-- Name: ezcontentobject_tree_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentobject_tree_node_id_seq OWNED BY public.ezcontentobject_tree.node_id;


--
-- Name: ezcontentobject_version; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezcontentobject_version (
    id integer NOT NULL,
    contentobject_id integer,
    created integer DEFAULT 0 NOT NULL,
    creator_id integer DEFAULT 0 NOT NULL,
    initial_language_id bigint DEFAULT 0 NOT NULL,
    language_mask bigint DEFAULT 0 NOT NULL,
    modified integer DEFAULT 0 NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    workflow_event_pos integer DEFAULT 0
);


ALTER TABLE public.ezcontentobject_version OWNER TO rag;

--
-- Name: ezcontentobject_version_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezcontentobject_version_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezcontentobject_version_id_seq OWNER TO rag;

--
-- Name: ezcontentobject_version_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezcontentobject_version_id_seq OWNED BY public.ezcontentobject_version.id;


--
-- Name: ezdfsfile; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezdfsfile (
    name_hash character varying(34) DEFAULT ''::character varying NOT NULL,
    name text NOT NULL,
    name_trunk text NOT NULL,
    datatype character varying(255) DEFAULT 'application/octet-stream'::character varying NOT NULL,
    scope character varying(25) DEFAULT ''::character varying NOT NULL,
    size bigint DEFAULT 0 NOT NULL,
    mtime integer DEFAULT 0 NOT NULL,
    expired boolean DEFAULT false NOT NULL,
    status boolean DEFAULT false NOT NULL
);


ALTER TABLE public.ezdfsfile OWNER TO rag;

--
-- Name: ezgmaplocation; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezgmaplocation (
    contentobject_attribute_id integer DEFAULT 0 NOT NULL,
    contentobject_version integer DEFAULT 0 NOT NULL,
    latitude double precision DEFAULT '0'::double precision NOT NULL,
    longitude double precision DEFAULT '0'::double precision NOT NULL,
    address character varying(150) DEFAULT NULL::character varying
);


ALTER TABLE public.ezgmaplocation OWNER TO rag;

--
-- Name: ezimagefile; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezimagefile (
    id integer NOT NULL,
    contentobject_attribute_id integer DEFAULT 0 NOT NULL,
    filepath text NOT NULL
);


ALTER TABLE public.ezimagefile OWNER TO rag;

--
-- Name: ezimagefile_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezimagefile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezimagefile_id_seq OWNER TO rag;

--
-- Name: ezimagefile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezimagefile_id_seq OWNED BY public.ezimagefile.id;


--
-- Name: ezkeyword; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezkeyword (
    id integer NOT NULL,
    class_id integer DEFAULT 0 NOT NULL,
    keyword character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.ezkeyword OWNER TO rag;

--
-- Name: ezkeyword_attribute_link; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezkeyword_attribute_link (
    id integer NOT NULL,
    keyword_id integer DEFAULT 0 NOT NULL,
    objectattribute_id integer DEFAULT 0 NOT NULL,
    version integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezkeyword_attribute_link OWNER TO rag;

--
-- Name: ezkeyword_attribute_link_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezkeyword_attribute_link_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezkeyword_attribute_link_id_seq OWNER TO rag;

--
-- Name: ezkeyword_attribute_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezkeyword_attribute_link_id_seq OWNED BY public.ezkeyword_attribute_link.id;


--
-- Name: ezkeyword_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezkeyword_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezkeyword_id_seq OWNER TO rag;

--
-- Name: ezkeyword_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezkeyword_id_seq OWNED BY public.ezkeyword.id;


--
-- Name: ezmedia; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezmedia (
    contentobject_attribute_id integer DEFAULT 0 NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    controls character varying(50) DEFAULT NULL::character varying,
    filename character varying(255) DEFAULT ''::character varying NOT NULL,
    has_controller integer DEFAULT 0,
    height integer,
    is_autoplay integer DEFAULT 0,
    is_loop integer DEFAULT 0,
    mime_type character varying(50) DEFAULT ''::character varying NOT NULL,
    original_filename character varying(255) DEFAULT ''::character varying NOT NULL,
    pluginspage character varying(255) DEFAULT NULL::character varying,
    quality character varying(50) DEFAULT NULL::character varying,
    width integer
);


ALTER TABLE public.ezmedia OWNER TO rag;

--
-- Name: eznode_assignment; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.eznode_assignment (
    id integer NOT NULL,
    contentobject_id integer,
    contentobject_version integer,
    from_node_id integer DEFAULT 0,
    is_main integer DEFAULT 0 NOT NULL,
    op_code integer DEFAULT 0 NOT NULL,
    parent_node integer,
    parent_remote_id character varying(100) DEFAULT ''::character varying NOT NULL,
    remote_id character varying(100) DEFAULT '0'::character varying NOT NULL,
    sort_field integer DEFAULT 1,
    sort_order integer DEFAULT 1,
    priority integer DEFAULT 0 NOT NULL,
    is_hidden integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.eznode_assignment OWNER TO rag;

--
-- Name: eznode_assignment_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.eznode_assignment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eznode_assignment_id_seq OWNER TO rag;

--
-- Name: eznode_assignment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.eznode_assignment_id_seq OWNED BY public.eznode_assignment.id;


--
-- Name: eznotification; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.eznotification (
    id integer NOT NULL,
    owner_id integer DEFAULT 0 NOT NULL,
    is_pending boolean DEFAULT true NOT NULL,
    type character varying(128) DEFAULT ''::character varying NOT NULL,
    created integer DEFAULT 0 NOT NULL,
    data text
);


ALTER TABLE public.eznotification OWNER TO rag;

--
-- Name: eznotification_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.eznotification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eznotification_id_seq OWNER TO rag;

--
-- Name: eznotification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.eznotification_id_seq OWNED BY public.eznotification.id;


--
-- Name: ezpackage; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezpackage (
    id integer NOT NULL,
    install_date integer DEFAULT 0 NOT NULL,
    name character varying(100) DEFAULT ''::character varying NOT NULL,
    version character varying(30) DEFAULT '0'::character varying NOT NULL
);


ALTER TABLE public.ezpackage OWNER TO rag;

--
-- Name: ezpackage_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezpackage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezpackage_id_seq OWNER TO rag;

--
-- Name: ezpackage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezpackage_id_seq OWNED BY public.ezpackage.id;


--
-- Name: ezpolicy; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezpolicy (
    id integer NOT NULL,
    function_name character varying(255) DEFAULT NULL::character varying,
    module_name character varying(255) DEFAULT NULL::character varying,
    original_id integer DEFAULT 0 NOT NULL,
    role_id integer
);


ALTER TABLE public.ezpolicy OWNER TO rag;

--
-- Name: ezpolicy_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezpolicy_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezpolicy_id_seq OWNER TO rag;

--
-- Name: ezpolicy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezpolicy_id_seq OWNED BY public.ezpolicy.id;


--
-- Name: ezpolicy_limitation; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezpolicy_limitation (
    id integer NOT NULL,
    identifier character varying(255) DEFAULT ''::character varying NOT NULL,
    policy_id integer
);


ALTER TABLE public.ezpolicy_limitation OWNER TO rag;

--
-- Name: ezpolicy_limitation_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezpolicy_limitation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezpolicy_limitation_id_seq OWNER TO rag;

--
-- Name: ezpolicy_limitation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezpolicy_limitation_id_seq OWNED BY public.ezpolicy_limitation.id;


--
-- Name: ezpolicy_limitation_value; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezpolicy_limitation_value (
    id integer NOT NULL,
    limitation_id integer,
    value character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.ezpolicy_limitation_value OWNER TO rag;

--
-- Name: ezpolicy_limitation_value_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezpolicy_limitation_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezpolicy_limitation_value_id_seq OWNER TO rag;

--
-- Name: ezpolicy_limitation_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezpolicy_limitation_value_id_seq OWNED BY public.ezpolicy_limitation_value.id;


--
-- Name: ezpreferences; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezpreferences (
    id integer NOT NULL,
    name character varying(100) DEFAULT NULL::character varying,
    user_id integer DEFAULT 0 NOT NULL,
    value text
);


ALTER TABLE public.ezpreferences OWNER TO rag;

--
-- Name: ezpreferences_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezpreferences_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezpreferences_id_seq OWNER TO rag;

--
-- Name: ezpreferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezpreferences_id_seq OWNED BY public.ezpreferences.id;


--
-- Name: ezrole; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezrole (
    id integer NOT NULL,
    is_new integer DEFAULT 0 NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    value character(1) DEFAULT NULL::bpchar,
    version integer DEFAULT 0
);


ALTER TABLE public.ezrole OWNER TO rag;

--
-- Name: ezrole_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezrole_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezrole_id_seq OWNER TO rag;

--
-- Name: ezrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezrole_id_seq OWNED BY public.ezrole.id;


--
-- Name: ezsearch_object_word_link; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezsearch_object_word_link (
    id integer NOT NULL,
    contentclass_attribute_id integer DEFAULT 0 NOT NULL,
    contentclass_id integer DEFAULT 0 NOT NULL,
    contentobject_id integer DEFAULT 0 NOT NULL,
    frequency double precision DEFAULT '0'::double precision NOT NULL,
    identifier character varying(255) DEFAULT ''::character varying NOT NULL,
    integer_value integer DEFAULT 0 NOT NULL,
    next_word_id integer DEFAULT 0 NOT NULL,
    placement integer DEFAULT 0 NOT NULL,
    prev_word_id integer DEFAULT 0 NOT NULL,
    published integer DEFAULT 0 NOT NULL,
    section_id integer DEFAULT 0 NOT NULL,
    word_id integer DEFAULT 0 NOT NULL,
    language_mask bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezsearch_object_word_link OWNER TO rag;

--
-- Name: ezsearch_object_word_link_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezsearch_object_word_link_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezsearch_object_word_link_id_seq OWNER TO rag;

--
-- Name: ezsearch_object_word_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezsearch_object_word_link_id_seq OWNED BY public.ezsearch_object_word_link.id;


--
-- Name: ezsearch_word; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezsearch_word (
    id integer NOT NULL,
    object_count integer DEFAULT 0 NOT NULL,
    word character varying(150) DEFAULT NULL::character varying
);


ALTER TABLE public.ezsearch_word OWNER TO rag;

--
-- Name: ezsearch_word_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezsearch_word_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezsearch_word_id_seq OWNER TO rag;

--
-- Name: ezsearch_word_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezsearch_word_id_seq OWNED BY public.ezsearch_word.id;


--
-- Name: ezsection; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezsection (
    id integer NOT NULL,
    identifier character varying(255) DEFAULT NULL::character varying,
    locale character varying(255) DEFAULT NULL::character varying,
    name character varying(255) DEFAULT NULL::character varying,
    navigation_part_identifier character varying(100) DEFAULT 'ezcontentnavigationpart'::character varying
);


ALTER TABLE public.ezsection OWNER TO rag;

--
-- Name: ezsection_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezsection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezsection_id_seq OWNER TO rag;

--
-- Name: ezsection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezsection_id_seq OWNED BY public.ezsection.id;


--
-- Name: ezsite_data; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezsite_data (
    name character varying(60) DEFAULT ''::character varying NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.ezsite_data OWNER TO rag;

--
-- Name: ezurl; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezurl (
    id integer NOT NULL,
    created integer DEFAULT 0 NOT NULL,
    is_valid integer DEFAULT 1 NOT NULL,
    last_checked integer DEFAULT 0 NOT NULL,
    modified integer DEFAULT 0 NOT NULL,
    original_url_md5 character varying(32) DEFAULT ''::character varying NOT NULL,
    url text
);


ALTER TABLE public.ezurl OWNER TO rag;

--
-- Name: ezurl_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezurl_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezurl_id_seq OWNER TO rag;

--
-- Name: ezurl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezurl_id_seq OWNED BY public.ezurl.id;


--
-- Name: ezurl_object_link; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezurl_object_link (
    contentobject_attribute_id integer DEFAULT 0 NOT NULL,
    contentobject_attribute_version integer DEFAULT 0 NOT NULL,
    url_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezurl_object_link OWNER TO rag;

--
-- Name: ezurlalias; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezurlalias (
    id integer NOT NULL,
    destination_url text NOT NULL,
    forward_to_id integer DEFAULT 0 NOT NULL,
    is_imported integer DEFAULT 0 NOT NULL,
    is_internal integer DEFAULT 1 NOT NULL,
    is_wildcard integer DEFAULT 0 NOT NULL,
    source_md5 character varying(32) DEFAULT NULL::character varying,
    source_url text NOT NULL
);


ALTER TABLE public.ezurlalias OWNER TO rag;

--
-- Name: ezurlalias_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezurlalias_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezurlalias_id_seq OWNER TO rag;

--
-- Name: ezurlalias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezurlalias_id_seq OWNED BY public.ezurlalias.id;


--
-- Name: ezurlalias_ml; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezurlalias_ml (
    parent integer DEFAULT 0 NOT NULL,
    text_md5 character varying(32) DEFAULT ''::character varying NOT NULL,
    action text NOT NULL,
    action_type character varying(32) DEFAULT ''::character varying NOT NULL,
    alias_redirects integer DEFAULT 1 NOT NULL,
    id integer DEFAULT 0 NOT NULL,
    is_alias integer DEFAULT 0 NOT NULL,
    is_original integer DEFAULT 0 NOT NULL,
    lang_mask bigint DEFAULT 0 NOT NULL,
    link integer DEFAULT 0 NOT NULL,
    text text NOT NULL
);


ALTER TABLE public.ezurlalias_ml OWNER TO rag;

--
-- Name: ezurlalias_ml_incr; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezurlalias_ml_incr (
    id integer NOT NULL
);


ALTER TABLE public.ezurlalias_ml_incr OWNER TO rag;

--
-- Name: ezurlalias_ml_incr_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezurlalias_ml_incr_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezurlalias_ml_incr_id_seq OWNER TO rag;

--
-- Name: ezurlalias_ml_incr_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezurlalias_ml_incr_id_seq OWNED BY public.ezurlalias_ml_incr.id;


--
-- Name: ezurlwildcard; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezurlwildcard (
    id integer NOT NULL,
    destination_url text NOT NULL,
    source_url text NOT NULL,
    type integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezurlwildcard OWNER TO rag;

--
-- Name: ezurlwildcard_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezurlwildcard_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezurlwildcard_id_seq OWNER TO rag;

--
-- Name: ezurlwildcard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezurlwildcard_id_seq OWNED BY public.ezurlwildcard.id;


--
-- Name: ezuser; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezuser (
    contentobject_id integer DEFAULT 0 NOT NULL,
    email character varying(150) DEFAULT ''::character varying NOT NULL,
    login character varying(150) DEFAULT ''::character varying NOT NULL,
    password_hash character varying(255) DEFAULT NULL::character varying,
    password_hash_type integer DEFAULT 1 NOT NULL,
    password_updated_at integer
);


ALTER TABLE public.ezuser OWNER TO rag;

--
-- Name: ezuser_accountkey; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezuser_accountkey (
    id integer NOT NULL,
    hash_key character varying(32) DEFAULT ''::character varying NOT NULL,
    "time" integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ezuser_accountkey OWNER TO rag;

--
-- Name: ezuser_accountkey_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezuser_accountkey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezuser_accountkey_id_seq OWNER TO rag;

--
-- Name: ezuser_accountkey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezuser_accountkey_id_seq OWNED BY public.ezuser_accountkey.id;


--
-- Name: ezuser_role; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezuser_role (
    id integer NOT NULL,
    contentobject_id integer,
    limit_identifier character varying(255) DEFAULT ''::character varying,
    limit_value character varying(255) DEFAULT ''::character varying,
    role_id integer
);


ALTER TABLE public.ezuser_role OWNER TO rag;

--
-- Name: ezuser_role_id_seq; Type: SEQUENCE; Schema: public; Owner: rag
--

CREATE SEQUENCE public.ezuser_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ezuser_role_id_seq OWNER TO rag;

--
-- Name: ezuser_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rag
--

ALTER SEQUENCE public.ezuser_role_id_seq OWNED BY public.ezuser_role.id;


--
-- Name: ezuser_setting; Type: TABLE; Schema: public; Owner: rag
--

CREATE TABLE public.ezuser_setting (
    user_id integer DEFAULT 0 NOT NULL,
    is_enabled integer DEFAULT 0 NOT NULL,
    max_login integer
);


ALTER TABLE public.ezuser_setting OWNER TO rag;

--
-- Name: ezcobj_state id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcobj_state ALTER COLUMN id SET DEFAULT nextval('public.ezcobj_state_id_seq'::regclass);


--
-- Name: ezcobj_state_group id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcobj_state_group ALTER COLUMN id SET DEFAULT nextval('public.ezcobj_state_group_id_seq'::regclass);


--
-- Name: ezcontentbrowsebookmark id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentbrowsebookmark ALTER COLUMN id SET DEFAULT nextval('public.ezcontentbrowsebookmark_id_seq'::regclass);


--
-- Name: ezcontentclass id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass ALTER COLUMN id SET DEFAULT nextval('public.ezcontentclass_id_seq'::regclass);


--
-- Name: ezcontentclass_attribute id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass_attribute ALTER COLUMN id SET DEFAULT nextval('public.ezcontentclass_attribute_id_seq'::regclass);


--
-- Name: ezcontentclassgroup id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclassgroup ALTER COLUMN id SET DEFAULT nextval('public.ezcontentclassgroup_id_seq'::regclass);


--
-- Name: ezcontentobject id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject ALTER COLUMN id SET DEFAULT nextval('public.ezcontentobject_id_seq'::regclass);


--
-- Name: ezcontentobject_attribute id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_attribute ALTER COLUMN id SET DEFAULT nextval('public.ezcontentobject_attribute_id_seq'::regclass);


--
-- Name: ezcontentobject_link id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_link ALTER COLUMN id SET DEFAULT nextval('public.ezcontentobject_link_id_seq'::regclass);


--
-- Name: ezcontentobject_tree node_id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_tree ALTER COLUMN node_id SET DEFAULT nextval('public.ezcontentobject_tree_node_id_seq'::regclass);


--
-- Name: ezcontentobject_version id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_version ALTER COLUMN id SET DEFAULT nextval('public.ezcontentobject_version_id_seq'::regclass);


--
-- Name: ezimagefile id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezimagefile ALTER COLUMN id SET DEFAULT nextval('public.ezimagefile_id_seq'::regclass);


--
-- Name: ezkeyword id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezkeyword ALTER COLUMN id SET DEFAULT nextval('public.ezkeyword_id_seq'::regclass);


--
-- Name: ezkeyword_attribute_link id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezkeyword_attribute_link ALTER COLUMN id SET DEFAULT nextval('public.ezkeyword_attribute_link_id_seq'::regclass);


--
-- Name: eznode_assignment id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.eznode_assignment ALTER COLUMN id SET DEFAULT nextval('public.eznode_assignment_id_seq'::regclass);


--
-- Name: eznotification id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.eznotification ALTER COLUMN id SET DEFAULT nextval('public.eznotification_id_seq'::regclass);


--
-- Name: ezpackage id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpackage ALTER COLUMN id SET DEFAULT nextval('public.ezpackage_id_seq'::regclass);


--
-- Name: ezpolicy id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpolicy ALTER COLUMN id SET DEFAULT nextval('public.ezpolicy_id_seq'::regclass);


--
-- Name: ezpolicy_limitation id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpolicy_limitation ALTER COLUMN id SET DEFAULT nextval('public.ezpolicy_limitation_id_seq'::regclass);


--
-- Name: ezpolicy_limitation_value id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpolicy_limitation_value ALTER COLUMN id SET DEFAULT nextval('public.ezpolicy_limitation_value_id_seq'::regclass);


--
-- Name: ezpreferences id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpreferences ALTER COLUMN id SET DEFAULT nextval('public.ezpreferences_id_seq'::regclass);


--
-- Name: ezrole id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezrole ALTER COLUMN id SET DEFAULT nextval('public.ezrole_id_seq'::regclass);


--
-- Name: ezsearch_object_word_link id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezsearch_object_word_link ALTER COLUMN id SET DEFAULT nextval('public.ezsearch_object_word_link_id_seq'::regclass);


--
-- Name: ezsearch_word id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezsearch_word ALTER COLUMN id SET DEFAULT nextval('public.ezsearch_word_id_seq'::regclass);


--
-- Name: ezsection id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezsection ALTER COLUMN id SET DEFAULT nextval('public.ezsection_id_seq'::regclass);


--
-- Name: ezurl id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurl ALTER COLUMN id SET DEFAULT nextval('public.ezurl_id_seq'::regclass);


--
-- Name: ezurlalias id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurlalias ALTER COLUMN id SET DEFAULT nextval('public.ezurlalias_id_seq'::regclass);


--
-- Name: ezurlalias_ml_incr id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurlalias_ml_incr ALTER COLUMN id SET DEFAULT nextval('public.ezurlalias_ml_incr_id_seq'::regclass);


--
-- Name: ezurlwildcard id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurlwildcard ALTER COLUMN id SET DEFAULT nextval('public.ezurlwildcard_id_seq'::regclass);


--
-- Name: ezuser_accountkey id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezuser_accountkey ALTER COLUMN id SET DEFAULT nextval('public.ezuser_accountkey_id_seq'::regclass);


--
-- Name: ezuser_role id; Type: DEFAULT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezuser_role ALTER COLUMN id SET DEFAULT nextval('public.ezuser_role_id_seq'::regclass);


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
\.


--
-- Data for Name: ezbinaryfile; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezbinaryfile (contentobject_attribute_id, version, download_count, filename, mime_type, original_filename) FROM stdin;
\.


--
-- Data for Name: ezcobj_state; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcobj_state (id, default_language_id, group_id, identifier, language_mask, priority) FROM stdin;
1	2	2	not_locked	3	0
2	2	2	locked	3	1
\.


--
-- Data for Name: ezcobj_state_group; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcobj_state_group (id, default_language_id, identifier, language_mask) FROM stdin;
2	2	ez_lock	3
\.


--
-- Data for Name: ezcobj_state_group_language; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcobj_state_group_language (contentobject_state_group_id, real_language_id, description, language_id, name) FROM stdin;
2	2		3	Lock
\.


--
-- Data for Name: ezcobj_state_language; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcobj_state_language (contentobject_state_id, language_id, description, name) FROM stdin;
1	3		Not locked
2	3		Locked
\.


--
-- Data for Name: ezcobj_state_link; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcobj_state_link (contentobject_id, contentobject_state_id) FROM stdin;
1	1
4	1
10	1
11	1
12	1
13	1
14	1
41	1
42	1
49	1
50	1
51	1
\.


--
-- Data for Name: ezcontent_language; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontent_language (id, disabled, locale, name) FROM stdin;
2	0	eng-GB	English (United Kingdom)
\.


--
-- Data for Name: ezcontentbrowsebookmark; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentbrowsebookmark (id, node_id, user_id, name) FROM stdin;
\.


--
-- Data for Name: ezcontentclass; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentclass (id, version, always_available, contentobject_name, created, creator_id, identifier, initial_language_id, is_container, language_mask, modified, modifier_id, remote_id, serialized_description_list, serialized_name_list, sort_field, sort_order, url_alias_name) FROM stdin;
1	0	1	<short_name|name>	1024392098	14	folder	2	1	2	1448831672	14	a3d405b81be900468eb153d774f4f0d2	a:0:{}	a:1:{s:6:"eng-GB";s:6:"Folder";}	1	1	\N
2	0	0	<short_title|title>	1024392098	14	article	2	1	3	1082454989	14	c15b600eb9198b1924063b5a68758232	\N	a:2:{s:6:"eng-GB";s:7:"Article";s:16:"always-available";s:6:"eng-GB";}	1	1	\N
3	0	1	<name>	1024392098	14	user_group	2	1	3	1048494743	14	25b4268cdcd01921b808a0d854b877ef	\N	a:2:{s:6:"eng-GB";s:10:"User group";s:16:"always-available";s:6:"eng-GB";}	1	1	\N
4	0	1	<first_name> <last_name>	1024392098	14	user	2	0	3	1082018364	14	40faa822edc579b02c25f6bb7beec3ad	\N	a:2:{s:6:"eng-GB";s:4:"User";s:16:"always-available";s:6:"eng-GB";}	1	1	\N
5	0	1	<name>	1031484992	14	image	2	0	3	1048494784	14	f6df12aa74e36230eb675f364fccd25a	\N	a:2:{s:6:"eng-GB";s:5:"Image";s:16:"always-available";s:6:"eng-GB";}	1	1	\N
12	0	1	<name>	1052385472	14	file	2	0	3	1052385669	14	637d58bfddf164627bdfd265733280a0	\N	a:2:{s:6:"eng-GB";s:4:"File";s:16:"always-available";s:6:"eng-GB";}	1	1	\N
\.


--
-- Data for Name: ezcontentclass_attribute; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentclass_attribute (id, version, can_translate, category, contentclass_id, data_float1, data_float2, data_float3, data_float4, data_int1, data_int2, data_int3, data_int4, data_text1, data_text2, data_text3, data_text4, data_text5, data_type_string, identifier, is_information_collector, is_required, is_searchable, is_thumbnail, placement, serialized_data_text, serialized_description_list, serialized_name_list) FROM stdin;
1	0	1		2	0	0	0	0	255	0	0	0	New article					ezstring	title	0	1	1	f	1	\N	\N	a:2:{s:6:"eng-GB";s:5:"Title";s:16:"always-available";s:6:"eng-GB";}
4	0	1		1	\N	\N	\N	\N	255	0	\N	\N	Folder	\N	\N	\N	\N	ezstring	name	0	1	1	f	1	N;	a:0:{}	a:1:{s:6:"eng-GB";s:4:"Name";}
6	0	1		3	0	0	0	0	255	0	0	0					\N	ezstring	name	0	1	1	f	1	\N	\N	a:2:{s:6:"eng-GB";s:4:"Name";s:16:"always-available";s:6:"eng-GB";}
7	0	1		3	0	0	0	0	255	0	0	0					\N	ezstring	description	0	0	1	f	2	\N	\N	a:2:{s:6:"eng-GB";s:11:"Description";s:16:"always-available";s:6:"eng-GB";}
8	0	1		4	0	0	0	0	255	0	0	0						ezstring	first_name	0	1	1	f	1	\N	\N	a:2:{s:6:"eng-GB";s:10:"First name";s:16:"always-available";s:6:"eng-GB";}
9	0	1		4	0	0	0	0	255	0	0	0						ezstring	last_name	0	1	1	f	2	\N	\N	a:2:{s:6:"eng-GB";s:9:"Last name";s:16:"always-available";s:6:"eng-GB";}
12	0	0		4	0	0	0	0	7	10	0	0		^[^@]+$				ezuser	user_account	0	1	0	f	3	\N	\N	a:2:{s:6:"eng-GB";s:12:"User account";s:16:"always-available";s:6:"eng-GB";}
116	0	1		5	0	0	0	0	150	0	0	0					\N	ezstring	name	0	1	1	f	1	\N	\N	a:2:{s:6:"eng-GB";s:4:"Name";s:16:"always-available";s:6:"eng-GB";}
117	0	1		5	0	0	0	0	10	0	0	0					\N	ezrichtext	caption	0	0	1	f	2	\N	\N	a:2:{s:6:"eng-GB";s:7:"Caption";s:16:"always-available";s:6:"eng-GB";}
118	0	1		5	0	0	0	0	10	0	0	0					\N	ezimage	image	0	0	0	f	3	\N	\N	a:2:{s:6:"eng-GB";s:5:"Image";s:16:"always-available";s:6:"eng-GB";}
119	0	1		1	\N	\N	\N	\N	5	\N	\N	\N	\N	\N	\N	\N	\N	ezrichtext	short_description	0	0	1	f	3	N;	a:0:{}	a:1:{s:6:"eng-GB";s:17:"Short description";}
120	0	1		2	0	0	0	0	10	0	0	0						ezrichtext	intro	0	1	1	f	4	\N	\N	a:2:{s:6:"eng-GB";s:5:"Intro";s:16:"always-available";s:6:"eng-GB";}
121	0	1		2	0	0	0	0	20	0	0	0						ezrichtext	body	0	0	1	f	5	\N	\N	a:2:{s:6:"eng-GB";s:4:"Body";s:16:"always-available";s:6:"eng-GB";}
123	0	0		2	0	0	0	0	0	0	0	0						ezboolean	enable_comments	0	0	0	f	6	\N	\N	a:2:{s:6:"eng-GB";s:15:"Enable comments";s:16:"always-available";s:6:"eng-GB";}
146	0	1		12	0	0	0	0	0	0	0	0	New file				\N	ezstring	name	0	1	1	f	1	\N	\N	a:2:{s:6:"eng-GB";s:4:"Name";s:16:"always-available";s:6:"eng-GB";}
147	0	1		12	0	0	0	0	10	0	0	0					\N	ezrichtext	description	0	0	1	f	2	\N	\N	a:2:{s:6:"eng-GB";s:11:"Description";s:16:"always-available";s:6:"eng-GB";}
148	0	1		12	0	0	0	0	0	0	0	0					\N	ezbinaryfile	file	0	1	0	f	3	\N	\N	a:2:{s:6:"eng-GB";s:4:"File";s:16:"always-available";s:6:"eng-GB";}
152	0	1		2	0	0	0	0	255	0	0	0						ezstring	short_title	0	0	1	f	2	\N	\N	a:2:{s:6:"eng-GB";s:11:"Short title";s:16:"always-available";s:6:"eng-GB";}
153	0	1		2	0	0	0	0	1	0	0	0						ezauthor	author	0	0	0	f	3	\N	\N	a:2:{s:6:"eng-GB";s:6:"Author";s:16:"always-available";s:6:"eng-GB";}
154	0	1		2	0	0	0	0	0	0	0	0						ezobjectrelation	image	0	0	1	f	7	\N	\N	a:2:{s:6:"eng-GB";s:5:"Image";s:16:"always-available";s:6:"eng-GB";}
155	0	1		1	\N	\N	\N	\N	100	0	\N	\N	\N	\N	\N	\N	\N	ezstring	short_name	0	0	1	f	2	N;	a:0:{}	a:1:{s:6:"eng-GB";s:10:"Short name";}
156	0	1		1	\N	\N	\N	\N	20	\N	\N	\N	\N	\N	\N	\N	\N	ezrichtext	description	0	0	1	f	4	N;	a:0:{}	a:1:{s:6:"eng-GB";s:11:"Description";}
179	0	1		4	0	0	0	0	10	0	0	0						eztext	signature	0	0	1	f	4	\N	\N	a:2:{s:6:"eng-GB";s:9:"Signature";s:16:"always-available";s:6:"eng-GB";}
180	0	1		4	0	0	0	0	10	0	0	0						ezimage	image	0	0	0	f	5	\N	\N	a:2:{s:6:"eng-GB";s:5:"Image";s:16:"always-available";s:6:"eng-GB";}
\.


--
-- Data for Name: ezcontentclass_attribute_ml; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentclass_attribute_ml (contentclass_attribute_id, version, language_id, name, description, data_text, data_json) FROM stdin;
\.


--
-- Data for Name: ezcontentclass_classgroup; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentclass_classgroup (contentclass_id, contentclass_version, group_id, group_name) FROM stdin;
1	0	1	Content
2	0	1	Content
3	0	2	Users
4	0	2	Users
5	0	3	Media
12	0	3	Media
\.


--
-- Data for Name: ezcontentclass_name; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentclass_name (contentclass_id, contentclass_version, language_id, language_locale, name) FROM stdin;
1	0	2	eng-GB	Folder
2	0	3	eng-GB	Article
3	0	3	eng-GB	User group
4	0	3	eng-GB	User
5	0	3	eng-GB	Image
12	0	3	eng-GB	File
\.


--
-- Data for Name: ezcontentclassgroup; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentclassgroup (id, created, creator_id, modified, modifier_id, name) FROM stdin;
1	1031216928	14	1033922106	14	Content
2	1031216941	14	1033922113	14	Users
3	1032009743	14	1033922120	14	Media
\.


--
-- Data for Name: ezcontentobject; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentobject (id, contentclass_id, current_version, initial_language_id, language_mask, modified, name, owner_id, published, remote_id, section_id, status, is_hidden) FROM stdin;
1	1	9	2	3	1448889046	eZ Platform	14	1448889046	9459d3c29e15006e45197295722c7ade	1	1	f
4	3	1	2	3	1033917596	Users	14	1033917596	f5c88a2209584891056f987fd965b0ba	2	1	f
10	4	2	2	3	1072180405	Anonymous User	14	1033920665	faaeb9be3bd98ed09f606fc16d144eca	2	1	f
11	3	1	2	3	1033920746	Guest accounts	14	1033920746	5f7f0bdb3381d6a461d8c29ff53d908f	2	1	f
12	3	1	2	3	1033920775	Administrator users	14	1033920775	9b47a45624b023b1a76c73b74d704acf	2	1	f
13	3	1	2	3	1033920794	Editors	14	1033920794	3c160cca19fb135f83bd02d911f04db2	2	1	f
14	4	3	2	3	1301062024	Administrator User	14	1033920830	1bb4fe25487f05527efa8bfd394cecc7	2	1	f
41	1	1	2	3	1060695457	Media	14	1060695457	a6e35cbcb7cd6ae4b691f3eee30cd262	3	1	f
42	3	1	2	3	1072180330	Anonymous Users	14	1072180330	15b256dbea2ae72418ff5facc999e8f9	2	1	f
49	1	1	2	3	1080220197	Images	14	1080220197	e7ff633c6b8e0fd3531e74c6e712bead	3	1	f
50	1	1	2	3	1080220220	Files	14	1080220220	732a5acd01b51a6fe6eab448ad4138a9	3	1	f
51	1	1	2	3	1080220233	Multimedia	14	1080220233	09082deb98662a104f325aaa8c4933d3	3	1	f
\.


--
-- Data for Name: ezcontentobject_attribute; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentobject_attribute (id, version, attribute_original_id, contentclassattribute_id, contentobject_id, data_float, data_int, data_text, data_type_string, language_code, language_id, sort_key_int, sort_key_string) FROM stdin;
1	9	0	4	1	\N	\N	Welcome to eZ Platform	ezstring	eng-GB	3	0	welcome to ez platform
2	9	0	119	1	\N	\N	<?xml version="1.0" encoding="UTF-8"?><section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"><para>You are now ready to start your project.</para></section>	ezrichtext	eng-GB	3	0	
7	1	0	7	4	\N	\N	Main group	ezstring	eng-GB	3	0	
8	1	0	6	4	\N	\N	Users	ezstring	eng-GB	3	0	
19	2	0	8	10	0	0	Anonymous	ezstring	eng-GB	3	0	anonymous
20	2	0	9	10	0	0	User	ezstring	eng-GB	3	0	user
21	2	0	12	10	0	0		ezuser	eng-GB	3	0	
22	1	0	6	11	0	0	Guest accounts	ezstring	eng-GB	3	0	
23	1	0	7	11	0	0		ezstring	eng-GB	3	0	
24	1	0	6	12	0	0	Administrator users	ezstring	eng-GB	3	0	
25	1	0	7	12	0	0		ezstring	eng-GB	3	0	
26	1	0	6	13	0	0	Editors	ezstring	eng-GB	3	0	
27	1	0	7	13	0	0		ezstring	eng-GB	3	0	
28	3	0	8	14	0	0	Administrator	ezstring	eng-GB	3	0	administrator
29	3	0	9	14	0	0	User	ezstring	eng-GB	3	0	user
30	3	30	12	14	0	0		ezuser	eng-GB	3	0	
98	1	0	4	41	0	0	Media	ezstring	eng-GB	3	0	
99	1	0	119	41	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
100	1	0	6	42	0	0	Anonymous Users	ezstring	eng-GB	3	0	anonymous users
101	1	0	7	42	0	0	User group for the anonymous user	ezstring	eng-GB	3	0	user group for the anonymous user
102	9	0	155	1	\N	\N	eZ Platform	ezstring	eng-GB	3	0	ez platform
103	1	0	155	41	0	0		ezstring	eng-GB	3	0	
104	9	0	156	1	\N	\N	<?xml version="1.0" encoding="UTF-8"?><section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"><para>This is the clean installation coming with eZ Platform. It's a bare-bones setup of the Platform, an excellent foundation to build upon if you want to start your project from scratch.</para></section>	ezrichtext	eng-GB	3	0	
105	1	0	156	41	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
142	1	0	4	49	0	0	Images	ezstring	eng-GB	3	0	images
143	1	0	155	49	0	0		ezstring	eng-GB	3	0	
144	1	0	119	49	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
145	1	0	156	49	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
147	1	0	4	50	0	0	Files	ezstring	eng-GB	3	0	files
148	1	0	155	50	0	0		ezstring	eng-GB	3	0	
149	1	0	119	50	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
150	1	0	156	50	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
152	1	0	4	51	0	0	Multimedia	ezstring	eng-GB	3	0	multimedia
153	1	0	155	51	0	0		ezstring	eng-GB	3	0	
154	1	0	119	51	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
155	1	0	156	51	0	1045487555	<?xml version="1.0" encoding="UTF-8"?>\n<section xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ezxhtml="http://ez.no/xmlns/ezpublish/docbook/xhtml" xmlns:ezcustom="http://ez.no/xmlns/ezpublish/docbook/custom" version="5.0-variant ezpublish-1.0"/>\n	ezrichtext	eng-GB	3	0	
177	2	0	179	10	0	0		eztext	eng-GB	3	0	
178	3	0	179	14	0	0		eztext	eng-GB	3	0	
179	2	0	180	10	0	0		ezimage	eng-GB	3	0	
180	3	0	180	14	0	0	<?xml version="1.0" encoding="utf-8"?>\n<ezimage serial_number="1" is_valid="" filename="" suffix="" basename="" dirpath="" url="" original_filename="" mime_type="" width="" height="" alternative_text="" alias_key="1293033771" timestamp="1301057722"><original attribute_id="180" attribute_version="3" attribute_language="eng-GB"/></ezimage>\n	ezimage	eng-GB	3	0	
\.


--
-- Data for Name: ezcontentobject_link; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentobject_link (id, contentclassattribute_id, from_contentobject_id, from_contentobject_version, relation_type, to_contentobject_id) FROM stdin;
\.


--
-- Data for Name: ezcontentobject_name; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentobject_name (contentobject_id, content_version, content_translation, language_id, name, real_translation) FROM stdin;
1	9	eng-GB	2	eZ Platform	eng-GB
4	1	eng-GB	3	Users	eng-GB
10	2	eng-GB	3	Anonymous User	eng-GB
11	1	eng-GB	3	Guest accounts	eng-GB
12	1	eng-GB	3	Administrator users	eng-GB
13	1	eng-GB	3	Editors	eng-GB
14	3	eng-GB	3	Administrator User	eng-GB
41	1	eng-GB	3	Media	eng-GB
42	1	eng-GB	3	Anonymous Users	eng-GB
49	1	eng-GB	3	Images	eng-GB
50	1	eng-GB	3	Files	eng-GB
51	1	eng-GB	3	Multimedia	eng-GB
\.


--
-- Data for Name: ezcontentobject_trash; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentobject_trash (node_id, contentobject_id, contentobject_version, depth, is_hidden, is_invisible, main_node_id, modified_subnode, parent_node_id, path_identification_string, path_string, priority, remote_id, sort_field, sort_order, trashed) FROM stdin;
\.


--
-- Data for Name: ezcontentobject_tree; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentobject_tree (node_id, contentobject_id, contentobject_is_published, contentobject_version, depth, is_hidden, is_invisible, main_node_id, modified_subnode, parent_node_id, path_identification_string, path_string, priority, remote_id, sort_field, sort_order) FROM stdin;
1	0	1	1	0	0	0	1	1448999778	1		/1/	0	629709ba256fe317c3ddcee35453a96a	1	1
2	1	1	9	1	0	0	2	1301073466	1	node_2	/1/2/	0	f3e90596361e31d496d4026eb624c983	8	1
5	4	1	1	1	0	0	5	1301062024	1	users	/1/5/	0	3f6d92f8044aed134f32153517850f5a	1	1
12	11	1	1	2	0	0	12	1081860719	5	users/guest_accounts	/1/5/12/	0	602dcf84765e56b7f999eaafd3821dd3	1	1
13	12	1	1	2	0	0	13	1301062024	5	users/administrator_users	/1/5/13/	0	769380b7aa94541679167eab817ca893	1	1
14	13	1	1	2	0	0	14	1081860719	5	users/editors	/1/5/14/	0	f7dda2854fc68f7c8455d9cb14bd04a9	1	1
15	14	1	3	3	0	0	15	1301062024	13	users/administrator_users/administrator_user	/1/5/13/15/	0	e5161a99f733200b9ed4e80f9c16187b	1	1
43	41	1	1	1	0	0	43	1081860720	1	media	/1/43/	0	75c715a51699d2d309a924eca6a95145	9	1
44	42	1	1	2	0	0	44	1081860719	5	users/anonymous_users	/1/5/44/	0	4fdf0072da953bb276c0c7e0141c5c9b	9	1
45	10	1	2	3	0	0	45	1081860719	44	users/anonymous_users/anonymous_user	/1/5/44/45/	0	2cf8343bee7b482bab82b269d8fecd76	9	1
51	49	1	1	2	0	0	51	1081860720	43	media/images	/1/43/51/	0	1b26c0454b09bb49dfb1b9190ffd67cb	9	1
52	50	1	1	2	0	0	52	1081860720	43	media/files	/1/43/52/	0	0b113a208f7890f9ad3c24444ff5988c	9	1
53	51	1	1	2	0	0	53	1081860720	43	media/multimedia	/1/43/53/	0	4f18b82c75f10aad476cae5adf98c11f	9	1
\.


--
-- Data for Name: ezcontentobject_version; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezcontentobject_version (id, contentobject_id, created, creator_id, initial_language_id, language_mask, modified, status, user_id, version, workflow_event_pos) FROM stdin;
4	4	0	14	2	3	0	1	0	1	1
439	11	1033920737	14	2	3	1033920746	1	0	1	0
440	12	1033920760	14	2	3	1033920775	1	0	1	0
441	13	1033920786	14	2	3	1033920794	1	0	1	0
472	41	1060695450	14	2	3	1060695457	1	0	1	0
473	42	1072180278	14	2	3	1072180330	1	0	1	0
474	10	1072180337	14	2	3	1072180405	1	0	2	0
488	49	1080220181	14	2	3	1080220197	1	0	1	0
489	50	1080220211	14	2	3	1080220220	1	0	1	0
490	51	1080220225	14	2	3	1080220233	1	0	1	0
499	14	1301061783	14	2	3	1301062024	1	0	3	0
506	1	1448889045	14	2	3	1448889046	1	0	9	0
\.


--
-- Data for Name: ezdfsfile; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezdfsfile (name_hash, name, name_trunk, datatype, scope, size, mtime, expired, status) FROM stdin;
\.


--
-- Data for Name: ezgmaplocation; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezgmaplocation (contentobject_attribute_id, contentobject_version, latitude, longitude, address) FROM stdin;
\.


--
-- Data for Name: ezimagefile; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezimagefile (id, contentobject_attribute_id, filepath) FROM stdin;
\.


--
-- Data for Name: ezkeyword; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezkeyword (id, class_id, keyword) FROM stdin;
\.


--
-- Data for Name: ezkeyword_attribute_link; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezkeyword_attribute_link (id, keyword_id, objectattribute_id, version) FROM stdin;
\.


--
-- Data for Name: ezmedia; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezmedia (contentobject_attribute_id, version, controls, filename, has_controller, height, is_autoplay, is_loop, mime_type, original_filename, pluginspage, quality, width) FROM stdin;
\.


--
-- Data for Name: eznode_assignment; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.eznode_assignment (id, contentobject_id, contentobject_version, from_node_id, is_main, op_code, parent_node, parent_remote_id, remote_id, sort_field, sort_order, priority, is_hidden) FROM stdin;
4	8	2	0	1	2	5		0	1	1	0	0
5	42	1	0	1	2	5		0	9	1	0	0
6	10	2	-1	1	2	44		0	9	1	0	0
7	4	1	0	1	2	1		0	1	1	0	0
8	12	1	0	1	2	5		0	1	1	0	0
9	13	1	0	1	2	5		0	1	1	0	0
11	41	1	0	1	2	1		0	1	1	0	0
12	11	1	0	1	2	5		0	1	1	0	0
27	49	1	0	1	2	43		0	9	1	0	0
28	50	1	0	1	2	43		0	9	1	0	0
29	51	1	0	1	2	43		0	9	1	0	0
38	14	3	-1	1	2	13		0	1	1	0	0
\.


--
-- Data for Name: eznotification; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.eznotification (id, owner_id, is_pending, type, created, data) FROM stdin;
\.


--
-- Data for Name: ezpackage; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezpackage (id, install_date, name, version) FROM stdin;
\.


--
-- Data for Name: ezpolicy; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezpolicy (id, function_name, module_name, original_id, role_id) FROM stdin;
317	*	content	0	3
319	login	user	0	3
328	read	content	0	1
331	login	user	0	1
332	*	*	0	2
333	read	content	0	4
334	view_embed	content	0	1
340	*	url	0	3
\.


--
-- Data for Name: ezpolicy_limitation; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezpolicy_limitation (id, identifier, policy_id) FROM stdin;
251	Section	328
252	Section	329
253	SiteAccess	331
254	Class	333
255	Owner	333
256	Class	334
\.


--
-- Data for Name: ezpolicy_limitation_value; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezpolicy_limitation_value (id, limitation_id, value) FROM stdin;
477	251	1
478	252	1
479	253	1766001124
480	254	4
481	255	1
482	256	5
483	256	12
\.


--
-- Data for Name: ezpreferences; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezpreferences (id, name, user_id, value) FROM stdin;
\.


--
-- Data for Name: ezrole; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezrole (id, is_new, name, value, version) FROM stdin;
1	0	Anonymous	 	0
2	0	Administrator	0	0
3	0	Editor	 	0
4	0	Member	 	0
\.


--
-- Data for Name: ezsearch_object_word_link; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezsearch_object_word_link (id, contentclass_attribute_id, contentclass_id, contentobject_id, frequency, identifier, integer_value, next_word_id, placement, prev_word_id, published, section_id, word_id, language_mask) FROM stdin;
2	4	1	1	0	name	0	3	0	0	1448889046	1	2	3
3	4	1	1	0	name	0	4	1	2	1448889046	1	3	3
4	4	1	1	0	name	0	5	2	3	1448889046	1	4	3
5	4	1	1	0	name	0	6	3	4	1448889046	1	5	3
6	119	1	1	0	short_description	0	7	4	5	1448889046	1	6	3
7	119	1	1	0	short_description	0	8	5	6	1448889046	1	7	3
8	119	1	1	0	short_description	0	9	6	7	1448889046	1	8	3
9	119	1	1	0	short_description	0	3	7	8	1448889046	1	9	3
10	119	1	1	0	short_description	0	10	8	9	1448889046	1	3	3
11	119	1	1	0	short_description	0	11	9	3	1448889046	1	10	3
12	119	1	1	0	short_description	0	12	10	10	1448889046	1	11	3
13	119	1	1	0	short_description	0	4	11	11	1448889046	1	12	3
14	155	1	1	0	short_name	0	5	12	12	1448889046	1	4	3
15	155	1	1	0	short_name	0	13	13	4	1448889046	1	5	3
16	156	1	1	0	description	0	14	14	5	1448889046	1	13	3
17	156	1	1	0	description	0	15	15	13	1448889046	1	14	3
18	156	1	1	0	description	0	16	16	14	1448889046	1	15	3
19	156	1	1	0	description	0	17	17	15	1448889046	1	16	3
20	156	1	1	0	description	0	18	18	16	1448889046	1	17	3
21	156	1	1	0	description	0	19	19	17	1448889046	1	18	3
22	156	1	1	0	description	0	4	20	18	1448889046	1	19	3
23	156	1	1	0	description	0	5	21	19	1448889046	1	4	3
24	156	1	1	0	description	0	20	22	4	1448889046	1	5	3
25	156	1	1	0	description	0	21	23	5	1448889046	1	20	3
26	156	1	1	0	description	0	22	24	20	1448889046	1	21	3
27	156	1	1	0	description	0	23	25	21	1448889046	1	22	3
28	156	1	1	0	description	0	24	26	22	1448889046	1	23	3
29	156	1	1	0	description	0	25	27	23	1448889046	1	24	3
30	156	1	1	0	description	0	26	28	24	1448889046	1	25	3
31	156	1	1	0	description	0	15	29	25	1448889046	1	26	3
32	156	1	1	0	description	0	5	30	26	1448889046	1	15	3
33	156	1	1	0	description	0	27	31	15	1448889046	1	5	3
34	156	1	1	0	description	0	28	32	5	1448889046	1	27	3
35	156	1	1	0	description	0	29	33	27	1448889046	1	28	3
36	156	1	1	0	description	0	3	34	28	1448889046	1	29	3
37	156	1	1	0	description	0	30	35	29	1448889046	1	3	3
38	156	1	1	0	description	0	31	36	3	1448889046	1	30	3
39	156	1	1	0	description	0	32	37	30	1448889046	1	31	3
40	156	1	1	0	description	0	6	38	31	1448889046	1	32	3
41	156	1	1	0	description	0	33	39	32	1448889046	1	6	3
42	156	1	1	0	description	0	3	40	6	1448889046	1	33	3
43	156	1	1	0	description	0	10	41	33	1448889046	1	3	3
44	156	1	1	0	description	0	11	42	3	1448889046	1	10	3
45	156	1	1	0	description	0	12	43	10	1448889046	1	11	3
46	156	1	1	0	description	0	34	44	11	1448889046	1	12	3
47	156	1	1	0	description	0	35	45	12	1448889046	1	34	3
48	156	1	1	0	description	0	0	46	34	1448889046	1	35	3
49	7	3	4	0	description	0	37	0	0	1033917596	2	36	3
50	7	3	4	0	description	0	38	1	36	1033917596	2	37	3
51	6	3	4	0	name	0	0	2	37	1033917596	2	38	3
52	8	4	10	0	first_name	0	40	0	0	1033920665	2	39	3
53	9	4	10	0	last_name	0	0	1	39	1033920665	2	40	3
54	6	3	11	0	name	0	42	0	0	1033920746	2	41	3
55	6	3	11	0	name	0	0	1	41	1033920746	2	42	3
56	6	3	12	0	name	0	38	0	0	1033920775	2	43	3
57	6	3	12	0	name	0	0	1	43	1033920775	2	38	3
58	6	3	13	0	name	0	0	0	0	1033920794	2	44	3
59	8	4	14	0	first_name	0	40	0	0	1033920830	2	43	3
60	9	4	14	0	last_name	0	0	1	43	1033920830	2	40	3
61	4	1	41	0	name	0	0	0	0	1060695457	3	45	3
62	6	3	42	0	name	0	38	0	0	1072180330	2	39	3
63	6	3	42	0	name	0	40	1	39	1072180330	2	38	3
64	7	3	42	0	description	0	37	2	38	1072180330	2	40	3
65	7	3	42	0	description	0	46	3	40	1072180330	2	37	3
66	7	3	42	0	description	0	15	4	37	1072180330	2	46	3
67	7	3	42	0	description	0	39	5	46	1072180330	2	15	3
68	7	3	42	0	description	0	40	6	15	1072180330	2	39	3
69	7	3	42	0	description	0	0	7	39	1072180330	2	40	3
70	4	1	49	0	name	0	0	0	0	1080220197	3	47	3
71	4	1	50	0	name	0	0	0	0	1080220220	3	48	3
72	4	1	51	0	name	0	0	0	0	1080220233	3	49	3
\.


--
-- Data for Name: ezsearch_word; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezsearch_word (id, object_count, word) FROM stdin;
2	1	welcome
3	1	to
4	1	ez
5	1	platform
6	1	you
7	1	are
8	1	now
9	1	ready
10	1	start
11	1	your
12	1	project
13	1	this
14	1	is
16	1	clean
17	1	installation
18	1	coming
19	1	with
20	1	it
21	1	s
22	1	a
23	1	bare
24	1	bones
25	1	setup
26	1	of
27	1	an
28	1	excellent
29	1	foundation
30	1	build
31	1	upon
32	1	if
33	1	want
34	1	from
35	1	scratch
36	1	main
41	1	guest
42	1	accounts
44	1	editors
43	2	administrator
45	1	media
15	2	the
37	2	group
39	2	anonymous
38	3	users
40	3	user
46	1	for
47	1	images
48	1	files
49	1	multimedia
\.


--
-- Data for Name: ezsection; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezsection (id, identifier, locale, name, navigation_part_identifier) FROM stdin;
1	standard		Standard	ezcontentnavigationpart
2	users		Users	ezusernavigationpart
3	media		Media	ezmedianavigationpart
\.


--
-- Data for Name: ezsite_data; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezsite_data (name, value) FROM stdin;
ezplatform-release	3.0.0
\.


--
-- Data for Name: ezurl; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezurl (id, created, is_valid, last_checked, modified, original_url_md5, url) FROM stdin;
\.


--
-- Data for Name: ezurl_object_link; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezurl_object_link (contentobject_attribute_id, contentobject_attribute_version, url_id) FROM stdin;
\.


--
-- Data for Name: ezurlalias; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezurlalias (id, destination_url, forward_to_id, is_imported, is_internal, is_wildcard, source_md5, source_url) FROM stdin;
12	content/view/full/2	0	1	1	0	d41d8cd98f00b204e9800998ecf8427e	
13	content/view/full/5	0	1	1	0	9bc65c2abec141778ffaa729489f3e87	users
15	content/view/full/12	0	1	1	0	02d4e844e3a660857a3f81585995ffe1	users/guest_accounts
16	content/view/full/13	0	1	1	0	1b1d79c16700fd6003ea7be233e754ba	users/administrator_users
17	content/view/full/14	0	1	1	0	0bb9dd665c96bbc1cf36b79180786dea	users/editors
18	content/view/full/15	0	1	1	0	f1305ac5f327a19b451d82719e0c3f5d	users/administrator_users/administrator_user
20	content/view/full/43	0	1	1	0	62933a2951ef01f4eafd9bdf4d3cd2f0	media
21	content/view/full/44	0	1	1	0	3ae1aac958e1c82013689d917d34967a	users/anonymous_users
22	content/view/full/45	0	1	1	0	aad93975f09371695ba08292fd9698db	users/anonymous_users/anonymous_user
28	content/view/full/51	0	1	1	0	38985339d4a5aadfc41ab292b4527046	media/images
29	content/view/full/52	0	1	1	0	ad5a8c6f6aac3b1b9df267fe22e7aef6	media/files
30	content/view/full/53	0	1	1	0	562a0ac498571c6c3529173184a2657c	media/multimedia
\.


--
-- Data for Name: ezurlalias_ml; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezurlalias_ml (parent, text_md5, action, action_type, alias_redirects, id, is_alias, is_original, lang_mask, link, text) FROM stdin;
0	50e2736330de124f6edea9b008556fe6	nop:	nop	1	17	0	0	1	17	media2
0	62933a2951ef01f4eafd9bdf4d3cd2f0	eznode:43	eznode	1	9	0	1	3	9	Media
0	86425c35a33507d479f71ade53a669aa	nop:	nop	1	3	0	0	1	3	users2
0	9bc65c2abec141778ffaa729489f3e87	eznode:5	eznode	1	2	0	1	3	2	Users
0	d41d8cd98f00b204e9800998ecf8427e	eznode:2	eznode	1	1	0	1	3	1	
2	a147e136bfa717592f2bd70bd4b53b17	eznode:14	eznode	1	6	0	1	3	6	Editors
2	c2803c3fa1b0b5423237b4e018cae755	eznode:44	eznode	1	10	0	1	3	10	Anonymous-Users
2	e57843d836e3af8ab611fde9e2139b3a	eznode:12	eznode	1	4	0	1	3	4	Guest-accounts
2	f89fad7f8a3abc8c09e1deb46a420007	eznode:13	eznode	1	5	0	1	3	5	Administrator-users
3	505e93077a6dde9034ad97a14ab022b1	nop:	nop	1	11	0	0	1	11	anonymous_users2
3	70bb992820e73638731aa8de79b3329e	eznode:12	eznode	1	26	0	0	1	4	guest_accounts
3	a147e136bfa717592f2bd70bd4b53b17	eznode:14	eznode	1	29	0	0	1	6	editors
3	a7da338c20bf65f9f789c87296379c2a	nop:	nop	1	7	0	0	1	7	administrator_users2
3	aeb8609aa933b0899aa012c71139c58c	eznode:13	eznode	1	27	0	0	1	5	administrator_users
3	e9e5ad0c05ee1a43715572e5cc545926	eznode:44	eznode	1	30	0	0	1	10	anonymous_users
5	5a9d7b0ec93173ef4fedee023209cb61	eznode:15	eznode	1	8	0	1	3	8	Administrator-User
7	a3cca2de936df1e2f805710399989971	eznode:15	eznode	1	28	0	0	0	8	administrator_user
9	2e5bc8831f7ae6a29530e7f1bbf2de9c	eznode:53	eznode	1	20	0	1	3	20	Multimedia
9	45b963397aa40d4a0063e0d85e4fe7a1	eznode:52	eznode	1	19	0	1	3	19	Files
9	59b514174bffe4ae402b3d63aad79fe0	eznode:51	eznode	1	18	0	1	3	18	Images
10	ccb62ebca03a31272430bc414bd5cd5b	eznode:45	eznode	1	12	0	1	3	12	Anonymous-User
11	c593ec85293ecb0e02d50d4c5c6c20eb	eznode:45	eznode	1	31	0	0	1	12	anonymous_user
17	2e5bc8831f7ae6a29530e7f1bbf2de9c	eznode:53	eznode	1	34	0	0	1	20	multimedia
17	45b963397aa40d4a0063e0d85e4fe7a1	eznode:52	eznode	1	33	0	0	1	19	files
17	59b514174bffe4ae402b3d63aad79fe0	eznode:51	eznode	1	32	0	0	1	18	images
\.


--
-- Data for Name: ezurlalias_ml_incr; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezurlalias_ml_incr (id) FROM stdin;
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
24
25
26
27
28
29
30
31
32
33
34
35
36
37
\.


--
-- Data for Name: ezurlwildcard; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezurlwildcard (id, destination_url, source_url, type) FROM stdin;
\.


--
-- Data for Name: ezuser; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezuser (contentobject_id, email, login, password_hash, password_hash_type, password_updated_at) FROM stdin;
10	nospam@ez.no	anonymous	$2y$10$35gOSQs6JK4u4whyERaeUuVeQBi2TUBIZIfP7HEj7sfz.MxvTuOeC	7	\N
14	nospam@ez.no	admin	$2y$10$FDn9NPwzhq85cLLxfD5Wu.L3SL3Z/LNCvhkltJUV0wcJj7ciJg2oy	7	\N
\.


--
-- Data for Name: ezuser_accountkey; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezuser_accountkey (id, hash_key, "time", user_id) FROM stdin;
\.


--
-- Data for Name: ezuser_role; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezuser_role (id, contentobject_id, limit_identifier, limit_value, role_id) FROM stdin;
28	11			1
31	42			1
32	13	Subtree	/1/2/	3
33	13	Subtree	/1/43/	3
34	12			2
35	13			4
\.


--
-- Data for Name: ezuser_setting; Type: TABLE DATA; Schema: public; Owner: rag
--

COPY public.ezuser_setting (user_id, is_enabled, max_login) FROM stdin;
10	1	1000
14	1	10
\.


--
-- Name: ezcobj_state_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcobj_state_group_id_seq', 2, true);


--
-- Name: ezcobj_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcobj_state_id_seq', 2, true);


--
-- Name: ezcontentbrowsebookmark_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentbrowsebookmark_id_seq', 1, true);


--
-- Name: ezcontentclass_attribute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentclass_attribute_id_seq', 180, true);


--
-- Name: ezcontentclass_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentclass_id_seq', 12, true);


--
-- Name: ezcontentclassgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentclassgroup_id_seq', 3, true);


--
-- Name: ezcontentobject_attribute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentobject_attribute_id_seq', 180, true);


--
-- Name: ezcontentobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentobject_id_seq', 51, true);


--
-- Name: ezcontentobject_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentobject_link_id_seq', 1, true);


--
-- Name: ezcontentobject_tree_node_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentobject_tree_node_id_seq', 53, true);


--
-- Name: ezcontentobject_version_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezcontentobject_version_id_seq', 506, true);


--
-- Name: ezimagefile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezimagefile_id_seq', 1, true);


--
-- Name: ezkeyword_attribute_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezkeyword_attribute_link_id_seq', 1, true);


--
-- Name: ezkeyword_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezkeyword_id_seq', 1, true);


--
-- Name: eznode_assignment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.eznode_assignment_id_seq', 38, true);


--
-- Name: eznotification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.eznotification_id_seq', 1, true);


--
-- Name: ezpackage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezpackage_id_seq', 1, true);


--
-- Name: ezpolicy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezpolicy_id_seq', 340, true);


--
-- Name: ezpolicy_limitation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezpolicy_limitation_id_seq', 256, true);


--
-- Name: ezpolicy_limitation_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezpolicy_limitation_value_id_seq', 483, true);


--
-- Name: ezpreferences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezpreferences_id_seq', 1, true);


--
-- Name: ezrole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezrole_id_seq', 4, true);


--
-- Name: ezsearch_object_word_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezsearch_object_word_link_id_seq', 72, true);


--
-- Name: ezsearch_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezsearch_word_id_seq', 49, true);


--
-- Name: ezsection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezsection_id_seq', 3, true);


--
-- Name: ezurl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezurl_id_seq', 1, true);


--
-- Name: ezurlalias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezurlalias_id_seq', 30, true);


--
-- Name: ezurlalias_ml_incr_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezurlalias_ml_incr_id_seq', 37, true);


--
-- Name: ezurlwildcard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezurlwildcard_id_seq', 1, true);


--
-- Name: ezuser_accountkey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezuser_accountkey_id_seq', 1, true);


--
-- Name: ezuser_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rag
--

SELECT pg_catalog.setval('public.ezuser_role_id_seq', 35, true);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: ezbinaryfile ezbinaryfile_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezbinaryfile
    ADD CONSTRAINT ezbinaryfile_pkey PRIMARY KEY (contentobject_attribute_id, version);


--
-- Name: ezcobj_state_group_language ezcobj_state_group_language_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcobj_state_group_language
    ADD CONSTRAINT ezcobj_state_group_language_pkey PRIMARY KEY (contentobject_state_group_id, real_language_id);


--
-- Name: ezcobj_state_group ezcobj_state_group_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcobj_state_group
    ADD CONSTRAINT ezcobj_state_group_pkey PRIMARY KEY (id);


--
-- Name: ezcobj_state_language ezcobj_state_language_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcobj_state_language
    ADD CONSTRAINT ezcobj_state_language_pkey PRIMARY KEY (contentobject_state_id, language_id);


--
-- Name: ezcobj_state_link ezcobj_state_link_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcobj_state_link
    ADD CONSTRAINT ezcobj_state_link_pkey PRIMARY KEY (contentobject_id, contentobject_state_id);


--
-- Name: ezcobj_state ezcobj_state_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcobj_state
    ADD CONSTRAINT ezcobj_state_pkey PRIMARY KEY (id);


--
-- Name: ezcontent_language ezcontent_language_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontent_language
    ADD CONSTRAINT ezcontent_language_pkey PRIMARY KEY (id);


--
-- Name: ezcontentbrowsebookmark ezcontentbrowsebookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentbrowsebookmark
    ADD CONSTRAINT ezcontentbrowsebookmark_pkey PRIMARY KEY (id);


--
-- Name: ezcontentclass_attribute_ml ezcontentclass_attribute_ml_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass_attribute_ml
    ADD CONSTRAINT ezcontentclass_attribute_ml_pkey PRIMARY KEY (contentclass_attribute_id, version, language_id);


--
-- Name: ezcontentclass_attribute ezcontentclass_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass_attribute
    ADD CONSTRAINT ezcontentclass_attribute_pkey PRIMARY KEY (id, version);


--
-- Name: ezcontentclass_classgroup ezcontentclass_classgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass_classgroup
    ADD CONSTRAINT ezcontentclass_classgroup_pkey PRIMARY KEY (contentclass_id, contentclass_version, group_id);


--
-- Name: ezcontentclass_name ezcontentclass_name_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass_name
    ADD CONSTRAINT ezcontentclass_name_pkey PRIMARY KEY (contentclass_id, contentclass_version, language_id);


--
-- Name: ezcontentclass ezcontentclass_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass
    ADD CONSTRAINT ezcontentclass_pkey PRIMARY KEY (id, version);


--
-- Name: ezcontentclassgroup ezcontentclassgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclassgroup
    ADD CONSTRAINT ezcontentclassgroup_pkey PRIMARY KEY (id);


--
-- Name: ezcontentobject_attribute ezcontentobject_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_attribute
    ADD CONSTRAINT ezcontentobject_attribute_pkey PRIMARY KEY (id, version);


--
-- Name: ezcontentobject_link ezcontentobject_link_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_link
    ADD CONSTRAINT ezcontentobject_link_pkey PRIMARY KEY (id);


--
-- Name: ezcontentobject_name ezcontentobject_name_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_name
    ADD CONSTRAINT ezcontentobject_name_pkey PRIMARY KEY (contentobject_id, content_version, content_translation);


--
-- Name: ezcontentobject ezcontentobject_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject
    ADD CONSTRAINT ezcontentobject_pkey PRIMARY KEY (id);


--
-- Name: ezcontentobject_trash ezcontentobject_trash_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_trash
    ADD CONSTRAINT ezcontentobject_trash_pkey PRIMARY KEY (node_id);


--
-- Name: ezcontentobject_tree ezcontentobject_tree_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_tree
    ADD CONSTRAINT ezcontentobject_tree_pkey PRIMARY KEY (node_id);


--
-- Name: ezcontentobject_version ezcontentobject_version_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentobject_version
    ADD CONSTRAINT ezcontentobject_version_pkey PRIMARY KEY (id);


--
-- Name: ezdfsfile ezdfsfile_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezdfsfile
    ADD CONSTRAINT ezdfsfile_pkey PRIMARY KEY (name_hash);


--
-- Name: ezgmaplocation ezgmaplocation_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezgmaplocation
    ADD CONSTRAINT ezgmaplocation_pkey PRIMARY KEY (contentobject_attribute_id, contentobject_version);


--
-- Name: ezimagefile ezimagefile_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezimagefile
    ADD CONSTRAINT ezimagefile_pkey PRIMARY KEY (id);


--
-- Name: ezkeyword_attribute_link ezkeyword_attribute_link_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezkeyword_attribute_link
    ADD CONSTRAINT ezkeyword_attribute_link_pkey PRIMARY KEY (id);


--
-- Name: ezkeyword ezkeyword_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezkeyword
    ADD CONSTRAINT ezkeyword_pkey PRIMARY KEY (id);


--
-- Name: ezmedia ezmedia_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezmedia
    ADD CONSTRAINT ezmedia_pkey PRIMARY KEY (contentobject_attribute_id, version);


--
-- Name: eznode_assignment eznode_assignment_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.eznode_assignment
    ADD CONSTRAINT eznode_assignment_pkey PRIMARY KEY (id);


--
-- Name: eznotification eznotification_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.eznotification
    ADD CONSTRAINT eznotification_pkey PRIMARY KEY (id);


--
-- Name: ezpackage ezpackage_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpackage
    ADD CONSTRAINT ezpackage_pkey PRIMARY KEY (id);


--
-- Name: ezpolicy_limitation ezpolicy_limitation_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpolicy_limitation
    ADD CONSTRAINT ezpolicy_limitation_pkey PRIMARY KEY (id);


--
-- Name: ezpolicy_limitation_value ezpolicy_limitation_value_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpolicy_limitation_value
    ADD CONSTRAINT ezpolicy_limitation_value_pkey PRIMARY KEY (id);


--
-- Name: ezpolicy ezpolicy_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpolicy
    ADD CONSTRAINT ezpolicy_pkey PRIMARY KEY (id);


--
-- Name: ezpreferences ezpreferences_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezpreferences
    ADD CONSTRAINT ezpreferences_pkey PRIMARY KEY (id);


--
-- Name: ezrole ezrole_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezrole
    ADD CONSTRAINT ezrole_pkey PRIMARY KEY (id);


--
-- Name: ezsearch_object_word_link ezsearch_object_word_link_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezsearch_object_word_link
    ADD CONSTRAINT ezsearch_object_word_link_pkey PRIMARY KEY (id);


--
-- Name: ezsearch_word ezsearch_word_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezsearch_word
    ADD CONSTRAINT ezsearch_word_pkey PRIMARY KEY (id);


--
-- Name: ezsection ezsection_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezsection
    ADD CONSTRAINT ezsection_pkey PRIMARY KEY (id);


--
-- Name: ezsite_data ezsite_data_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezsite_data
    ADD CONSTRAINT ezsite_data_pkey PRIMARY KEY (name);


--
-- Name: ezurl ezurl_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurl
    ADD CONSTRAINT ezurl_pkey PRIMARY KEY (id);


--
-- Name: ezurlalias_ml_incr ezurlalias_ml_incr_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurlalias_ml_incr
    ADD CONSTRAINT ezurlalias_ml_incr_pkey PRIMARY KEY (id);


--
-- Name: ezurlalias_ml ezurlalias_ml_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurlalias_ml
    ADD CONSTRAINT ezurlalias_ml_pkey PRIMARY KEY (parent, text_md5);


--
-- Name: ezurlalias ezurlalias_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurlalias
    ADD CONSTRAINT ezurlalias_pkey PRIMARY KEY (id);


--
-- Name: ezurlwildcard ezurlwildcard_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezurlwildcard
    ADD CONSTRAINT ezurlwildcard_pkey PRIMARY KEY (id);


--
-- Name: ezuser_accountkey ezuser_accountkey_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezuser_accountkey
    ADD CONSTRAINT ezuser_accountkey_pkey PRIMARY KEY (id);


--
-- Name: ezuser ezuser_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezuser
    ADD CONSTRAINT ezuser_pkey PRIMARY KEY (contentobject_id);


--
-- Name: ezuser_role ezuser_role_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezuser_role
    ADD CONSTRAINT ezuser_role_pkey PRIMARY KEY (id);


--
-- Name: ezuser_setting ezuser_setting_pkey; Type: CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezuser_setting
    ADD CONSTRAINT ezuser_setting_pkey PRIMARY KEY (user_id);


--
-- Name: ezco_link_from; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezco_link_from ON public.ezcontentobject_link USING btree (from_contentobject_id, from_contentobject_version, contentclassattribute_id);


--
-- Name: ezco_link_to_co_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezco_link_to_co_id ON public.ezcontentobject_link USING btree (to_contentobject_id);


--
-- Name: ezcobj_state_group_identifier; Type: INDEX; Schema: public; Owner: rag
--

CREATE UNIQUE INDEX ezcobj_state_group_identifier ON public.ezcobj_state_group USING btree (identifier);


--
-- Name: ezcobj_state_group_lmask; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_state_group_lmask ON public.ezcobj_state_group USING btree (language_mask);


--
-- Name: ezcobj_state_identifier; Type: INDEX; Schema: public; Owner: rag
--

CREATE UNIQUE INDEX ezcobj_state_identifier ON public.ezcobj_state USING btree (group_id, identifier);


--
-- Name: ezcobj_state_lmask; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_state_lmask ON public.ezcobj_state USING btree (language_mask);


--
-- Name: ezcobj_state_priority; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_state_priority ON public.ezcobj_state USING btree (priority);


--
-- Name: ezcobj_trash_co_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_trash_co_id ON public.ezcontentobject_trash USING btree (contentobject_id);


--
-- Name: ezcobj_trash_depth; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_trash_depth ON public.ezcontentobject_trash USING btree (depth);


--
-- Name: ezcobj_trash_modified_subnode; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_trash_modified_subnode ON public.ezcontentobject_trash USING btree (modified_subnode);


--
-- Name: ezcobj_trash_p_node_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_trash_p_node_id ON public.ezcontentobject_trash USING btree (parent_node_id);


--
-- Name: ezcobj_trash_path; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_trash_path ON public.ezcontentobject_trash USING btree (path_string);


--
-- Name: ezcobj_trash_path_ident; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_trash_path_ident ON public.ezcontentobject_trash USING btree (path_identification_string);


--
-- Name: ezcobj_version_creator_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_version_creator_id ON public.ezcontentobject_version USING btree (creator_id);


--
-- Name: ezcobj_version_status; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcobj_version_status ON public.ezcontentobject_version USING btree (status);


--
-- Name: ezcontent_language_name; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontent_language_name ON public.ezcontent_language USING btree (name);


--
-- Name: ezcontentbrowsebookmark_location; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentbrowsebookmark_location ON public.ezcontentbrowsebookmark USING btree (node_id);


--
-- Name: ezcontentbrowsebookmark_user; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentbrowsebookmark_user ON public.ezcontentbrowsebookmark USING btree (user_id);


--
-- Name: ezcontentbrowsebookmark_user_location; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentbrowsebookmark_user_location ON public.ezcontentbrowsebookmark USING btree (user_id, node_id);


--
-- Name: ezcontentclass_attr_ccid; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentclass_attr_ccid ON public.ezcontentclass_attribute USING btree (contentclass_id);


--
-- Name: ezcontentclass_attribute_ml_lang_fk; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentclass_attribute_ml_lang_fk ON public.ezcontentclass_attribute_ml USING btree (language_id);


--
-- Name: ezcontentclass_identifier; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentclass_identifier ON public.ezcontentclass USING btree (identifier, version);


--
-- Name: ezcontentclass_version; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentclass_version ON public.ezcontentclass USING btree (version);


--
-- Name: ezcontentobject_attribute_co_id_ver_lang_code; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_attribute_co_id_ver_lang_code ON public.ezcontentobject_attribute USING btree (contentobject_id, version, language_code);


--
-- Name: ezcontentobject_attribute_language_code; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_attribute_language_code ON public.ezcontentobject_attribute USING btree (language_code);


--
-- Name: ezcontentobject_classattr_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_classattr_id ON public.ezcontentobject_attribute USING btree (contentclassattribute_id);


--
-- Name: ezcontentobject_classid; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_classid ON public.ezcontentobject USING btree (contentclass_id);


--
-- Name: ezcontentobject_currentversion; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_currentversion ON public.ezcontentobject USING btree (current_version);


--
-- Name: ezcontentobject_lmask; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_lmask ON public.ezcontentobject USING btree (language_mask);


--
-- Name: ezcontentobject_name_cov_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_name_cov_id ON public.ezcontentobject_name USING btree (content_version);


--
-- Name: ezcontentobject_name_lang_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_name_lang_id ON public.ezcontentobject_name USING btree (language_id);


--
-- Name: ezcontentobject_name_name; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_name_name ON public.ezcontentobject_name USING btree (name);


--
-- Name: ezcontentobject_owner; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_owner ON public.ezcontentobject USING btree (owner_id);


--
-- Name: ezcontentobject_pub; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_pub ON public.ezcontentobject USING btree (published);


--
-- Name: ezcontentobject_remote_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE UNIQUE INDEX ezcontentobject_remote_id ON public.ezcontentobject USING btree (remote_id);


--
-- Name: ezcontentobject_section; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_section ON public.ezcontentobject USING btree (section_id);


--
-- Name: ezcontentobject_status; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_status ON public.ezcontentobject USING btree (status);


--
-- Name: ezcontentobject_tree_co_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_tree_co_id ON public.ezcontentobject_tree USING btree (contentobject_id);


--
-- Name: ezcontentobject_tree_contentobject_id_path_string; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_tree_contentobject_id_path_string ON public.ezcontentobject_tree USING btree (path_string, contentobject_id);


--
-- Name: ezcontentobject_tree_depth; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_tree_depth ON public.ezcontentobject_tree USING btree (depth);


--
-- Name: ezcontentobject_tree_p_node_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_tree_p_node_id ON public.ezcontentobject_tree USING btree (parent_node_id);


--
-- Name: ezcontentobject_tree_path; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_tree_path ON public.ezcontentobject_tree USING btree (path_string);


--
-- Name: ezcontentobject_tree_path_ident; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_tree_path_ident ON public.ezcontentobject_tree USING btree (path_identification_string);


--
-- Name: ezcontentobject_tree_remote_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontentobject_tree_remote_id ON public.ezcontentobject_tree USING btree (remote_id);


--
-- Name: ezcontobj_version_obj_status; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezcontobj_version_obj_status ON public.ezcontentobject_version USING btree (contentobject_id, status);


--
-- Name: ezdfsfile_expired_name; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezdfsfile_expired_name ON public.ezdfsfile USING btree (expired, name);


--
-- Name: ezdfsfile_mtime; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezdfsfile_mtime ON public.ezdfsfile USING btree (mtime);


--
-- Name: ezdfsfile_name; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezdfsfile_name ON public.ezdfsfile USING btree (name);


--
-- Name: ezdfsfile_name_trunk; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezdfsfile_name_trunk ON public.ezdfsfile USING btree (name_trunk);


--
-- Name: ezimagefile_coid; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezimagefile_coid ON public.ezimagefile USING btree (contentobject_attribute_id);


--
-- Name: ezimagefile_file; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezimagefile_file ON public.ezimagefile USING btree (filepath);


--
-- Name: ezkeyword_attr_link_kid_oaid; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezkeyword_attr_link_kid_oaid ON public.ezkeyword_attribute_link USING btree (keyword_id, objectattribute_id);


--
-- Name: ezkeyword_attr_link_oaid; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezkeyword_attr_link_oaid ON public.ezkeyword_attribute_link USING btree (objectattribute_id);


--
-- Name: ezkeyword_attr_link_oaid_ver; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezkeyword_attr_link_oaid_ver ON public.ezkeyword_attribute_link USING btree (objectattribute_id, version);


--
-- Name: ezkeyword_keyword; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezkeyword_keyword ON public.ezkeyword USING btree (keyword);


--
-- Name: eznode_assignment_co_version; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX eznode_assignment_co_version ON public.eznode_assignment USING btree (contentobject_version);


--
-- Name: eznode_assignment_coid_cov; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX eznode_assignment_coid_cov ON public.eznode_assignment USING btree (contentobject_id, contentobject_version);


--
-- Name: eznode_assignment_is_main; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX eznode_assignment_is_main ON public.eznode_assignment USING btree (is_main);


--
-- Name: eznode_assignment_parent_node; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX eznode_assignment_parent_node ON public.eznode_assignment USING btree (parent_node);


--
-- Name: eznotification_owner; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX eznotification_owner ON public.eznotification USING btree (owner_id);


--
-- Name: eznotification_owner_is_pending; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX eznotification_owner_is_pending ON public.eznotification USING btree (owner_id, is_pending);


--
-- Name: ezpolicy_limit_value_limit_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezpolicy_limit_value_limit_id ON public.ezpolicy_limitation_value USING btree (limitation_id);


--
-- Name: ezpolicy_limitation_value_val; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezpolicy_limitation_value_val ON public.ezpolicy_limitation_value USING btree (value);


--
-- Name: ezpolicy_original_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezpolicy_original_id ON public.ezpolicy USING btree (original_id);


--
-- Name: ezpolicy_role_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezpolicy_role_id ON public.ezpolicy USING btree (role_id);


--
-- Name: ezpreferences_name; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezpreferences_name ON public.ezpreferences USING btree (name);


--
-- Name: ezpreferences_user_id_idx; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezpreferences_user_id_idx ON public.ezpreferences USING btree (user_id, name);


--
-- Name: ezsearch_object_word_link_frequency; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezsearch_object_word_link_frequency ON public.ezsearch_object_word_link USING btree (frequency);


--
-- Name: ezsearch_object_word_link_identifier; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezsearch_object_word_link_identifier ON public.ezsearch_object_word_link USING btree (identifier);


--
-- Name: ezsearch_object_word_link_integer_value; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezsearch_object_word_link_integer_value ON public.ezsearch_object_word_link USING btree (integer_value);


--
-- Name: ezsearch_object_word_link_object; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezsearch_object_word_link_object ON public.ezsearch_object_word_link USING btree (contentobject_id);


--
-- Name: ezsearch_object_word_link_word; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezsearch_object_word_link_word ON public.ezsearch_object_word_link USING btree (word_id);


--
-- Name: ezsearch_word_obj_count; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezsearch_word_obj_count ON public.ezsearch_word USING btree (object_count);


--
-- Name: ezsearch_word_word_i; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezsearch_word_word_i ON public.ezsearch_word USING btree (word);


--
-- Name: ezurl_ol_coa_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurl_ol_coa_id ON public.ezurl_object_link USING btree (contentobject_attribute_id);


--
-- Name: ezurl_ol_coa_version; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurl_ol_coa_version ON public.ezurl_object_link USING btree (contentobject_attribute_version);


--
-- Name: ezurl_ol_url_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurl_ol_url_id ON public.ezurl_object_link USING btree (url_id);


--
-- Name: ezurl_url; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurl_url ON public.ezurl USING btree (url);


--
-- Name: ezurlalias_desturl; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_desturl ON public.ezurlalias USING btree (destination_url);


--
-- Name: ezurlalias_forward_to_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_forward_to_id ON public.ezurlalias USING btree (forward_to_id);


--
-- Name: ezurlalias_imp_wcard_fwd; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_imp_wcard_fwd ON public.ezurlalias USING btree (is_imported, is_wildcard, forward_to_id);


--
-- Name: ezurlalias_ml_act_org; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_ml_act_org ON public.ezurlalias_ml USING btree (action, is_original);


--
-- Name: ezurlalias_ml_actt_org_al; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_ml_actt_org_al ON public.ezurlalias_ml USING btree (action_type, is_original, is_alias);


--
-- Name: ezurlalias_ml_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_ml_id ON public.ezurlalias_ml USING btree (id);


--
-- Name: ezurlalias_ml_par_act_id_lnk; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_ml_par_act_id_lnk ON public.ezurlalias_ml USING btree (action, id, link, parent);


--
-- Name: ezurlalias_ml_par_lnk_txt; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_ml_par_lnk_txt ON public.ezurlalias_ml USING btree (parent, text, link);


--
-- Name: ezurlalias_ml_text; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_ml_text ON public.ezurlalias_ml USING btree (text, id, link);


--
-- Name: ezurlalias_ml_text_lang; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_ml_text_lang ON public.ezurlalias_ml USING btree (text, lang_mask, parent);


--
-- Name: ezurlalias_source_md5; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_source_md5 ON public.ezurlalias USING btree (source_md5);


--
-- Name: ezurlalias_source_url; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_source_url ON public.ezurlalias USING btree (source_url);


--
-- Name: ezurlalias_wcard_fwd; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezurlalias_wcard_fwd ON public.ezurlalias USING btree (is_wildcard, forward_to_id);


--
-- Name: ezuser_login; Type: INDEX; Schema: public; Owner: rag
--

CREATE UNIQUE INDEX ezuser_login ON public.ezuser USING btree (login);


--
-- Name: ezuser_role_contentobject_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezuser_role_contentobject_id ON public.ezuser_role USING btree (contentobject_id);


--
-- Name: ezuser_role_role_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX ezuser_role_role_id ON public.ezuser_role USING btree (role_id);


--
-- Name: hash_key; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX hash_key ON public.ezuser_accountkey USING btree (hash_key);


--
-- Name: idx_object_version_objver; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX idx_object_version_objver ON public.ezcontentobject_version USING btree (contentobject_id, version);


--
-- Name: latitude_longitude_key; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX latitude_longitude_key ON public.ezgmaplocation USING btree (latitude, longitude);


--
-- Name: modified_subnode; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX modified_subnode ON public.ezcontentobject_tree USING btree (modified_subnode);


--
-- Name: policy_id; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX policy_id ON public.ezpolicy_limitation USING btree (policy_id);


--
-- Name: sort_key_int; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX sort_key_int ON public.ezcontentobject_attribute USING btree (sort_key_int);


--
-- Name: sort_key_string; Type: INDEX; Schema: public; Owner: rag
--

CREATE INDEX sort_key_string ON public.ezcontentobject_attribute USING btree (sort_key_string);


--
-- Name: ezcontentbrowsebookmark ezcontentbrowsebookmark_location_fk; Type: FK CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentbrowsebookmark
    ADD CONSTRAINT ezcontentbrowsebookmark_location_fk FOREIGN KEY (node_id) REFERENCES public.ezcontentobject_tree(node_id) ON DELETE CASCADE;


--
-- Name: ezcontentbrowsebookmark ezcontentbrowsebookmark_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentbrowsebookmark
    ADD CONSTRAINT ezcontentbrowsebookmark_user_fk FOREIGN KEY (user_id) REFERENCES public.ezuser(contentobject_id) ON DELETE CASCADE;


--
-- Name: ezcontentclass_attribute_ml ezcontentclass_attribute_ml_lang_fk; Type: FK CONSTRAINT; Schema: public; Owner: rag
--

ALTER TABLE ONLY public.ezcontentclass_attribute_ml
    ADD CONSTRAINT ezcontentclass_attribute_ml_lang_fk FOREIGN KEY (language_id) REFERENCES public.ezcontent_language(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: rag
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO rag;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: rag
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

