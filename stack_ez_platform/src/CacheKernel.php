<?php
/**
 * © Project
 */

namespace App;

use EzSystems\PlatformHttpCacheBundle\AppCache as PlatformHttpCacheBundleAppCache;

/**
 * For easier upgrade do not change this file, prefer to adapt public/index.php instead.
 */
final class CacheKernel extends PlatformHttpCacheBundleAppCache
{
}
